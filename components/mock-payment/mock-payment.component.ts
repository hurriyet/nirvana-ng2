import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mock-payment',
  templateUrl: './mock-payment.component.html',
  styleUrls: ['./mock-payment.component.scss']
})
export class MockPaymentComponent implements OnInit {
	private addedItem : Object = {};
	private result : string = '';
  	constructor() { }

	ngOnInit() {
	}

    


    private addProductsToSlip(){
  		try {
                var saleItemRequest = {
                    Name: 'TEST',
                    Barcode: '',
                    Quantity: 1,
                    UnitPrice: 1000,
                    TaxRate: 18,
                    TransactionType: window['TransactionType_FiscalProcess']
                };

                var saleItemRequestJson = JSON.stringify(saleItemRequest);
                var saleItemResponse = window['DeviceIntegrationInstance'].saleItem(saleItemRequestJson);
                if (saleItemResponse.result.isTransactionSuccess) {
                     this.addedItem = saleItemRequest;
                    if (saleItemResponse.result.deviceResultValue == window['ITEMSALE_SUCCESS']) {
                       
                    }
                    else if (saleItemResponse.result.deviceResultValue == window['POS_HAS_UNCOMPLETED_PROCESS']) {
                        window['DeviceIntegrationInstance'].cancelReceipt();
                    }
                    else {
                        //Did not success
                    }
                }
                else {
                    //Error
                }
            }
            catch (err) {
                alert(err);
            }
  }

  private makePayment(){
  	let payment = { amount : 1000, type : 1};
  	   try {
                var paymentRequest = {
                    Amount: payment.amount,
                    PaymentType: payment.type, //2 kk 1 nakit
                    //ReceiptMessages: [
                    //            {
                    //                MessageText: $("#txtFreeMessageText").val(),
                    //                ReceiptMessageParameters: [ReceiptMessageType_Size24, ReceiptMessageType_CENTER, ReceiptMessageType_BOLD]
                    //            }
                    //]
                };

                var paymentRequestJson = JSON.stringify(paymentRequest);
                var paymentResponse = window['DeviceIntegrationInstance'].doPayment(paymentRequestJson);
                if (paymentResponse.result.isTransactionSuccess) {
                    if (paymentResponse.result.deviceResultValue == window['PAYMENT_SUCCESS']) {
                        //Success
                        var uniqueId = paymentResponse.response.uniqueId;
                        var authorizationCode = paymentResponse.response.authorizationCode;
                        var bankBkmId = paymentResponse.response.bankBkmId;
                        var bankName = paymentResponse.response.bankName;
                        var batchNo = paymentResponse.response.batchNo;
                        var merchantId = paymentResponse.response.merchantId;
                        var numberOfBonus = paymentResponse.response.numberOfBonus;
                        var numberOfDiscount = paymentResponse.response.numberOfDiscount;
                        var pan = paymentResponse.response.pan;
                        var rrn = paymentResponse.response.rrn;
                        var stan = paymentResponse.response.stan;
                        var terminalId = paymentResponse.response.terminalId;
                        var totalReceiptAmount = paymentResponse.response.totalReceiptAmount;
                        var transFlag = paymentResponse.response.transFlag;
                        var numberOfInstallments = paymentResponse.response.numberOfInstallments;
                        var strResult = "uniqueId:" + uniqueId + "|" +
                                        "authorizationCode:" + authorizationCode + "|" +
                                        "bankBkmId:" + bankBkmId + "|" +
                                        "bankName:" + bankName + "|" +
                                        "batchNo:" + batchNo + "|" +
                                        "merchantId:" + merchantId + "|" +
                                        "numberOfBonus:" + numberOfBonus + "|" +
                                        "numberOfDiscount:" + numberOfDiscount + "|" +
                                        "pan:" + pan + "|" +
                                        "rrn:" + rrn + "|" +
                                        "stan:" + stan + "|" +
                                        "terminalId:" + terminalId + "|" +
                                        "totalReceiptAmount:" + totalReceiptAmount + "|" +
                                        "transFlag:" + transFlag + "|" +
                                        "numberOfInstallments:" + numberOfInstallments + "|";
                        this.result = strResult;
                    }
                    else {
                        //Did not success
                    }
                }
                else {
                    //Error
                }
            }
            catch (err) {
                alert(err);
            }
  	}



}
