import { Component, OnInit,Input,Output,EventEmitter,HostBinding } from '@angular/core';

@Component({
  selector: 'app-multi-inline-edit',
  templateUrl: './multi-inline-edit.component.html',
  styleUrls: ['./multi-inline-edit.component.scss']
})
export class MultiInlineEditComponent implements OnInit {
  @HostBinding ('class.c-multi-inline') true;
  private isEditable: boolean = true;
  private isEdited: boolean = true;
  private editCount:number = 0;
  // Apiden Değişikik için olumlu cevap gelmiş gibi !
  private apiRespondOkForTheChange:boolean = true;


  @Output() fieldEdit: EventEmitter<any> = new EventEmitter();
  @Input() text:string ;
  @Input() phoneNumber:string ;
  constructor() { }

  ngOnInit() {
  }

  beginEdit(el:HTMLElement,phoneEl:HTMLElement):void {
    this.isEditable = false;
    setTimeout(() => {
      el.focus();
      phoneEl.focus();
    }, 100); 
  }

  reEdit(newText:string,newNumber:string) {
    this.isEditable = true;
	  this.isEdited = false;
	  this.editCount = 1;
    this.text = newText;
    this.phoneNumber = newNumber;
    this.fieldEdit.emit({
      'username': this.text,
      'phoneNumber': this.phoneNumber,
      // Apiden değişim için olumlu cevap gelmiş gibi 
      'apiResponse': this.apiRespondOkForTheChange
      });      

    

  }

}
