
import { Router, ActivatedRoute } from '@angular/router';
import { WizardHeaderComponent } from './../wizard-header/wizard-header.component';
import { Component, OnInit, HostBinding, Input, ViewChild, ViewChildren, ElementRef, Output, EventEmitter } from '@angular/core';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { NotificationService } from '../../services/notification.service';
import { OkcVariables } from '../../classes/okc-variables';
import { TetherDialog } from '../../components/tether-dialog/tether-dialog';

declare var $: any;

@Component({
	selector: 'app-multiple-payment-block',
	templateUrl: './multiple-payment-block.component.html',
	styleUrls: ['./multiple-payment-block.component.scss']
})
export class MultiplePaymentBlockComponent implements OnInit {
	@ViewChild(WizardHeaderComponent) wizardHeader: WizardHeaderComponent;
	@ViewChildren('paymentInput') paymentInputs: ElementRef[];

	@HostBinding('class.c-multiple-payment-block') true;

	@Output() paymentActionEvent: EventEmitter<{ action: string, paymentData: any, paymentList: any[] }> = new EventEmitter();

	@Input() customerScore: number;

	//@Input() paymentTypes: {type: string, name: string, icon: string, amount?:number, isSelected?: boolean, isFocused?:boolean, isPromising?: boolean, params?: {paymentType: any}}[];
	@Input() paymentTypes: any = [];
	@Input() paymentList: { id: string, type: string, name: string, amount: number, info?: string, isPromising?: boolean, params?: { payment: any } }[];
	@Input() paymentListStorage: { id: string, type: string, name: string, amount: number, info?: string, isPromising?: boolean, params?: { payment: any } }[];
	@Input() remainingAmount: number;
	@Input() productCount: number;
	@Input() orderSummaryList: { name: string, amount: string }[];
	@Input() isBalanced: boolean;
	@Input() currency: string = "TL";
	private okcVariables = OkcVariables;
	private customer;
	private total;
	private amountStore: any;
	private lastInputPayment: any;
	private paymentProcessType: number;
	private result;
	private levels: { key: string, title: string, params?: any }[];
	private currentLevel: { key: string, title: string, params?: any };
	private currentLevelIndex: number = -1;
	private isPrintLastReceipt: boolean = false;
	private currentState: any;
	private isOnProcessType: boolean = true;
	private selectedPayment: { type: string, name: string, icon: string, isSelected?: boolean, isPromising?: boolean, params?: { paymentType: any } };
	private subscription;
	constructor(private shoppingCartService: ShoppingCartService, private notificationService: NotificationService, private router: Router, private tetherService: TetherDialog) { }

	ngOnInit() {
		this.shoppingCartService.setCustomEndpoint('GotoPayment', true);
		let changeState = this.shoppingCartService.create({});
		changeState.subscribe(state => {
			if (state["CurrentState"]) {
				this.shoppingCartService.setCustomEndpoint('GetCurrentState', true);
				this.shoppingCartService.query({});
			}
		}, error => {
			if (error['Message']) {
				this.notificationService.add({ text: error['Message'], type: 'warning' })
			}
		});
		this.subscription = this.shoppingCartService.data.subscribe(
			result => {
				if (result && result.length == undefined && result['State'] && result['CurrentState'] == 7 && result['State']["PaymentOptions"]) {
					result["State"]["PaymentOptions"].forEach(payment => {
						this.paymentTypes.push({
							Id: payment["Id"],
							Name: payment["Name"],
							Type: payment["Type"],
							TypeId: payment["TypeId"],
							Type_Desc: payment["Type_Desc"],
							icon: "account_balance_wallet",
							isSelected: false,
							isFocused: false
						})
					})

					this.total = result["State"]["SubTotal"];
					this.currentState = result["State"];
					this.productCount = 0;
					this.remainingAmount = this.total.TotalAmount;
					this.addProductsToSlip();
					/*
					this.currentState['Events'].forEach(event=>{
						event['Performances'].forEach(performance => {
							event['Products'].forEach(product => {
								this.productCount += product['CountOfProductsSelected'];
							})
						})
					})
					*/
				}
			}, error => {
				if (error['Message']) {
					this.notificationService.add({ text: error['Message'], type: 'warning' })
				}
			});
		this.levels = [];
		this.levels.push({ key: "payment", title: "ÖDEME İŞLEMLERİ" });
		this.levels.push({ key: "summary", title: "ALIŞVERİŞ ÖZETİ" });
		this.customer = this.shoppingCartService.getCartUser();
		this.customerScore = 0; //Kullanıcı puanı, servis bağlanınca maplenebilir veya getter olarak düzenlenebilir
		this.paymentProcessType = 0;
		/*

			this.paymentTypes = [];
			this.paymentTypes.push({type: "creditCard", name: "KREDİ KARTI", icon: "credit_card"});
			this.paymentTypes.push({type: "bankTransfer", name: "BANKA HAVALESİ", icon: "note-multiple-outline"});
			this.paymentTypes.push({type: "cash", name: "NAKİT", icon: "account_balance_wallet"});
			this.paymentTypes.push({type: "customerScore", name: "PUAN", icon: "local_activity"});

		*/

		if (!this.paymentList) {
			this.paymentList = []; this.paymentListStorage = [];
		}
		this.gotoLevel(0);

	}

	ngAfterViewInit() {

	}

	private selectPaymentType(paymentType, event) {
		if (this.isBalanced) return;
		this.paymentTypes.map(item => item.isSelected = item.Type == paymentType.Type);
		let self = this;

		setTimeout(function () {

			let input: ElementRef = self.paymentInputs.find(input => $(input.nativeElement).data('paymentType') == paymentType.Type);
			if (input) input.nativeElement.focus();
		}, 100);
	}
	private makePayment(paymentType, event, map?: string) {
		// Fiscal İşlemlerde hata olduğunda girilen tüm verileri tutup Aynısını tekrar gönderebilemk içişn clonluyoruz
		if (map != 'onFiscalError') {
			this.lastInputPayment = JSON.parse(JSON.stringify(paymentType));
		}

		if (this.isBalanced) return;
		paymentType.isPromising = true;
		paymentType.isSelected = false;
		let self = this;
		let amount = this.processAmount(paymentType.amount); //Math.round(Number(paymentType.amount) * 100) / 100;
		let willCheckBalance: number = Number(self.remainingAmount) - amount;
		let balance: number = this.processAmount(Number(self.remainingAmount) - this.processAmount(amount));

		if (willCheckBalance < 0) {
			this.notificationService.add({ text: 'Ödeme yaptığınız tutar toplam tutardan fazla, kontrol edip tekrar deneyiniz', type: 'warning' });
			paymentType.isPromising = false;
			paymentType.amount = "";
			return false;

		} else if (isNaN(willCheckBalance) || isNaN(amount)) {

			paymentType.isPromising = false;
			paymentType.amount = "";
			this.notificationService.add({ text: 'Ödeme alanı sadece rakam kabul etmektedir.', type: 'warning' });
			return false;
		} else if (amount <= 0.09) {
			paymentType.isPromising = false;
			paymentType.amount = "";
			this.notificationService.add({ text: "Ödeme miktarı 0'dan büyük olmalı.", type: 'warning' });
			return false;
		}
		let paymentModel = {
			Id: Math.random().toString(30).substr(2, 5),
			Type: "",
			Amount: amount,
			Balance: balance,
			IsBalanced: balance == 0
		}

		paymentType.isPromising = false;

		if (map != 'onFiscalError') {
			paymentType.amount = "";
		}

		this.addPayment({
			id: Math.random().toString(30).substr(2, 5),
			type: paymentType.Type,
			name: paymentType.Type_Desc,
			amount: amount,
			paymentType: paymentType,
			isBalanced: paymentModel.IsBalanced,
			balance: paymentModel.Balance
		});
	}

	private addPayment(payment: { id: string, type: string, amount: number, name: string, info?: string, isBalanced: boolean, balance: any, paymentType?: any }) {
		try {
			let paymentRequest = {
				Amount: payment.amount,
				PaymentType: payment.paymentType.Type, //2 kk 1 nakit
				PaymentProcessType: this.paymentProcessType,
				//ReceiptMessages: [
				//            {
				//                MessageText: $("#txtFreeMessageText").val(),
				//                ReceiptMessageParameters: [ReceiptMessageType_Size24, ReceiptMessageType_CENTER, ReceiptMessageType_BOLD]
				//            }
				//]

			};
			let paymentRequestJson = JSON.stringify(paymentRequest);
			let paymentResponse = window['DeviceIntegrationInstance'].doPayment(paymentRequestJson);
			if (paymentResponse.result.isTransactionSuccess) {
				if (paymentResponse.result.deviceResultValue == this.okcVariables.PAYMENT_SUCCESS || paymentResponse.result.deviceResultValue == this.okcVariables.PAYMENT_SUCCESS_BUT_NOT_PRINT_MF) {
					if (paymentResponse.result.deviceResultValue == this.okcVariables.PAYMENT_SUCCESS_BUT_NOT_PRINT_MF) {
						this.notificationService.add({ text: 'Ödeme Başarılı Bir Şekilde Tamamlandı.!! Son Aşamada Yetersiz Kağıt veya Cihaz Bağlantılarından Dolayı Bir Hata Oluşmuştur !!', type: 'warning', timeOut: 10000 });
						this.isPrintLastReceipt = true;

					}
					payment['info'] = paymentResponse.response.bankName;
					payment['okcResponse'] = paymentResponse.response;
					this.paymentListStorage.push(payment);
					this.paymentList.push(payment);
					this.isBalanced = payment.isBalanced;
					this.remainingAmount = this.processAmount(payment.balance);//paymentResponse.response.decimalTotalRemainAmount;  //Math.round(Number(payment.balance) * 100) / 100;
					this.paymentProcessType = 0;
				}
				else if (paymentResponse.result.deviceResultValue == this.okcVariables.PAYMENT_FAILED_ON_FISCAL_PROCESS) {
					this.paymentProcessType = 1;
					this.notificationService.add({ text: 'Mali İşlem Tamamlanamadı. Yetersiz Kağıt veya Cihaz Bağlantılarını Kontrol Ediniz!!', type: 'danger', timeOut: 10000 });
				}
				else if (paymentResponse.result.deviceResultValue == this.okcVariables.NO_PAPER) {
					this.msgHandler('NO_PAPER', 'warning');
				}
				else if (paymentResponse.result.deviceResultValue == this.okcVariables.CABLE_ERROR) {
					this.msgHandler('CABLE_ERROR', 'warning');
				}
				else if (paymentResponse.result.deviceResultValue == this.okcVariables.TIMEOUT) {
					this.msgHandler('TIMEOUT', 'warning');
				}
				else if (paymentResponse.result.deviceResultValue == this.okcVariables.PAYMENT_FAILED) {
					this.msgHandler('PAYMENT_FAILED', 'danger');
				}
				else {
					this.msgHandler('PAYMENT_FAILED', 'danger');
				}
			}
			else {
				this.msgHandler('PAYMENT_PROCESS_FAILED', 'danger');
			}

		}
		catch (err) {
			this.msgHandler('EXCEPTION', 'danger', err);
		}
	}
	private msgHandler(msgType: string, type: string, params?: any) {
		switch (msgType) {
			case ('CABLE_ERROR'):
				this.notificationService.add({ text: 'İletişim Problemi !! Lütfen OKC Cihazı Kablosunu Kontrol Ediniz.', type: type });
				break;
			case ('TIMEOUT'):
				this.notificationService.add({ text: 'ÖKC ürün satış işlemi sırasında zaman aşımı, işlem iptal ediliyor.', type: type });
				break;
			case ('PAYMENT_FAILED'):
				this.notificationService.add({ text: 'Ödeme başarısız oldu.', type: type });
				break;
			case ('NO_PAPER'):
				this.notificationService.add({ text: 'Yetersiz Kağıt.', type: type })
				break;
			case ('PAYMENT_PROCESS_FAILED'):
				this.notificationService.add({ text: "İşlem Başarılı Bir Şekilde Tamamlanamadı", type: type })
				break;
			case ('EXCEPTION'):
				this.notificationService.add({ text: "Bir Hata oluştu /" + params, type: type });
				break;
		}
	}
	private cancelTransaction() {
		let cancelTransactionResponse = this.cancelReceiptProcess();
		if (cancelTransactionResponse == true) {
			this.shoppingCartService.flushCart();
			this.router.navigate(['/boxoffice']);
		}
	}


	private removePayment(payment: { id: string }, event) {
		let paymentItem: any = this.paymentList.find(item => item.id == payment.id);
		if (paymentItem) {
			try {
				let paymentCancelRequest = {
					PaymentIndex: this.paymentListStorage.indexOf(paymentItem)
				};
				let paymentCancelRequestJson = JSON.stringify(paymentCancelRequest);
				let paymentCancelResponse = window["DeviceIntegrationInstance"].cancelPayment(paymentCancelRequestJson);
				if (paymentCancelResponse.result.isTransactionSuccess) {
					if (paymentCancelResponse.result.deviceResultValue == this.okcVariables.CANCEL_PAYMENT_SUCCESS) {
						paymentItem.isPromising = true;
						let balance: number = Number(this.remainingAmount) + Number(paymentItem.amount);
						if (isNaN(balance)) {
							if (confirm('Ödeme silme sırasında bir hata ile karşılaşıldı. Tüm ödemeler silinecek, onaylıyor musunuz?')) {
								let cancelAllResponse = window["DeviceIntegrationInstance"].cancelAllPayment();
								this.isBalanced = false;
								paymentItem.isPromising = false;
								paymentItem.amount = "";
							}
							return false;
						} else {
							let responseData = {
								Id: paymentItem.id,
								Balance: balance,
								IsBalanced: balance == 0
							}
							paymentItem.isPromising = false;
							this.isBalanced = responseData.IsBalanced;
							this.remainingAmount = this.processAmount(responseData.Balance); //paymentCancelResponse.response.decimalTotalRemainAmount; //Math.round(Number(responseData.Balance) * 100) / 100;

							this.paymentList.splice(this.paymentList.indexOf(paymentItem), 1);
							if (this.paymentList.length == 0) this.remainingAmount = this.total.TotalAmount;
						}
					}
					else if (paymentCancelResponse.result.deviceResultValue == this.okcVariables.NO_PAPER) {
						this.msgHandler('NO_PAPER', 'warning');
					}
					else if (paymentCancelResponse.result.deviceResultValue == this.okcVariables.CABLE_ERROR) {
						this.msgHandler('CABLE_ERROR', 'warning');
					}
					else if (paymentCancelResponse.result.deviceResultValue == this.okcVariables.TIMEOUT) {
						this.msgHandler('TIMEOUT', 'warning');
					}
					else {
						this.notificationService.add({ text: 'Ödeme İptal Edilemedi', type: 'danger' });
					}
				}
				else {
					//Error
					this.notificationService.add({ text: 'Ödeme İptal Edilirken Bir Hata Oluştu !', type: 'danger' });
				}
			}
			catch (err) {
				this.msgHandler('EXCEPTION', 'danger', err);

			}
		}
	}
	private refundPayment(payment, paymentItem) {
		let paymentVoidRequest = {};
		paymentVoidRequest['Amount'] = payment.amount;
		paymentVoidRequest['BankBkmId'] = payment['okcResponse']['bankBkmId'];
		paymentVoidRequest['BatchNo'] = payment['okcResponse']['batchNo'];
		paymentVoidRequest['Stan'] = payment['okcResponse']['stan'];
		paymentVoidRequest['TerminalId'] = payment['okcResponse']['terminalId'];
		let paymentVoidRequestJson = JSON.stringify(paymentVoidRequest);
		let paymentVoidResponse = window["DeviceIntegrationInstance"].voidPayment(paymentVoidRequestJson);
		if (paymentVoidResponse.result.isTransactionSuccess) {
			if (paymentVoidResponse.result.deviceResultValue == this.okcVariables.VOID_PAYMENT_SUCCESS) {
				paymentItem.isPromising = true;
				let balance: number = Number(this.remainingAmount) + Number(paymentItem.amount);
				if (isNaN(balance)) {
					if (confirm('Ödeme silme sırasında bir hata ile karşılaşıldı. Tüm ödemeler silinecek, onaylıyor musunuz?')) {
						let cancelAllResponse = window["DeviceIntegrationInstance"].cancelAllPayment();
						this.isBalanced = false;
						paymentItem.isPromising = false;
						paymentItem.amount = "";
					}
					return false;
				} else {
					let responseData = {
						Id: paymentItem.id,
						Balance: balance,
						IsBalanced: balance == 0
					}
					paymentItem.isPromising = false;
					this.isBalanced = responseData.IsBalanced;
					this.remainingAmount = this.processAmount(responseData.Balance); //Math.round(Number(responseData.Balance) * 100) / 100;
					this.paymentList.splice(this.paymentList.indexOf(paymentItem), 1);
					if (this.paymentList.length == 0) this.remainingAmount = this.total.TotalAmount;
				}
			}
			else {
				//Did not success
			}
		}
		else {
			//Error
		}
	}
	submitClickHandler(event) {
		if (!this.currentLevel) return;
		if (this.isBalanced) {
			switch (this.currentLevel.key) {
				case "summary":
					this.sendPostPayment();
					//this.paymentActionEvent.emit({action: "complete", paymentData: null, paymentList: this.paymentList});
					break;
				default:
					this.nextLevel();
					break;
			}
		}
	}
	private sendPostPayment() {
		let payments = [];
		this.paymentList.forEach(payment => {
			let okcDetail = {};
			if (payment['okcResponse']) {
				okcDetail = {
					"UniqueId": payment['okcResponse']['uniqueId'],
					"AuthorizationCode": payment['okcResponse']['authorizationCode'],
					"BankBkmId": payment['okcResponse']['bankBkmId'],
					"BankName": payment['okcResponse']['bankName'],
					"BatchNo": payment['okcResponse']['batchNo'],
					"MerchantId": payment['okcResponse']['merchantId'],
					"NumberOfBonus": payment['okcResponse']['numberOfBonus'],
					"NumberOfDiscount": payment['okcResponse']['numberOfDiscount'],
					"Pan": payment['okcResponse']['pan'],
					"Rrn": payment['okcResponse']['rrn'],
					"Stan": payment['okcResponse']['stan'],
					"TerminalId": payment['okcResponse']['terminalId'],
					"TotalReceiptAmount": payment['okcResponse']['totalReceiptAmount'],
					"TransFlag": payment['okcResponse']['transFlag'],
					"NumberOfInstallments": payment['okcResponse']['numberOfInstallments'],
					"OkcEndOfDayNo": payment['okcResponse']['zNo'],
					"OkcInvoiceNo": payment['okcResponse']['fNo'],
					"OkcInvoiceType": 'FATURA',
					"OkcSerialNo": window["DeviceIntegrationInstance"].getDeviceSerialNumber(),
					"TotalRemainingAmount": payment['okcResponse']['totalRemainingAmount']
				};
			}
			payments.push(
				{
					"Amount": payment.amount,
					"Id": payment['paymentType']['Id'],
					"Type": payment['paymentType']['Type'],
					"OkcDetail": okcDetail
				}
			);
		})
		this.shoppingCartService.setCustomEndpoint('PostPayment', true);
		let save = this.shoppingCartService.create({
			"InvoiceType": 0,
			"Payments": payments
		});
		save.subscribe(item => {
			this.paymentActionEvent.emit({ action: "redirect", paymentData: [], paymentList: [] });
		}, error => {
			if (error['Message']) {
				// Ödemme alınamsına rağmen fiş iptali olması gerekiyor.
				this.notificationService.add({ text: error['Message'], type: 'danger' })
			}
		})
	}
	private openCalculator() {

	}

	private paymentInputFocusHandler(paymentType, event) {
		paymentType.isFocused = true;
	}

	private paymentInputBlurHandler(paymentType, event) {
		paymentType.isFocused = false;
	}

	wizardActionHandler(event: { action: string, params?: any }) {
		switch (event.action) {
			case "goBack":
				this.previousLevel();
				break;
		}
	}

	nextLevel() {
		this.gotoLevel(Math.min(this.currentLevelIndex + 1, this.levels.length - 1));
	}

	previousLevel() {
		this.gotoLevel(Math.max(this.currentLevelIndex - 1, 0));
	}

	gotoLevel(key: any) {
		if (Number.isInteger(key)) {
			this.currentLevelIndex = key;
		} else {
			let self = this;
			this.levels.forEach(function (item, index) {
				if (item.key == key) {
					self.currentLevelIndex = index;
					return;
				}
			});
		}
		let targetLevel = this.levels[this.currentLevelIndex];
		if (targetLevel != this.currentLevel) {
			this.currentLevel = targetLevel;
		}
	}
	private totalPriceDetailAmount(subtotal) {
		let total = 0;
		subtotal.PriceDetails.forEach(price => {
			if (price.ServiceFee) {
				total += price.ServiceFee;
			}
			if (price.TicketingFee) {
				total += price.TicketingFee;
			}
		})
		total += subtotal.TransactionAmount;
		return this.processAmount(total);
	}
	private totalDiscountAmount(discounts) {
		let amount = 0;
		if (discounts) {
			discounts.forEach(discount => {
				amount += discount['AppliedDiscountAmount'];
			})
		}
		return this.processAmount(amount);
	}
	ngOnDestroy() {
		if (this.subscription) this.subscription.unsubscribe();
	}
	private addProductsToSlip() {
		if (this.currentState && this.currentState.Products && this.currentState.Products.length > 0) {
			let self = this,
				waitFor = 100,
				items = [];
			this.currentState.Products.forEach(item => {
				if (item.Prices.Product) {
					let name = (item.ProductName) ? item.ProductName + ' ' + item.VariantName : item.VariantName;
					items.push({
						Name: name,
						Barcode: item.VariantId,
						Quantity: item.CountOfProductsSelected,
						UnitPrice: item.Prices.Product.Amount,
						TaxRate: item.Prices.Product.Vat,
						TransactionType: this.okcVariables.TransactionType_InfoProcess
					})
				}
				if (item.Prices.ServiceFee) {
					items.push({
						Name: 'Hizmet Bedeli',
						Barcode: '',
						Quantity: item.CountOfProductsSelected,
						UnitPrice: item.Prices.ServiceFee.Amount,
						TaxRate: item.Prices.ServiceFee.Vat,
						TransactionType: this.okcVariables.TransactionType_InfoProcess
					})
				}
				if (item.Prices.TicketingFee) {
					items.push({
						Name: 'Biletleme Bedeli',
						Barcode: '',
						Quantity: item.CountOfProductsSelected,
						UnitPrice: item.Prices.TicketingFee.Amount,
						TaxRate: item.Prices.TicketingFee.Vat,
						TransactionType: this.okcVariables.TransactionType_InfoProcess
					})
				}

			});
			if (this.currentState.TransactionFees.TicketingTrxFee) {
				items.push({
					Name: 'İşlem Bedeli',
					Barcode: '',
					Quantity: 1,
					UnitPrice: this.currentState.TransactionFees.TicketingTrxFee.Amount,
					TaxRate: this.currentState.TransactionFees.TicketingTrxFee.Vat,
					TransactionType: this.okcVariables.TransactionType_InfoProcess
				})
			}

			for (let i: number = 0; i < items.length; i++) {
				if (!self.saleItem(items[i])) {
					this.shoppingCartService.flushCart();
					this.router.navigate(['/boxoffice']);
					break;
				}
			}
			/*			let saleItemHandle = setInterval(function () {
							if (self.saleItem(items[i]) && items.length - 1 != i) {
								i++;
								if (i == 50) {
									clearTimeout(saleItemHandle);
								}
							} else {
								clearTimeout(saleItemHandle);
								this.router.navigate['/boxoffice'];
							}
						}, waitFor);*/
		}
	}
	private saleItem(saleItemRequest) {
		if (saleItemRequest) {
			try {
				let saleItemRequestJson = JSON.stringify(saleItemRequest);
				let saleItemResponse = window['DeviceIntegrationInstance'].saleItem(saleItemRequestJson);
				if (saleItemResponse.result.isTransactionSuccess) {
					if (saleItemResponse.result.deviceResultValue == this.okcVariables.ITEMSALE_SUCCESS) {
						return true;
					}
					else if (saleItemResponse.result.deviceResultValue == this.okcVariables.POS_HAS_UNCOMPLETED_PROCESS) {

						let cancelResult = this.cancelReceiptProcess();
						if (cancelResult == true) {
							return this.saleItem(saleItemRequest);
						}
						else {
							return false;
						}
					}
					else if (saleItemResponse.result.deviceResultValue == this.okcVariables.PAIRING_REQUIRED) {
						let result = window["DeviceIntegrationInstance"].handShake();
						if (result == true) {
							return this.saleItem(saleItemRequest)
						} else {
							this.notificationService.add({ text: 'ÖKC ile eşleşme yapılamadı.', type: 'error' });
							return false;
						}
					}
					else if (saleItemResponse.result.deviceResultValue == this.okcVariables.NO_PAPER) {
						this.notificationService.add({ text: 'Yetersiz Kağıt.', type: 'warning' })
						return false;
					}
					else if (saleItemResponse.result.deviceResultValue == this.okcVariables.CABLE_ERROR) {
						this.notificationService.add({ text: 'İletişim Problemi !! Lütfen OKC Cihazı Kablosunu Kontrol Ediniz.', type: 'warning' });
						return false;
					}
					else if (saleItemResponse.result.deviceResultValue == this.okcVariables.TIMEOUT) {
						this.notificationService.add({ text: 'ÖKC ürün satış işlemi sırasında zaman aşımı, işlem iptal ediliyor.', type: 'warning' });
						this.cancelReceiptProcess();
						return false;
					}
					else {
						this.notificationService.add({ text: 'ÖKC ürün satış işlemi sırasında bir hata oluştu, işlem iptal ediliyor.', type: 'warning' });
						this.cancelReceiptProcess();
						return false;
					}
				}
				else {
					this.notificationService.add({ text: 'ÖKC ürün satış işlemi sırasında bir hata oluştu, işlem iptal ediliyor.', type: 'warning' });
					this.cancelReceiptProcess();
					return false;
				}
			}
			catch (err) {
				alert(err);
			}
		}
	}
	get userLetters(): string {
		let letters: string = "";
		if (this.customer && this.customer["Name"] && this.customer["Name"].length) letters += this.customer["Name"].charAt(0).toUpperCase();
		if (this.customer && this.customer["Surname"] && this.customer["Surname"].length) letters += this.customer["Surname"].charAt(0).toUpperCase();
		return letters;
	}

	private cancelReceiptProcess() {
		/*Cancel Receipt Process Start*/
		let cancelReceiptResponse = window['DeviceIntegrationInstance'].cancelReceipt();
		if (cancelReceiptResponse.result.isTransactionSuccess) {
			if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.CANCEL_RECEIPT_SUCCESS) {
				return true;
			}
			else if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.NO_PAPER) {
				this.notificationService.add({ text: 'Yetersiz Kağıt.', type: 'warning' });
				return false;
			}
			else if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.CABLE_ERROR) {
				this.notificationService.add({ text: 'İletişim Problemi !! Lütfen OKC Cihazı Kablosunu Kontrol Ediniz.', type: 'warning' });
				return false;
			}
			else if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.TIMEOUT) {
				this.notificationService.add({ text: 'İşlem zaman aşımına uğradı.', type: 'warning' })
				return false;
			}
			else {
				this.notificationService.add({ text: 'Varolan İşlem İptal Edilemiyor', type: 'error' });
				return false;
			}
		}
		else {
			this.notificationService.add({ text: 'Varolan işlemi iptal ederken bir hata oluştu !', type: 'error' });
			return false;
		}
		/*Cancel Receipt Process End*/
	}


	private processAmount(amount) {
		return +(Math.floor(Number(amount) * 100) / 100).toFixed(2);
	}
	confirm() {
		this.tetherService.confirm({
			title: 'ÖKC Kasiyer Şifresi',
			description: 'Son İşlem Kopyasını Basmak İçin Kasiyer Şifresini Giriniz',
			feedback: { label: 'KASİYER ŞİFRESİ' },
			confirmButton: { label: 'GİRİŞ' },
			dismissButton: { label: 'VAZGEÇ' }
		}).then(result => {
			this.printLastReceipt(result.feedback);
		}).catch(reason => {

		})
	}

	private printLastReceipt(adminPass: string) {
		let printLastReceiptRequest = {
			AdminPassword: adminPass
		};
		let printLastReceiptRequestJson = JSON.stringify(printLastReceiptRequest);

		let cancelReceiptResponse = window['DeviceIntegrationInstance'].printLastReceipt(printLastReceiptRequestJson);
		if (cancelReceiptResponse.result.isTransactionSuccess) {
			if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.LAST_RECEIPT_SUCCESS) {
				this.notificationService.add({ text: 'Son İşlem Kopyası Başarı ile Basıldı.', type: 'success' });
			}
			else if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.LAST_RECEIPT_FAILED) {
				this.notificationService.add({ text: 'İşlemi Yaparken Bir Hata Oluştu!', type: 'danger' });
			}
			else if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.LAST_RECEIPT_NEED_ADMIN_PASSWORD) {
				this.notificationService.add({ text: 'Lütfen Kasiyer Şifresini Giriniz', type: 'danger' });
			}						
			else if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.NO_PAPER) {
				this.notificationService.add({ text: 'Yetersiz Kağıt.', type: 'warning' });
			}
			else if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.CABLE_ERROR) {
				this.notificationService.add({ text: 'İletişim Problemi !! Lütfen OKC Cihazı Kablosunu Kontrol Ediniz.', type: 'warning' });
			}
			else if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.TIMEOUT) {
				this.notificationService.add({ text: 'İşlem zaman aşımına uğradı.', type: 'warning' })
			}
			else {
				this.notificationService.add({ text: 'İşlemi Yaparken Bir Hata Oluştu!', type: 'danger' });
			}
		}
		else {
			this.notificationService.add({ text: 'İşlemi Yaparken Bir Hata Oluştu!', type: 'danger' });
		}
		/*Cancel Receipt Process End*/
	}

}
