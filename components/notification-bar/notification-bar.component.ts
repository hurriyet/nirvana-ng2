import { Component, OnInit, HostBinding } from '@angular/core';
import { NotificationService } from '../../services/notification.service';

@Component({
  selector: 'app-notification-bar',
  templateUrl: './notification-bar.component.html',
  styleUrls: ['./notification-bar.component.scss']
})

export class NotificationBarComponent implements OnInit {
	@HostBinding("class.c-notification-bar") true;

	private notifications : Array<any>;
	private timeOut : number = 5000;
	private subscribe;
 	constructor( private notificationService : NotificationService) {
		this.notifications = [];
 	}

  	ngOnInit() {
  		let self = this;
  		let subscribe = this.notificationService.notifications.subscribe(item => {
  			if(item){
				item.timer = item.timeOut ? item.timeOut/1000 : self.timeOut/1000;
				item.interval = setInterval(function(){
					item.timer--;
				}, 1000);

				setTimeout(function(){
					//  çıkış animasoyunu
					item.animation = "out";
					setTimeout(function() {
						let indexOf = self.notifications.indexOf(item);
  						self.notifications.splice(indexOf,1);
					}, 500);
					clearInterval(item.interval);
  				}, item.timeOut || this.timeOut);
	  			this.notifications.push(item);
				setTimeout(function() {
					item.animation = "in";
				}, 100);
  			}
  		});
  	}
  	close(notification){
  		let indexOf = this.notifications.indexOf(notification);
  		this.notifications.splice(indexOf,1);
  	}
  	ngOnDestroy(){
  		this.notifications = [];
  		if(this.subscribe) this.subscribe.unsubscribe();
  	}

}
