import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-wizard-header',
  templateUrl: './wizard-header.component.html',
  styleUrls: ['./wizard-header.component.scss'],
  host: {
    class: "c-wizard-header"
  }
})
export class WizardHeaderComponent implements OnInit {
  @Input() currentLevel: number;
  @Input() totalLevel: number;
  @Output() actionEvent = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  emitAction(event: {action: string, params?: any}) {
    this.actionEvent.emit(event);
  }
}
