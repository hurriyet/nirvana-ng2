import { Router } from '@angular/router';
import {
    Component,
    ComponentFactory,
    ComponentRef,
    ComponentFactoryResolver,
    Type,
    ViewContainerRef,
    Injector,
    OnInit
} from '@angular/core';

import { HeaderTitleService } from './../../services/header-title.service';
import { TetherDialog } from '../tether-dialog/tether-dialog';

//Dialog Contents...
import { HeaderSearchBarComponent } from '../header-search-bar/header-search-bar.component';
import { ModalSearchBoxComponent } from '../modal-search-box/modal-search-box.component';

import { ContextMenuComponent } from '../context-menu/context-menu.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    entryComponents: [HeaderSearchBarComponent, ContextMenuComponent, ModalSearchBoxComponent],
    host: {
        class: "c-header"
    }
})
export class HeaderComponent implements OnInit {

    private username: string;
    private firstname: string;
    private lastname: string;
    private images: string;
    private letters: string;

    get fullname():string {
        return this.firstname + " " + this.lastname;
    }

    get title() {
        return this.headerTitleService.getTitle();
    }

    constructor(
        private resolver: ComponentFactoryResolver,
        private injector: Injector,
        private viewContainer: ViewContainerRef,
        private router: Router,
        private tetherService: TetherDialog,
        private headerTitleService: HeaderTitleService) {

    }

    ngOnInit() {
        this.username = localStorage.getItem('nirvanaUserName');
        this.firstname = localStorage.getItem('nirvanaFirstName');
        this.lastname = localStorage.getItem('nirvanaLastName');
        this.images = localStorage.getItem('nirvanaImages');
        if(this.images == 'null') this.images = null;
        if(this.firstname == 'null') this.firstname = null;
        if(this.lastname == 'null') this.lastname = null;

        if(this.firstname && this.lastname) this.letters = this.firstname.charAt(0) + this.lastname.charAt(0);
        if(this.username && this.username.length > 1 && !this.letters) this.letters = this.username.charAt(0).toUpperCase() + this.username.charAt(1).toLocaleLowerCase();
        (this.firstname) ? this.firstname.split(/\b/)[0] : this.firstname;
    }

    openUserActions(event) {
        let component: ComponentRef<ContextMenuComponent> = this.resolver.resolveComponentFactory(ContextMenuComponent).create(this.injector)
        let instance: ContextMenuComponent = component.instance;

        instance.data = [
            {action: "gotoLink", icon: 'exit_to_app', label: 'Oturumu Kapat', params: "/logout"}
        ]
        this.tetherService.context(component,
            {
                target: event.target,
                attachment: "top right",
                targetAttachment: "top right",
                targetOffset: '-13px 0px'
            }).then(result => {
                switch(result['action']) {
                    case "gotoLink":
                        this.router.navigate([result['params']])
                    break;
                }
            }).catch(reason => {

            });
    }

    showUserDetail(event) {

    }
    // openHeaderSearchBar(event) {
    //     this.tetherService.modal(this.resolver.resolveComponentFactory(HeaderSearchBarComponent),
    //         {
    //             escapeKeyIsActive: false,
    //             dialog: {
    //                 style: {
    //                     width: "40vw",
    //                     height: "50vh"
    //                 }
    //             },
    //             target: event.target,
    //             attachment: "top right",
    //             targetAttachment: "top right"
    //         }).then(result => {
    //             console.log("promise result : ", result);
    //         }).catch(reason => {
    //             console.log("dismiss reason : ", reason);
    //         });
    // }

    // openHelpDrawer() {
    //     this.tetherService.drawer(this.resolver.resolveComponentFactory(HeaderSearchBarComponent),
    //         {

    //         }).then(result => {
    //             console.log("promise result : ", result);
    //         }).catch(reason => {
    //             console.log("dismiss reason : ", reason);
    //         });
    // }

    // openNotificationModal() {
    //     let component: ComponentRef<ModalSearchBoxComponent> = this.resolver.resolveComponentFactory(ModalSearchBoxComponent).create(this.injector)
    //     let instance: ModalSearchBoxComponent = component.instance;

    //     instance.title = "Notification Ara"
    //     instance.settings = {
    //         search: {
    //             placeholder: 'Eklemek istediğiniz notification ismini yazın',
    //             feedback: {
    //                 title: 'Aramanız ile eşleşen notification kaydı bulunamadı',
    //                 description: 'Arama kriterlerini değiştirerek yeniden deneyebilir ya da yeni notification ekleyebilirsiniz',
    //                 action: {
    //                     action: 'createNewNotification',
    //                     label: 'YENİ NOTIFICATION OLUŞTUR',
    //                     params: { link: '/notification/create' }
    //                 }
    //             }
    //         }
    //     }
    //     instance.resultEvent.subscribe(result => {
    //         console.log("Result Event Hanlder : ", result);
    //     })

    //     this.tetherService.modal(component,
    //         {
    //             dialog: {
    //                 class: "c-tether-dialog__content--half"
    //             },
    //             //dismissConfirm: true,
    //             //dismissConfirmMessage: "İşleminizi kaydetmeden çıkmak istiyor musunuz?"
    //         }).then(result => {
    //             console.log("promise result : ", result);
    //         }).catch(reason => {
    //             console.log("dismiss reason : ", reason);
    //         });
    // }
}
