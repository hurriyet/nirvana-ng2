import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { MockService } from '../../services/mock.service';

@Component({
  selector: 'app-narrow-col-basket-item',
  templateUrl: './narrow-col-basket-item.component.html',
  styleUrls: ['./narrow-col-basket-item.component.scss']
})
export class NarrowColBasketItemComponent implements OnInit {
	private model : any;
  @HostBinding('class.c-narrow-col-basket-item') true;
  @Output() actionEvent: EventEmitter<any> = new EventEmitter();
  @Input()
  public set data(value)
  {
   	this.model = value;
  }
  constructor(mockService: MockService) {
    //mockService.fillInputs(this, null);
  }

  ngOnInit() {
  }

  countChangeHandler(model, product, value) {
    this.actionEvent.emit( {
      action: "countChange",
      modelId: model.id,
      variantId: product.id,
      changedValue: value
    } );
  }

  updateProduct(model, product) {
    this.actionEvent.emit( {
      action: "updateProduct",
      modelId: model.id,
      variantId: product.id
    } );
  }

  removeProduct(model, product) {
    this.actionEvent.emit( {
      action: "removeProduct",
      modelId: model.id,
      variantId: product.id
    } );
  }

}
