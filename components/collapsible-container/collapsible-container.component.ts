import { GridListComponent } from './../grid-list/grid-list.component';
import { Component, OnInit, HostBinding, HostListener, Input, Renderer, ElementRef, ContentChildren, ViewChild } from '@angular/core';

declare var $:any;

@Component({
  selector: 'app-collapsible-container',
  templateUrl: './collapsible-container.component.html',
  styleUrls: ['./collapsible-container.component.scss'],
})

export class CollapsibleContainerComponent implements OnInit {
  @ViewChild('content') content:ElementRef;
  @ViewChild('aside') aside:ElementRef;

  @ContentChildren(GridListComponent) grids: GridListComponent[]

  @HostListener('window:resize')
  resizeHandler() {
    this.resize();
  }

  @HostBinding('class.c-collapsible-container') true;

  @HostBinding('class.c-collapsible-container--gray')
  private isGray: boolean;

  @HostBinding('class.c-collapsible-container--toggle-gray')
  private isToggleGray: boolean;

  @HostBinding('class.main-loader')
  @Input() isLoading: boolean;
  
  @Input() asideWidth: number = 260;
  @Input() asideHasNoPadding: boolean = false;
  @Input() contentHasNoPadding: boolean = false;
  @Input() hasOverlay: boolean;
  
  @Input() set isAsideOpen(value: boolean) {
    this.asideOpened = value;
    if(this.asideOpened) {
      this.renderer.setElementStyle(this.aside.nativeElement, 'width', this.asideWidth + "px");
    }else{
      this.renderer.setElementStyle(this.aside.nativeElement, 'width', "20px");
    }
    this.resize();
  }

  get isAsideOpen():boolean { return this.asideOpened };

  @Input() set theme(value:string) {
    this.isGray = value == "gray";
    this.isToggleGray = value == "toggle-gray";
  }

  private asideOpened: boolean;

  constructor(private renderer: Renderer, private elementRef: ElementRef) { }

  ngOnInit() {

  }

  ngAfterContentInit(){
    this.resize();
  }

  resize() {
    this.renderer.setElementStyle(this.elementRef.nativeElement, 'min-height', (window.innerHeight - this.elementRef.nativeElement.offsetTop).toString()+"px");
  }

  toggleAside(){
    this.isAsideOpen = !this.asideOpened;
    
    setTimeout(function() {
      $(window).trigger('resize');
      window.dispatchEvent(new Event('resize'));  
    }, 500);
    if(this.grids) this.grids.forEach( grid => grid.render() )
  }

}
