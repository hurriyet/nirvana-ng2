import { Observable } from 'rxjs/Observable';
import { Component, OnInit, HostBinding, ViewChild, EventEmitter, Output, Input,ContentChild,OnDestroy } from '@angular/core';
import { TetherDialog } from '../tether-dialog/tether-dialog';
import { TypeaheadComponent } from '../typeahead/typeahead.component';

@Component({
  selector: 'app-search-customer',
  templateUrl: './search-customer.component.html',
  styleUrls: ['./search-customer.component.scss']
})
export class SearchCustomerComponent implements OnInit,OnDestroy {
  @ViewChild(TypeaheadComponent) typeahead: TypeaheadComponent;
  @HostBinding('class.c-modal-search-box.c-search-customer') true;

  @HostBinding('class.c-modal-search-box--narrow') private isNarrow: boolean;

  @Output() searchEvent: EventEmitter<any> = new EventEmitter();
  @Output() resultEvent: EventEmitter<any> = new EventEmitter();
  @Output() dismissEvent: EventEmitter<any> = new EventEmitter();
  @Output() actionEvent: EventEmitter<Object> = new EventEmitter();
  @Output() formSendEvent: EventEmitter<any> = new EventEmitter();

  @Input() title: string;
  @Input() settings: {
    search: {
      placeholder: string,
      feedback: {
        title: string,
        description: string,
        action?: {action: string, label: string, params?: Object},
        icon?: {type: string, name: string}
      }
    }
  }

  @Input() presets: Observable<{}> | Observable<{ title: string, list: {id: any, title: string, icon?: string, description?:string, params?: any}[] }[]>;

  @Input() searchResults: Observable<{}> | Observable<{ title: string, list: { title?: string, icon?: string, MemberId: any,Name: string,Surname: string,DateOfBirth?: any,Gender: string,PhoneNumber: string,Email: string,ProfilePicture?: string, }[]}[]>;

  // @Input() searchResults: Observable<{}> | Observable<{ title: string, list: {id: any, title: string, icon?: string, description?:string, params?:any,}[] }[]>;

  @Input() isPromising: boolean = false;

  @Input() set theme(value:string) {
    this.isNarrow = value == "narrow";
  }
  private name:any;
  private surname:any;
  private email:any;
  private phone:any;
  private city?:any;
  private town?:any;
  private formData = [];
  private queryType : number = 1;
  private get titleCase():string {
    if(this.isNarrow) return this.title.toUpperCase();
    return this.title.length > 0 ? this.title.replace(/\w\S*/g, (txt => txt[0].toUpperCase() + txt.substr(1).toLowerCase() )) : '';
  }

  private tetherService: TetherDialog;
  private addUser:boolean = false;
  private resultEventSubscription:any;
  private dismissEventSubscription:any;
	private get isValid():boolean {
		if(this.email && this.surname && this.phone && this.email){
			return true;
		}else{
			return false
		}
	};
  constructor(tetherService: TetherDialog) {
    this.tetherService = tetherService;
  }

  ngOnInit() {

  }
  addingAllowed(event) {
    if(event.action === 'add'){
      this.addUser =true;
    }
    else {
      this.addUser = false;
    }
  }
  resultHandler(event) {
  	console.log(event)
    this.resultEvent.emit(event);
    this.tetherService.close(event);
  }

  dismissHandler(event) {
    this.dismissEvent.emit(event);
    this.tetherService.dismiss(event);
  }

  inputHandler($event,field) {
    switch(field){
      case 'name':
        this.name = $event;
      break;
      case 'surname':
        this.surname = $event;
      break;
      case 'email':
        this.email = $event;
      break;
      case 'phone':
        this.phone = $event;
    }
  }

  townEvent(event) {
    console.log("TownEvent",event);
    this.city = event.city;
    this.town = event.town;
  }

  sendFormData() {
      /**
       * Bunun Emitini Narrow Client Display Componentinde  satır 99 da yakalıyorum.
       */
    this.formSendEvent.emit({
      // city ve town uda ekliyorum
      city:this.city,
      town:this.town,
      title: this.name + ' ' + this.surname,
      Name: this.name,
      Surname: this.surname,
      PhoneNumber:this.phone,
      Email:this.email
    });


      /**
       * TimeBomb Servisten Success Gelmiş Gibi Davranıyor.Sonra
       */
      setTimeout(() => {
        this.tetherService.dismiss();
      },666)

  }

  // Emit edileni unsubscribe etmek için servise bağlandığında ihtiyaç kalmayacaktır
  ngOnDestroy() {
    this.formSendEvent.unsubscribe();
  }
  search(event){
  	if(event){
	  	this.searchEvent.emit({queryType:this.queryType, value:event});
  	}
  }
  searchType(type){
  	this.queryType = type;
  }

}
