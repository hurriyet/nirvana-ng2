import { Component, ComponentFactory, ComponentRef, ComponentFactoryResolver,EventEmitter,Injector, OnInit, Output, HostBinding,AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GetIconPipe } from '../../pipes/get-icon.pipe';
import { Router, ActivatedRoute } from '@angular/router';
import { TetherDialog } from '../../components/tether-dialog/tether-dialog';
import { ContextMenuComponent } from '../../components/context-menu/context-menu.component';
import { MenuItemService } from './../../services/menu-item.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
  inputs: [
    'isSideMenuCollapsed',
  ],
  entryComponents:[ContextMenuComponent]
})
export class MainMenuComponent implements OnInit {
	@HostBinding('class.c-main-menu') true;
	@HostBinding('class.c-main-menu--collapsed') isSideMenuCollapsed:boolean;
	@Output() toggleSideMenu : EventEmitter<boolean> = new EventEmitter<boolean>();
    private itemsSet = [];
	private items:Array<Object>;
 	constructor(
        private router: Router,
        private route: ActivatedRoute,
        private resolver: ComponentFactoryResolver,
        private injector: Injector,
        private tetherService: TetherDialog,
        private menuItemService : MenuItemService
     ) {
		this.menuItemService.mainMenu.subscribe(items=>{
			if(items && items.length > 0){
				this.items = items;
			}
		})
 	}
	ngOnInit() {
  	}

	ngAfterViewInit(){

		/*
		this.items = [
			{label: "Etkinlik Yönetimi", icon: "event",submenu:[
            {
                action: 'link', // link or edit or create
                label:'Etkinlikler',
                icon:"event",
                routerLink:"/events/index"
            },
            {
                 action: 'link',
                label:"Performanslar",
                icon:"performance",
                routerLink:"/performances"
            },
            {
                action: 'link',
                label:"Performans Biletleri",
                icon: "ticket",
                routerLink:"/performance"
            }
            ]},
			{label: "Mekanlar", icon: "location", routerLink: "/venues"},
			{label: "Sanatçılar", icon: "microphone", routerLink: "/performers"},
            {label: "Satışlar/Kullanıcılar", icon: "cash-register",submenu:[{
                action: 'link',
                label:"Müşteriler",
                icon:"customers",
                routerLink:"",
            },
            {
                action: 'link',
                label:"İşlemler",
                icon:"transaction_history",
                routerLink:""
            },
            {
                action: 'link',
                label:"Satış Noktalar",
                icon:"add_location",
                routerLink:""
            },
            {
                action: 'link',
                label:"Satış",
                icon:"credit_card",
                routerLink:""
            },
            {
                action: 'link',
                label:"Kasa İşlemleri",
                icon:"cash-register",
                routerLink:""
            }
            ]},

			{label: "Nirvana Yönetimi", icon: "sim_card", routerLink: "",submenu:[{
                action: 'link',
                label:'Firmalar',
                icon:"polymer",
                routerLink:""
            },
            {
                action: 'link',
                label:"Kullanıcı Gurupları",
                icon:"group_add",
                routerLink:""
            },
            {
                action: 'link',
                label:"Kullanıcılar",
                icon:"customers",
                routerLink:""
            },
            {
                action: 'link',
                label:"Gişe Tanımlamaları",
                icon:"fingerprint",
                routerLink:""
            }
            ]},
			{label: "Ürünler", icon:"ticket", routerLink: "/products"},
			{label: "Box Office", icon:"cash-multiple", routerLink: "/boxoffice"},
			{label: "İçerik Yönetimi", icon:"description", routerLink: ""}, // mock linkler
			{label: "Gişe Yönetimi", icon:"account_balance", routerLink: ""}, // mock linkler
			{label: "Kampanyalar", icon:"settings_input_antenna", routerLink: ""} // mock linkler
		]
*/
        // let datas = this.items.map(result => { return result['submenu'][0]['label']})[0];
        //this.itemsSet.push({label:"etkinliks",icon:"event"});

	}

	openEventsContextMenu(e, index) {
        let component: ComponentRef<ContextMenuComponent> = this.resolver.resolveComponentFactory(ContextMenuComponent).create(this.injector)
        let instance: ContextMenuComponent = component.instance;
        let iconPipe:GetIconPipe = new GetIconPipe();
        instance.actionEvent.subscribe(action => {
            //this.eventService[action.action](event, action.parameters);
        });
        // let willChangeEventSaleStatus = event.status === 6 ? 2 : 6;
        // let willChangeEventPublishStatus = event.status === 8 ? 1 : 8;

        let dataso = this.items[index]['submenu'].length;
        this.itemsSet = [];
        for(let s = 0; s < this.items[index]['submenu'].length; s++){
            this.itemsSet.push(this.items[index]['submenu'][s]);
            instance.title = this.items[index]['label'];
        }
        instance.data = this.itemsSet;

        //console.log("Target:",e.target);
        this.tetherService.tether(component,
            {
                overlay: {},
                target: e.target,
                attachment: "top left",
                targetAttachment: "top left",
                targetOffset: '0px 0px',
                dialog:{
                    style: {
                        width:"15vw",
                        height: "auto"
                    }
                }
            }).then(result => {

                if(result) {
                    switch(result['action']){
                        case('link'):
                            this.router.navigate([result['routerLink']]);
                        break;
                    }
                }
                // if (result) {
                //     switch (result['action']) {
                //         case "editEvent":
                //             if (result['parameters'] && result['parameters']['eventId']) {
                //                 this.router.navigate(['/event', result['parameters']['eventId'], 'edit']);
                //             }
                //             break;
                //     }
                // }
            }).catch(reason => {
                console.log("dismiss reason : ", reason);
                this.itemsSet.length = 0;
            });
    }

  	toggle(){
	    this.isSideMenuCollapsed = !this.isSideMenuCollapsed;
	    this.toggleSideMenu.emit(this.isSideMenuCollapsed);
  	}
}
