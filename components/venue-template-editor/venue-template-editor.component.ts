import { AuthenticationService } from './../../services/authentication.service';
import { Router } from '@angular/router';
import { Component, OnInit, HostBinding, Input, Renderer, ElementRef, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-venue-template-editor',
  templateUrl: './venue-template-editor.component.html',
  styleUrls: ['./venue-template-editor.component.scss']
})
export class VenueTemplateEditorComponent implements OnInit {
  
  static readonly EVENT_READY: string = "JSB:EVENT_READY";
  static readonly EVENT_SELECT: string = "JSB:EVENT_SELECT";
  static readonly EVENT_SAVE_SUCCESS: string = "JSB:EVENT_SAVE_SUCCESS";
  static readonly EVENT_SAVE_FAIL: string = "JSB:EVENT_SAVE_FAIL";
  
  static readonly COMMAND_CHANGE_ALLOWED_BLOCKS: string = "JSB:COMMAND_CHANGE_ALLOWED_BLOCKS";
  static readonly COMMAND_CHANGE_ALLOWED_SEATS: string = "JSB:COMMAND_CHANGE_ALLOWED_SEATS";
  static readonly COMMAND_CHANGE_SELECTABLES: string = "JSB:COMMAND_CHANGE_SELECTABLES";
  static readonly COMMAND_FOCUS_BLOCK: string = "JSB:COMMAND_FOCUS_BLOCK";
  static readonly COMMAND_NEW: string = "JSB:COMMAND_NEW";
  static readonly COMMAND_SAVE: string = "JSB:COMMAND_SAVE";
  static readonly COMMAND_SET_OBJECT_DATA: string = "JSB:COMMAND_SET_OBJECT_DATA";
  static readonly COMMAND_SELECT_SEATS: string = "JSB:COMMAND_SELECT_SEATS";
  
  static readonly ROLE_VENUE: string = "venue_editor";
  static readonly ROLE_PERFORMANCE: string = "performance_editor";
  static readonly ROLE_PRODUCT: string = "product_editor";
  static readonly ROLE_SALES_MOBILET: string = "sales_screen_mobilet";
  static readonly ROLE_SALES_BACKOFFICE: string = "sales_screen_box_office";
  static readonly ROLE_CANCEL_BLOCK: string = "cancel_block";
  static readonly ROLE_GROUP_SALES: string = "group_sales";

  @HostBinding('class.c-venue-template-editor') true;

  @Output() actionEvent: EventEmitter<{action: string, data?: any}> = new EventEmitter();
  @Output() editorEvent: EventEmitter<any> = new EventEmitter();

  @Input() role: string;
  @Input() venueId: any = null;
  @Input() templateId: any = null;
  @Input() performanceId: any = null;
  @Input() productId: any = null;
  @Input() firmCode: any = null;
  @Input() channelCode: any = null;
  
  @Input() templateName: string = null;
  
  private environmentName: string = environment['name'];
  
  public venuedScript;
  public venued;
  public venuedConf;

  public jsb;
  private jsbScript;
  private isLoading: boolean = false;
  private selectedSeats: {}[];

  constructor( 
    private authenticationService: AuthenticationService,

    private router: Router,
    private element: ElementRef,
    private renderer: Renderer,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    if(this.jsbScript) {
      this.element.nativeElement.removeChild(this.venuedScript);
      this.venuedScript = null;
      delete window["_venued_jsb"];
    }
  }

  ngAfterViewInit(){
    let conf: {role: string, env: string, accessToken: any, venueId?: any, templateId?: any, performanceId?: any, productId?: any, firmCode?:any, channelCode?: any};
    conf = {
      role: this.role,
      env: this.environmentName,
      accessToken: this.authenticationService.getToken()
    };
    
    let hasError: boolean = false;
    switch(this.role) {
      case VenueTemplateEditorComponent.ROLE_VENUE:
        if(this.venueId) {
          conf.venueId = this.venueId;
          conf.templateId = this.templateId || -1
        }else{hasError = true};
      break;
      case VenueTemplateEditorComponent.ROLE_PRODUCT:
        if(this.performanceId) {
          conf.performanceId = this.performanceId;
          conf.productId = this.productId;
        }else{hasError = true};
      break;
      case VenueTemplateEditorComponent.ROLE_PERFORMANCE:
      case VenueTemplateEditorComponent.ROLE_CANCEL_BLOCK:
      case VenueTemplateEditorComponent.ROLE_GROUP_SALES:
        if(this.performanceId) {
          conf.performanceId = this.performanceId;
        }else{hasError = true};
      break;
      case VenueTemplateEditorComponent.ROLE_SALES_BACKOFFICE:
      case VenueTemplateEditorComponent.ROLE_SALES_MOBILET:
        if(this.performanceId) {
          conf.performanceId = this.performanceId;
          conf.firmCode = this.firmCode;
          conf.channelCode = this.channelCode;
        }else{hasError = true};
      break;
      
    }

    if(hasError) {
      alert("Venue Editor " + this.role +" rolü için gerekli alanlar eksik");
    }else{
      window["_venued_conf"] = conf;
      this.setupVenueEditor();
    }
  }

  public save(payload: {}) {
    this.sendToApp(VenueTemplateEditorComponent.COMMAND_SAVE, payload);
  }

  public setSeat(payload: {}) {
    this.sendToApp(VenueTemplateEditorComponent.COMMAND_SET_OBJECT_DATA, payload);
  }

  public selectSeats(payload: number[]) {
    this.sendToApp(VenueTemplateEditorComponent.COMMAND_SELECT_SEATS, payload);
  }

  public sendToApp(type: string, payload: any) {
    if(this.venued) this.venued.sendToApp({type: type, payload: payload});
  }

  private setupVenueEditor(){
    let self = this;
    this.isLoading = true;
    this.venuedScript = document.createElement('script');
    this.venuedScript.type = "text/javascript";
    this.venuedScript.id = "venued-loader";

    this.venuedScript.onload = function(){
      self.venued = window["_venued_jsb"];
      //console.log("loaded : ", this, window["_venued_jsb"]);
    }

    window["_venued_jsb_receiver"] = function(event) {
      self.editorEvent.emit(event);
    }

    this.venuedScript.src = environment.venued.jsPath;
    this.element.nativeElement.appendChild(this.venuedScript);
  }
}
