import { Component, OnInit, ElementRef, HostBinding, ViewChild, Renderer, Input } from '@angular/core';

@Component({
    selector: 'app-collapsible-sidebar',
    templateUrl: './collapsible-sidebar.component.html',
    styleUrls: ['./collapsible-sidebar.component.scss']
})
export class CollapsibleSidebarComponent implements OnInit {
    @HostBinding('class') class = 'o-collapsible-container__aside';
    @HostBinding('class.o-collapsible-container__aside--expanded') @Input() isVisible: boolean = false;
    @HostBinding('class.o-collapsible-container__aside--padding') isReset(): boolean { return this.theme == 'padding' };

    @Input() theme: string;

    constructor(private renderer: Renderer) { }

    ngOnInit() {
    }
    toggle() {
        this.isVisible = !this.isVisible;
    }

}
