import { Component, OnInit, ViewChild, ElementRef, HostBinding, Input, Renderer, HostListener } from '@angular/core';

@Component({
  selector: 'app-split-container',
  templateUrl: './split-container.component.html',
  styleUrls: ['./split-container.component.scss']
})
export class SplitContainerComponent implements OnInit {
  @ViewChild('fluid') fluid:ElementRef;
  @ViewChild('fixed') fixed:ElementRef;

  @HostBinding('class.c-split-container') true;

  @HostListener('window:resize')
  resizeHandler() {
    this.resize();
  }

  @Input() fixedFloat: string;
  @Input() hasBorder: boolean = true;
  @Input() fixedWidth: number = 400;

  constructor(private renderer: Renderer, private element: ElementRef) { }

  ngOnInit() {
    
  }

  resize() {
    this.renderer.setElementStyle(this.fluid.nativeElement, 'min-height', (window.innerHeight - this.element.nativeElement.offsetTop).toString()+"px");
    this.renderer.setElementStyle(this.fixed.nativeElement, 'height', (window.innerHeight - this.element.nativeElement.offsetTop).toString()+"px");
  }

}
