import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { MockService } from '../../services/mock.service';

@Component({
  selector: 'app-tab-bar',
  templateUrl: './tab-bar.component.html',
  styleUrls: ['./tab-bar.component.scss']
})
export class TabBarComponent implements OnInit {
	// @Input() tabs:Array<any>;
	@HostBinding('class.c-tab-bar') true;
	@HostBinding('class.c-tab-bar--white') get themeIsLight():boolean { return this.theme === 'light' };
	
	@Input() theme:string;
	@Input() data: Object[];
	

	get items():Object[] { return this.data };

	constructor(mockService: MockService) { 
		mockService.fillInputs(this, null);
	 }

	ngOnInit() {
	}

}
