import { TextInputComponent } from './../text-input/text-input.component';
import { ExpandableBlockComponent } from './../expandable-block/expandable-block.component';
import { AddVariantPriceBoxComponent } from './../../common/add-variant-price-box/add-variant-price-box.component';
import { VariantPrice } from './../../models/variant-price';
import { TetherDialog } from './../tether-dialog/tether-dialog';
import { AddVariantBoxComponent } from './../../common/add-variant-box/add-variant-box.component';
import { Variant } from './../../models/variant';
import { PriceList } from './../../models/price-list';
import { Component, OnInit, HostBinding, Input, Output, EventEmitter, ComponentRef, ComponentFactoryResolver, Injector, ChangeDetectorRef, ViewChild } from '@angular/core';

import * as moment from 'moment';
declare var $: any;

@Component({
  selector: 'app-product-price-block',
  templateUrl: './product-price-block.component.html',
  styleUrls: ['./product-price-block.component.scss'],
  entryComponents: [AddVariantBoxComponent, AddVariantPriceBoxComponent]
})
export class ProductPriceBlockComponent implements OnInit {
  @ViewChild(ExpandableBlockComponent) expandableBlock: ExpandableBlockComponent;
  @ViewChild(TextInputComponent) firstTextInput: TextInputComponent;

  @HostBinding('class.c-product-price-block') true;

  @Output() actionEvent : EventEmitter<Object> = new EventEmitter<Object>();
  @Output() changeEvent : EventEmitter<PriceList> = new EventEmitter<PriceList>();

  @Input() priceList: PriceList

  @Input() contextMenuData: {action: string, label: string, icon?: string, params?: any, group?: any }[];

  private addVariantBox: AddVariantBoxComponent;
  private addVariantPriceBox: AddVariantPriceBoxComponent;

  private validation: {
		Name: { isValid: any, message: string },
		BeginDate: { isValid: any, message: string },
		EndDate: { isValid: any, message: string },
		NominalPrice: { isValid: any, message: string }
	} = {
		Name: {
			message: "Fiyat Blok Adı zorunludur.",
			isValid(): boolean {
				return this.priceList && this.priceList.Localization && this.priceList.Localization.Tr && this.priceList.Localization.Tr.Name && this.priceList.Localization.Tr.Name.length > 0;
			}
		},
		BeginDate: {
			message: "Fiyat başlangıç tarihi zorunludur",
			isValid():boolean {
				return this.priceList && this.priceList.BeginDate && moment(this.priceList.BeginDate).isValid();
			}
		},
		EndDate: {
			message: "Fiyat bitiş tarihi zorunludur",
			isValid():boolean {
				return this.priceList && this.priceList.EndDate && moment(this.priceList.EndDate).isValid();
			}
		},
		NominalPrice: {
			message: "Fiyat bilgisi zorunludur!",
			isValid():boolean {
				return this.priceList && this.priceList.NominalPrice > 0;
			}
		}
	};

	public get isValid():boolean {
		if( this.priceList && this.validation
			&& this.validation.Name.isValid.call(this)
			&& this.validation.BeginDate.isValid.call(this)
			&& this.validation.EndDate.isValid.call(this)
			&& this.validation.NominalPrice.isValid.call(this)
			){
			return true;
		}else{
			// if( this.priceList && this.validation) console.log(
			// 	this.validation.Name.isValid.call(this),
			// 	this.validation.BeginDate.isValid.call(this),
			// 	this.validation.EndDate.isValid.call(this),
      //   this.validation.NominalPrice.isValid.call(this)
			// )
			return false
		}
	};

  constructor(
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private tetherService: TetherDialog,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit() {
    //this.firstTextInput.focus();
  }

  public collapse() {
    if(this.expandableBlock) this.expandableBlock.collapse();
  }

  public expand () {
    if(this.expandableBlock) this.expandableBlock.expand();
  }

  private emitAction(event) {
    if(!event.params) event.params = {};
    event.params.priceList = this.priceList;
    this.actionEvent.emit(event);
  }

  private inputChangeHandler(event, name:string) {
    switch(name) {
      case "Name":
      case "Info":
        if(!this.priceList.Localization) this.priceList.Localization = {};
        if(!this.priceList.Localization.Tr) this.priceList.Localization.Tr = {};
        this.priceList.Localization[name] = event;
        this.priceList.Localization.Tr[name] = event;
      break;
      default:
        this.priceList[name] = event;
      break;
    }
    this.changeEvent.emit(this.priceList);
  }

  private openVariantContextMenu(event, variant: Variant) {
    this.tetherService.context({
      title: "İŞLEMLER",
      data: [
        {action: "edit", label: "Varyantı Düzenle", params: {variant: variant}},
        {action: "remove", label: "Varyantı Sil", params: {variant: variant}}
      ]
    }, {
      target: event.target
    }).then( result => this.variantContextActionHandler(result)).catch( reason => {});
  }

  private openAddVariantBox(variant: Variant) {
    let component: ComponentRef<AddVariantBoxComponent> = this.resolver.resolveComponentFactory(AddVariantBoxComponent).create(this.injector);
    this.addVariantBox = component.instance;
    this.addVariantBox.priceList = this.priceList;
    this.addVariantBox.variant = variant;

    this.tetherService.modal(component, {dialog: {style: {width: "50vw", height: 'auto'}}}).then(
      result => {
        let variant:Variant = result as Variant;
        if(!this.priceList.Variants || this.priceList.Variants.indexOf(variant) < 0) {
          this.addNewVariant(variant);
        }
      }
    ).catch( reason => {});
  }

  private addNewVariant(variant: Variant) {
    if(!this.priceList.Variants) this.priceList.Variants = [];
    variant.ProductId = this.priceList.ProductId;
    variant.PriceListId = this.priceList.Id;
    this.priceList.Variants.push(variant);
    this.changeEvent.emit(this.priceList);
  }
  
  private variantContextActionHandler(event) {
    switch(event.action) {
      case "edit":
        this.openAddVariantBox(event.params.variant);
      break;
      case "remove":
      case "delete":
        let existVariant = this.priceList.Variants.find( item => item.Id == event.params.variant.Id);
        if(existVariant) this.priceList.Variants.splice(this.priceList.Variants.indexOf(event.params.variant), 1);
      break;
    }
  }

  private openVariantPriceContextMenu(event, variantPrice: VariantPrice, variant: Variant) {
    this.tetherService.context({
      title: "İŞLEMLER",
      data: [
        {action: "edit", label: "Satış Kanalı Düzenle", params: {variantPrice: variantPrice, variant: variant}},
        {action: "remove", label: "Satış Kanalı Sil", params: {variantPrice: variantPrice, variant: variant}}
      ]
    }, {
      target: event.target
    }).then( result => this.variantPriceContextActionHandler(result)).catch( reason => {});
  }

  private openAddVariantPriceBox(variantPrice: VariantPrice, variant: Variant, isEditMode: boolean = false) {
    let component: ComponentRef<AddVariantPriceBoxComponent> = this.resolver.resolveComponentFactory(AddVariantPriceBoxComponent).create(this.injector);
    this.addVariantPriceBox = component.instance;
    this.addVariantPriceBox.variant = variant;
    this.addVariantPriceBox.variantPrice = variantPrice;
    this.addVariantPriceBox.isEditMode = isEditMode;

    this.tetherService.modal(component, {dialog: {style: {width: "50vw", height: 'auto'}}}).then(
      result => {
        let variantPrices: VariantPrice[] = result.variantPrices as VariantPrice[];
        let variant: Variant = result.variant as Variant;
        variantPrices.forEach( variantPrice => this.addNewVariantPrice(variantPrice, variant));
      }
    ).catch( reason => {});
  }

  private addNewVariantPrice(variantPrice: VariantPrice, variant: Variant) {
    if(!variant.Prices) variant.Prices = [];
    let existVariantPrice = variant.Prices.find( item => item.SalesChannelId == variantPrice.SalesChannelId);
    variantPrice.VariantId = variant.Id;
    variantPrice.BeginDate = this.priceList.BeginDate;
    variantPrice.EndDate = this.priceList.EndDate;
    if(existVariantPrice) {
      $.extend(true, existVariantPrice, variantPrice);
    }else{
      variant.Prices.push(variantPrice);
    }
  }
  
  private variantPriceContextActionHandler(event) {
    switch(event.action) {
      case "edit":
        this.openAddVariantPriceBox(event.params.variantPrice, event.params.variant, true);
      break;
      case "remove":
      case "delete":
        let existVariantPrice = event.params.variant.Prices.find( item => item.Id == event.params.variantPrice.Id);
        if(existVariantPrice) event.params.variant.Prices.splice(event.params.variant.Prices.indexOf(event.params.variantPrice), 1);
      break;
    }
  }

}
