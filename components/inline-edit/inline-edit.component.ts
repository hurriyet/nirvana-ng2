import { Component, OnInit, Input, Output, HostBinding, EventEmitter, ElementRef, ViewChild, Renderer } from '@angular/core';
import { MockService } from '../../services/mock.service';
import { Observable } from 'rxjs/Rx';

import { TextInputComponent } from '../text-input/text-input.component';

@Component({
  selector: 'app-inline-edit',
  templateUrl: './inline-edit.component.html',
  styleUrls: ['./inline-edit.component.scss']
})
export class InlineEditComponent implements OnInit {
  @ViewChild(TextInputComponent) textInput: TextInputComponent;

  @HostBinding('class.c-inline-edit') true ;

  @HostBinding('class.c-inline-edit--error')

  @Input() isError: boolean = false;

  @HostBinding('class.c-inline-edit--editable')
  @Input() isEditing: boolean = false;

  @HostBinding('class.c-inline-edit--processing')
  @Input() isPromising: boolean = false;

  @HostBinding('class.c-inline-edit--underline')
  themeIsUnderline: boolean = false;

  @HostBinding('class.c-inline-edit--dirty')
  get isDirty():boolean {
    return this.value && this.value.length > 0;
  }

  @Output() changeEvent: EventEmitter<string> = new EventEmitter<string>();
  @Output() onInputSave : EventEmitter<String> = new EventEmitter<String>();
  @Output() onInputChange : EventEmitter<String> = new EventEmitter<String>();
  
  @Input() data: string;
  @Input() placeholder: string;
  @Input() delay: number = 300;
  @Input() dismissOnFocusOut: boolean = false;

  @Input() value: string;

  @Input('theme') set inputTheme(value: string) {
    this.theme = value;
    this.themeIsUnderline = value == "underline";
  };

  get label():string {
    return this.value ? this.value : this.placeholder;
  }
  
  private theme: string;
  
  constructor(private mockService: MockService, private elementRef: ElementRef, private renderer: Renderer) {
    mockService.fillInputs(this, {});
    // this.inputValue = this.data;
    // const stream = Observable.fromEvent(elementRef.nativeElement, 'input').map(() => this.inputValue)
    //   .debounceTime(this.delay)
    //   .distinctUntilChanged();

    // stream.subscribe(input => this.onChange(input))
  }

  ngOnInit() {
    
  }

  inputReadyEventHandler(input) {
    input.focus();
  }

  inputChangeEventHandler(value){
    this.isEditing = false;
    this.value = value;
    this.changeEvent.emit(this.value);
    this.onSave();
  }

  inputDismissEventHandler(value) {
    this.isEditing = false;
  }

  startEdit(e:MouseEvent){
    if(this.isEditing) return;
    this.isEditing = true;
    let self = this;
    setTimeout(function(){
      self.textInput.focus();
      let tempvalue = self.textInput.input.nativeElement.value;
      self.textInput.input.nativeElement.value = "";
      self.textInput.input.nativeElement.value = tempvalue;
    }, 100);
  }
  
  onSave(){
    this.onInputSave.emit(this.value);
  }

  onChange(val){
    this.data = val;
    this.onInputChange.emit(this.data);
  }

}
