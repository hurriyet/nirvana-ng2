import { Component, OnInit, HostBinding, Input, ElementRef, ViewChildren, Renderer } from '@angular/core';
import { MockService } from '../../services/mock.service';

@Component({
  selector: 'app-promise-icon',
  templateUrl: './promise-icon.component.html',
  styleUrls: ['./promise-icon.component.scss']
})
export class PromiseIconComponent implements OnInit {
  @ViewChildren('blade') blades:ElementRef[];

  @HostBinding("class.c-promise-icon") true;
  @HostBinding('class.c-promise-icon--sm') get isSmall():boolean { return this.size == 'sm'};
  @HostBinding('class.c-promise-icon--lg') get isLarge():boolean { return this.size == 'lg'};
  @HostBinding('class.c-promise-icon--autosize') get isAutosize():boolean { return this.size == 'autosize'};

  @HostBinding("class.c-promise-icon--promise")
  @Input() isPromising: boolean = false;

  @Input() iconName: string;

  @Input() size: string;
  @Input() set color(value: string) {
    this.iconColor = value;
    if(!this.blades || this.blades.length == 0) return;
    this.renderer.setElementStyle(this.element.nativeElement, 'color', this.iconColor);
    this.blades.forEach( blade => this.renderer.setElementStyle(blade.nativeElement, 'background-color', this.iconColor));
  }

  private iconColor: string;

  constructor(
    private renderer: Renderer,
    private element: ElementRef,
    private mockService: MockService
  ) { 
    mockService.fillInputs(this, {});
  }

  ngOnInit() {
    
  }

  ngAfterViewInit() {
    if(this.iconColor) this.color = this.iconColor;
  }

  public toggle() {
    this.isPromising = !this.isPromising;
  }

}
