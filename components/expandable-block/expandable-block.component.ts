import { TetherDialog } from './../tether-dialog/tether-dialog';
import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-expandable-block',
  templateUrl: './expandable-block.component.html',
  styleUrls: ['./expandable-block.component.scss']
})
export class ExpandableBlockComponent implements OnInit {
  @HostBinding('class.c-expandable-block') true;

  @Output() actionEvent : EventEmitter<Object> = new EventEmitter<Object>();

  @HostBinding('class.c-expandable-block--expanded') 
  @Input() isExpanded: boolean;

  @Input() leftIcon: string = 'menu';
  @Input() isDraggable: boolean;
  @Input() title: string;
  @Input() contextMenuData: {action: string, label: string, icon?: string, params?: any, group?: any }[];

  constructor(
    private tetherService: TetherDialog
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if(this.isDraggable) this.leftIcon = "menu";
  }

  public collapse() {
    this.isExpanded = false;
  }

  public expand() {
    this.isExpanded = true;
  }

  private toggleExpand() {
    this.isExpanded = !this.isExpanded;
  }

  private openContextMenu(event) {
    if(!this.contextMenuData || this.contextMenuData.length == 0) return;

    this.tetherService.context({
			title: "İŞLEMLER",
			data: this.contextMenuData
		}, {
      target: event.target
    }).then( result => this.actionEvent.emit(result)).catch( reason => {});

  }

}
