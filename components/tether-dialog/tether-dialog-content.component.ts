import {
  Component,
  ViewChild,
  ViewContainerRef,
  HostBinding,
  HostListener,
  ElementRef
} from '@angular/core';

import { TetherOptions } from './tether-options';

declare var Tether: any;

@Component({
  selector: 'app-tether-dialog__content',
  template: `
      <template #contentContainer></template>
  `
})
export class TetherDialogContentComponent {
  @ViewChild('contentContainer', {read: ViewContainerRef})
  public contentContainer: ViewContainerRef;

  @HostBinding('class')
  get class() {
    return "c-tether-dialog__content " + this.customClass;
  };

  @HostBinding('style.width')
  private styleWidth;

  @HostBinding('style.height')
  private styleHeight;

  @HostBinding('style.min-height')
  private styleMinHeight;

  @HostBinding('style.max-width')
  private styleMaxWidth;

  @HostBinding('style.background-color')
  private styleBackgroundColor;

  private customClass:string = "";
  private tetherOptions:TetherOptions;
  private dialogSettings:any;
  private tether:any;
  
  private tetherPromise:Promise<Object>;
  private tetherResolve: (result?: any) => void;
  private tetherReject: (reason?: any) => void;

  get promise() { return this.tetherPromise };
  get resolve() { return this.tetherResolve };
  get reject() { return this.tetherReject };
  get settings() { return this.tetherOptions.settings };
  get element():ElementRef { return this.elementRef };
  get contentTether():any { return this.tether };

  public clickCount:number = 0;

  constructor(private elementRef: ElementRef) {  }

  public setDialogSettings(tetherOptions: TetherOptions){
    let styles= [];
    this.tetherOptions = tetherOptions;
    this.dialogSettings = tetherOptions.settings.dialog;
    if(this.dialogSettings){
      if(this.dialogSettings.style) {
        if(this.dialogSettings.style.width) this.styleWidth = this.dialogSettings.style.width;
        if(this.dialogSettings.style.height) this.styleHeight = this.dialogSettings.style.height;
        if(this.dialogSettings.style.minHeight) this.styleMinHeight = this.dialogSettings.style.minHeight;
        if(this.dialogSettings.style.maxWidth) this.styleMaxWidth = this.dialogSettings.style.maxWidth;
        if(this.dialogSettings.style.backgroundColor) this.styleBackgroundColor = this.dialogSettings.style.backgroundColor;
      };
      if(this.dialogSettings.class){
        this.customClass = this.dialogSettings.class;
      };
    };
    
    tetherOptions.settings.element = this.elementRef.nativeElement;
    this.tether = new Tether(tetherOptions.settings);
    this.tetherPromise = new Promise((resolve, reject) => {
      this.tetherResolve = resolve;
      this.tetherReject = reject;
    });
    this.position();
  };
  
  public position() {
      this.tether.position();
  }

  public ngAfterViewInit(){
      this.position();
  }
  
  public ngOnDestroy(){
    if (this.contentContainer) {
        this.contentContainer.clear()
        this.contentContainer = null;
    };

    if(this.tether) this.tether.destroy();

    this.clickCount = 0;
  };
}