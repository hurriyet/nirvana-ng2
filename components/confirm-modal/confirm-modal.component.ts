import { TetherDialog } from './../tether-dialog/tether-dialog';
import { Component, OnInit, HostBinding, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {
  @HostBinding('class.c-confirm-modal') true;

  @Input() title: string;
  @Input() description: string;
  @Input() confirmButton: {label: string, type?: string, theme?:string};
  @Input() dismissButton: {label: string, type?: string, theme?:string};
  @Input() feedback: {label: string, placeholder?: string, value?: string};

  constructor(
    private tether: TetherDialog
  ) { }

  ngOnInit() {
  }

  private confirm() {
    this.tether.close({
      feedback: this.feedback ? this.feedback.value : null
    })
  }

}
