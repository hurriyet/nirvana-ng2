import { Component, OnInit,EventEmitter, Output, Input, HostBinding } from '@angular/core';
import { MockService } from '../../services/mock.service';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.scss']
})

export class TreeViewComponent implements OnInit {
  @HostBinding('class.c-tree-view') true ;
  @Output() treeViewOnClick : EventEmitter<any> = new EventEmitter<any>();
  @Input() list: Array<any>;
  @Input() isAllEnabled: Boolean = true;
  @Input() title: String;

  private selectedItemKey: any;
  constructor(private mockService: MockService) {
    mockService.fillInputs(this, {});
  }
  ngOnInit() {}

  select(value){
    if(value == null || value.id == this.selectedItemKey ){
      this.selectedItemKey = null;
      this.treeViewOnClick.emit(null);
    } else{
      this.selectedItemKey = value.id;
      this.treeViewOnClick.emit(value)
    }
  }
}
