import { Component, OnInit, HostBinding, ViewChild, Output, EventEmitter, HostListener, Input } from '@angular/core';
import { PromiseIconComponent } from '../promise-icon/promise-icon.component';
@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.scss']
})
export class AddButtonComponent implements OnInit {
  @HostBinding('class.c-add-button') true;
  @ViewChild(PromiseIconComponent) promiseIcon: PromiseIconComponent;
  
  @Output() clickEvent: EventEmitter<any> = new EventEmitter();

  @HostListener('click') clickEmit() {
    this.clickEvent.emit(this.promiseIcon);
  }

  @Input() size: string = "autosize";

  constructor() { }

  ngOnInit() {
  }

}
