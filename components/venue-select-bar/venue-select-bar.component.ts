import { Template } from './../../models/template';
import { Venue } from './../../models/venue';
import { VenueSearchSelectComponent } from './../../common/venue-search-select/venue-search-select.component';
import { Observable } from 'rxjs/Rx';
import { TetherDialog } from './../tether-dialog/tether-dialog';
import { Component, OnInit, HostBinding, Input, ComponentRef, ComponentFactoryResolver, Injector, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-venue-select-bar',
  templateUrl: './venue-select-bar.component.html',
  styleUrls: ['./venue-select-bar.component.scss'],
  entryComponents: [VenueSearchSelectComponent]
})
export class VenueSelectBarComponent implements OnInit {
  @HostBinding('class.c-venue-select-bar') true;

  @Output() actionEvent: EventEmitter<{action: string, data?: any}> = new EventEmitter();
  @Output() changeEvent: EventEmitter<{venue: Venue, template: Template, params?:{}}> = new EventEmitter();

  @HostBinding('class.c-venue-select-bar--empty')
  private get isEmpty(): boolean { return this.template == null };

  @Input() settings: {
    addLabel: string,
    levels: { key: string, title: string, params?:any }[],
    search: {
      title: string,
      placeholder: string,
      presets: { title: string, list: {id: any, title: string, icon?: string, description?:string}[] }[]
      feedback: {
        title: string, 
        description: string, 
        action?: {action: string, label: string, params?: Object}
      }
    }
  }

  @Input() venue: Venue;
  @Input() template: Template;
  @Input() isEditMode: boolean;

  private venueSearchBox: VenueSearchSelectComponent;
  private resultSubscription: any;
  private feedbackSubscription: any;
  private searchSubscription: any;

  constructor(
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private tether: TetherDialog) { }

  ngOnInit() {
  }

  private selectHandler(result:{id: any, title: string}) {
    this.actionEvent.emit({action: "select", data: result});
    //this.addFirm({id: result.id, name: result.title, type: -1});
  }

  public searchVenue() {
    let component:ComponentRef<VenueSearchSelectComponent> = this.resolver.resolveComponentFactory(VenueSearchSelectComponent).create(this.injector);
    this.venueSearchBox = component.instance;

    this.venueSearchBox.isTemplateEditable = this.isEditMode;
    this.venueSearchBox.settings = {
      levels: this.settings.levels,
      search: this.settings.search,
    }

    this.venueSearchBox.actionEvent.subscribe( event => this.actionEvent.emit(event) );

    this.tether.modal(component, {
      escapeKeyIsActive: true
    }).then(result => {
      //this.venueInfo = {id: result['venue']['id'], name: result['venue']['name'], template: result['template']['name'], image: result['venue']['image']};
      this.venue = result["venue"];
      this.template = result["template"];
      this.changeEvent.emit({venue: this.venue, template: this.template, params:{action:result["action"]}});
    }).catch( reason => {
      
    })
  }
}
