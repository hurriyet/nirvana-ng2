import { Component, OnInit,Input,Output,EventEmitter,ElementRef } from '@angular/core';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-wide-col-basket-item',
  templateUrl: './wide-col-basket-item.component.html',
  styleUrls: ['./wide-col-basket-item.component.scss']
})
export class WideColBasketItemComponent implements OnInit {
  private isEditable: boolean = true;
  @Output() actionEvent: EventEmitter<any> = new EventEmitter();
  @Output() fieldEdit: EventEmitter<any> = new EventEmitter();
  @Output() inlineEventBubbling: EventEmitter<any> = new EventEmitter();

  @Input() text:string ;
  @Input() data: Object;
  @Input() options:Array<any>;

  @Input() totalSum:number;

  public selectedItems = [];
  public selectedSeats = [];
  public isItemSelected:Array<boolean> = [];
  private customer;

  get model(): Object {
    return this.data;
  }

  constructor(private shoppingCartService : ShoppingCartService, el: ElementRef, private router : Router) {
   }

  ngOnInit() {
  	this.customer = this.shoppingCartService.getCartUser();
  /*
   let sum:number = 0;

   this.data['products'].forEach(element => {
    element['basketItem'].forEach(item => {
     sum += item.price;
   })
 });
 this.totalSum = sum;
 return this.totalSum;
*/
  }
  onColumnEdit($event) {
    console.log($event);
    this.inlineEventBubbling.emit($event);
  }

// Lazım Olabilir Şimdilik silmeyelim.

//   basketItemSelection(indexId,checkIsSeatable){
//     this.isItemSelected[indexId] = !this.isItemSelected[indexId];
//     if(checkIsSeatable == false){
//       if(this.isItemSelected[indexId] == false) {
//         this.selectedSeats.splice(indexId,1);
//       }
//       else
//       {
//         this.selectedSeats.push(this.data['products'][0]['basketItem'][indexId]['userId']);
//       }

//     }
//     else
//       {
//       if(this.isItemSelected[indexId] == false) {
//         this.selectedItems.splice(indexId,1);
//       }
//       else
//       {
//         this.selectedItems.push(this.data['products'][0]['basketItem'][indexId]['userId']);
//       }
//     }
// }

  removeSelectedItems(model,product) {

        if(this.selectedItems.length > 0)
        {
         this.actionEvent.emit( {
            action:"removeSelectedItem",
            modelId:model.id,
            eventId:product.id,
            removeThisUsersFromEvent:this.selectedItems
         })
        }
        else {
          this.actionEvent.emit({
            action:"removeAllItems",
            modelId:model.id,
            removeAllItems: true,
          })
        }
  }
  private totalDiscountAmount(discounts){
  	let amount = 0;
  	discounts.forEach(discount => {
  		amount += discount['AppliedDiscountAmount'];
  	})
  	return amount.toFixed(2);
  }
  private totalVariantCountFromPerformances(performance){
  	if(performance){
  		let count : number = 0;
  		performance.Products.forEach(product => {
  			count += product.CountOfProductsSelected;
  		});
  		return count;
  	}
  }
  update(id){
  	//let cancelAllResponse = window["DeviceIntegrationInstance"].cancelAllPayment();
  	this.router.navigate(['boxoffice',id,'products']);
  }
  seatsFromPerformances(performance){
  	if(performance){
  		let seats : string = '';
  			performance.Products.forEach(product => {
  				product.Variants.forEach(variant => {
  					if(variant.Seats && variant.Seats.length > 0){
  						variant.Seats.forEach(seat =>{
  							seats += (seat['Section'] ? seat['Section'] : '-') + ' / ' +  (seat['Block'] ? seat['Block'] : '-') + ' / ' + (seat['Seat'] ? seat['Seat'] : '-');
  							seats += '  &  ';
  						});
  					}else{
  						seats += '-';
  					}

  				});
  			});
  		seats = seats.slice(0,-5);
  		return seats;
  	}
  }
}
