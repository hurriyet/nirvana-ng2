import { Component, OnInit, ViewChild, ContentChild,ElementRef,Renderer, HostBinding, Input, Output, EventEmitter, HostListener, AfterViewInit, AfterContentInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import * as SimpleMDE from 'simplemde';

@Component({
  selector: 'app-md-editor',
  templateUrl: './md-editor.component.html',
  styleUrls: ['./md-editor.component.scss']
})
export class MdEditorComponent implements OnInit,AfterViewInit {
  @ViewChild('textarea') textarea: ElementRef;

  @HostBinding('class.c-md-editor') true;
  @HostBinding('class.c-md-editor--disabled')
  @Input() isDisabled: boolean;

  @Output() editorValue: EventEmitter<any> = new EventEmitter();
  @Output() changeEvent: EventEmitter<string> = new EventEmitter();
  
  @Input() options:{
    previewOn?:boolean,
    focusOn?:boolean,
    placeholder?:string,
  }

  private inputValue:string;
  
  @Input() set value(value:string) {
    if(!value) value = "";
    if(!this.mdEditor || this.mdEditor.value() == value) return;
    this.mdEditor.value(value);
  }

  public mdEditor:SimpleMDE;

  constructor(private el:ElementRef,private renderer:Renderer) {
  }

  ngOnInit(){

  }

  ngAfterViewInit() {
    this.mdEditor = new SimpleMDE({ 
      element: this.textarea.nativeElement,
      autofocus: this.options.focusOn == true  ? true : false,
      spellChecker:false,
      promptURLs: false,
      placeholder: this.options.placeholder,
      initialValue: this.inputValue,
      toolbar: ["bold", "italic", "heading-1","heading-2","heading-3", this.options.previewOn == true ? "preview" : ''],
      status: false
    });

    this.mdEditor.codemirror.self = this;
    this.mdEditor.codemirror.on("change", this.editorChangeHandler);
  }

  ngOnDestroy() {
    this.mdEditor.codemirror.off("change", this.editorChangeHandler)
    this.mdEditor = null;
  }

  private editorChangeHandler(event) {
    event.self.changeEvent.emit(event.self.mdEditor.value());
    event.self.editorValue.emit(event.self.mdEditor.value());
  }

}
