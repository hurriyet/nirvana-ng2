import { Component, OnInit, HostBinding, EventEmitter, Output, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


/**
 * Pill button groups.
 *
 * @param {array} pills - Pills have to be an iterable array. Each array item must have { {number} count, {string} text, {boolean} isActive } variables.
 */
@Component({
	selector: 'app-pill-group',
	templateUrl: './pill-group.component.html',
	styleUrls: ['./pill-group.component.scss']
})
export class PillGroupComponent implements OnInit {
	@HostBinding('class') class = 'c-pill-group';

	@Input() pills : Array<any>;
	@Output() selectPillFilterAction : EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

	@Input() selectedPill = null;

	@Input() set setSelectedPill(pill) {
		if(pill) {
			this.selectedPill = pill;
			this.selectPillFilterAction.emit(this.selectedPill);
		}
	}

	constructor(
		private route: ActivatedRoute
	) {
	}

	ngOnInit() {
		// this.route.url.subscribe(url => {
		// });
	}

	toggle(pill: any){
		if(this.selectedPill == pill){
			this.selectedPill = null;
		} else {
			this.selectedPill = pill;
		}
		this.selectPillFilterAction.emit(pill);
	}
}
