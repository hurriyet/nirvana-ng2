import { Component, OnInit, HostBinding, Input, Output, EventEmitter, ViewChild, ElementRef, Renderer, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';

import { PromiseIconComponent} from '../promise-icon/promise-icon.component';
import { MockService } from '../../services/mock.service';
declare var Pikaday;
import * as moment from 'moment';
import 'moment/locale/tr'
@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss']
})
export class TextInputComponent implements OnInit {
  @ViewChild(PromiseIconComponent) promiseIcon: PromiseIconComponent;
  @ViewChild('input') input: ElementRef;

  @HostBinding('class.c-text-input') true;
  @HostBinding('class.c-text-input--error')
  private get hasError():boolean { return this.formControl && !this.formControl.valid && (this.formControl.touched || this.formControl.dirty)}

  @HostBinding('class.c-text-input--disabled')
  @Input() isDisabled: boolean;

  @HostBinding('class.c-text-input--underline')
  private isUnderline: boolean = false;

  @HostBinding('class.c-text-input--no-border')
  private isNoBorder: boolean = false;

  @HostBinding('class.c-text-input--focused')
  private isFocused: boolean;

  @HostBinding('class.c-text-input--autosize')
  get isAutosize(){
    return this.size == "autosize";
  }
  @HostBinding('class.c-text-input--lg')
  get isLg(){
    return this.size == "lg";
  }
  @HostBinding('class.c-text-input--md')
  get isMd(){
    return this.size == "md";
  }
  @HostBinding('class.c-text-input--sm')
  get isSm(){
    return this.size == "sm";
  }

  @HostBinding('class.c-text-input--sm-text')
  get isSmText(){
    return this.size == "sm-text";
  }

  @HostBinding('class.c-text-input--md-text')
  get isMdText(){
    return this.size == "md-text";
  }

  @HostBinding('class.c-text-input--dirty')
  get isDirty():boolean {
    return this.value != null && this.value.toString().length > 0;
  }

  @Output() changeEvent:EventEmitter<any> = new EventEmitter();
  @Output() typeEvent:EventEmitter<any> = new EventEmitter();
  @Output() dismissEvent:EventEmitter<any> = new EventEmitter();

  @Input() form:FormGroup;
  @Input() name: string;
  @Input() value: string;
  @Input() placeholder: string = "";
  @Input() min: number;
  @Input() max: number;
  @Input() maxlength: number;
  @Input() required: boolean;
  @Input() icon: string;
  @Input() iconAsButton: boolean = false;
  @Input() autocomplete: string = "off";
  @Input() isIconAlignLeft:boolean = false;
  @Input() isPromising: boolean = false;
  @Input() typeDebounceTime: number = 500;
  @Input() dismissOnFocusOut: boolean = false;
  @Input() dismissOnEscape: boolean = true;
  @Input() dateTimeFormat: string = 'DD.MM.YYYY, dddd HH:mm';
  @Input() autoFocusWhenEnabled: boolean = false;

  @Input() size: string = "md";

  @Input() set type(value: string) {
    this.inputType = value || "text";
    switch(this.inputType) {
      case 'email':
        this.pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      break;
      case 'url':
        this.pattern = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/;
      break;
      case 'tel':
        this.inputType = "text";
        this.maxlength = 20;
        this.pattern = /(?:\(\d{3}\)|\d{3})[- ]?\d{3}[- ]?\d{4}/;
      break;
      case 'search':
        if(!this.icon) this.icon = "search";
      break;
      case 'lat':
        this.inputType = "number";
        this.pattern = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$/;
      break;
      case 'lon':
        this.inputType = "number";
        this.pattern = /^[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;
      break;
      case 'latlon':
        this.inputType = "number";
        this.pattern = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;
      break;
      case 'datepicker':
        if(this.placeholder == null) this.placeholder = "Bir Tarih Seçin";
        if(!this.icon) this.icon = "insert_invitation";
        moment.locale('tr');
        let self = this;
        let picker = new Pikaday({
	    	field: this.input.nativeElement,
        showTime: true,
        showMinutes: true,
        showSeconds: false,
        use24hour: true,
        incrementHourBy: 1,
        incrementMinuteBy: 5,
        autoClose: true,
        format: this.dateTimeFormat,
        	onSelect: function(date) {
        		self.value = moment(date).format(self.dateTimeFormat);
        	},
        	i18n: {
            previousMonth : 'Previous Month',
            nextMonth     : 'Next Month',
            months        : moment.localeData()["_months"],
            weekdays      : moment.localeData()["_weekdays"],
            weekdaysShort : moment.localeData()["_weekdaysShort"]
			    }
	  	  });
        this.pikaday = picker;
      break;
    }
  }

  @Input() set minDate(value:Date) {
    value = moment(value).toDate();
    if(this.pikaday) this.pikaday.setMinDate(value);
  }

  @Input() set maxDate(value) {
    value = moment(value).toDate();
    if(this.pikaday) this.pikaday.setMaxDate(value);
  }

  @Input() set date(value: string) {
  	if(value != null && moment(value).isValid()){
  		this.value = moment(value).format(this.dateTimeFormat);
  	}else{
      this.value = null;
    }
  }
  @Input() set pattern(value: RegExp) {
    this.inputPattern = value;
  }

  @Input() set patternString(value:string) {
    this.pattern = new RegExp(value,"g");
  }

  @Input() set theme(value: string) {
    this.isUnderline = value == 'underline';
    this.isNoBorder = value == "no-border";
  }

  @Input() set iconAlign(value: string) {
    this.isIconAlignLeft = value == "left";
  }

  @Input() set disabled(value: boolean) {
    this.isDisabled = value;
  }

  @Input() set isTypeEmitting(value: boolean) {
    if(value && this.typeSubscription == null) {
      this.typeSubscription = this.formControl.valueChanges
      .map(item => {
        if(!this.stateUserTyping) this.stateUserTyping = true;
        return item;
       })
      .debounceTime(this.typeDebounceTime)
      .distinctUntilChanged()
      .subscribe( value => {
        this.stateUserTyping = false;
        this.typeEvent.emit(value);
      });
    }else{
      this.typeSubscription.unsubscribe();
      this.typeSubscription = null;
    }
  }

  get focused():boolean { return this.isFocused };

  private get stateIcon() {
    return this.stateUserTyping ? "keyboard" : this.icon;
  }

  private inputType: string = 'text';
  private inputPattern: RegExp;
  private typeSubscription;
  private pristineValue: string;
  private isTriggeredBefore: boolean = false;
  private stateUserTyping: boolean = false;

  private pikaday: Pikaday;

  public formControl: FormControl = new FormControl({value: this.value, disabled: this.isDisabled}, this.patternValidator());


  constructor(
    private renderer: Renderer,
    private elementRef: ElementRef,
    private changeDetector: ChangeDetectorRef,
    mockService: MockService) {
    //mockService.fillInputs(this, null);
   }

  ngOnInit() {
    if(this.form && this.name) {
      this.form.addControl(this.name, this.formControl);
      this.changeDetector.detectChanges();
      this.formControl.statusChanges.subscribe( status => {
        this.isDisabled = status == "DISABLED";
      })
    }
  }

  ngOnChanges(changes) {
    if(changes.isDisabled && this.formControl) {
      changes.isDisabled.currentValue ? this.formControl.disable() : this.formControl.enable();
      if(this.autoFocusWhenEnabled && !this.formControl.disabled) this.focus();
    }
  }

  ngOnDestroy() {
    if(this.formControl && this.form) {
      this.form.removeControl(this.name);
      this.formControl = null;
    }
  }

  focus() {
    //this.input.nativeElement.focus();
    let self = this;
    setTimeout(function() {
      self.input.nativeElement.focus();
    }, 10);
  }

  save() {
    this.emitChangeEvent();
    this.isTriggeredBefore = true;
    this.input.nativeElement.blur();
    this.isTriggeredBefore = false;
  }

  onFocus() {
    this.isFocused = true;
    this.pristineValue = this.value;
  }

  onFocusOut() {
    this.isFocused = false;
    if(this.isTriggeredBefore) return;
    this.dismissOnFocusOut ? this.dismissChanges() :  this.emitChangeEvent();
  }

  onSubmit(form) {

  }

  private dismissChanges($event?) {
    if($event && !this.dismissOnEscape) return;
    this.value = this.pristineValue;
    this.isTriggeredBefore = true;
    this.input.nativeElement.blur();
    this.isTriggeredBefore = false;
    this.dismissEvent.emit(this.value);
  }

  private emitChangeEvent() {
    switch(this.inputType) {
      case "datepicker":
        if(this.value) this.changeEvent.emit(moment(this.value, this.dateTimeFormat).toISOString());
      break;
      case "number":
        this.changeEvent.emit(parseFloat(this.value));
      break;
      default:
        this.changeEvent.emit(this.value);
      break;
    }
  }

  private patternValidator(): ValidatorFn {
    return (): {[key: string]: any} => {
      if(!this.inputPattern) return null;
      if(!this.value || !this.inputType) return null;
      const name = this.value;
      const test = this.inputPattern.test(name);
      return !test ? {'patternValidator': {name}} : null;
    };
  }

}
