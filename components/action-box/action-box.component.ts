import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-action-box',
  templateUrl: './action-box.component.html',
  styleUrls: ['./action-box.component.scss']
})
export class ActionBoxComponent implements OnInit {
  @HostBinding('class.c-action-box') true;

  @Output() actionEvent: EventEmitter<{action: string, value: any, params?: any}> = new EventEmitter();

  @Input() title: string;
  @Input() description: string;
  @Input() input: {type?: string, placeholder?: string};
  @Input() buttons: {label: string, action: string, params?: any, theme?: string}[];
  @Input() value: string = "";

  private get inputIsValid():boolean {
    return this.value && this.value.toString().length > 0;
  }

  constructor() { }

  ngOnInit() {

  }

  private buttonClickHandler(button:{label: string, action: string, params?: any, theme?: string}, event) {
    if(button) this.actionEvent.emit({action: button.action, value: this.input && this.input.type == "number" ? parseFloat(this.value) : this.value, params: button.params});
  }

  private inputFocusHandler(event) {

  }

  private inputFocusOutHandler(event) {
    
  }

  private inputEnterHandler(event) {
    if(!this.buttons){
      this.actionEvent.emit({action: 'enter', value: this.input && this.input.type == "number" ? parseFloat(this.value) : this.value});
    }else{
      if(this.buttons.length == 1) this.actionEvent.emit({action: this.buttons[0].action, value: this.input && this.input.type == "number" ? parseFloat(this.value) : this.value, params: this.buttons[0].params});
    }
  }

  private inputEscapeHandler(event) {
    if(!this.buttons) this.actionEvent.emit({action: 'escape', value: null});
  }

}
