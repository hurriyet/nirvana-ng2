import { Component, OnInit, HostBinding, Input,Type,ViewContainerRef } from '@angular/core';
import { TetherDialog } from '../tether-dialog/tether-dialog';

@Component({
  selector: 'app-cover-image',
  templateUrl: './cover-image.component.html',
  styleUrls: ['./cover-image.component.scss']
})
export class CoverImageComponent implements OnInit {
  @HostBinding('class.c-cover-image') true;
  @HostBinding('style.background-image')
  get backgroundSource():string {
    return "url(" + this.backgroundImage +")";
  }

  @Input() backgroundImage:string;

  constructor(
    private viewContainer: ViewContainerRef,

  ) { }

  ngOnInit() {
  }


}
