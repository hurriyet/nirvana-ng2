import { Component, OnInit, HostBinding, ViewChild, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { MockService } from '../../services/mock.service';

import { InlineEditComponent } from '../inline-edit/inline-edit.component';

@Component({
  selector: 'app-header-inline-edit',
  templateUrl: './header-inline-edit.component.html',
  styleUrls: ['./header-inline-edit.component.scss']
})
export class HeaderInlineEditComponent implements OnInit {
  @ViewChild(InlineEditComponent) inlineEdit: InlineEditComponent;

  @HostBinding('class.c-header-inline-edit') true ;

  @Output() changeEvent: EventEmitter<string> = new EventEmitter<string>();
  private changeEventSubscription;

  @Input() breadcrumbs: Array<Object>;
  @Input() placeholder: string;
  @Input() value: string;
  @Input() isPromising:boolean = false;
  
  constructor(private changeDetector: ChangeDetectorRef, mockService: MockService) { mockService.fillInputs(this, {}); }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.changeEventSubscription = this.inlineEdit.changeEvent.subscribe( value => {
      this.value = value;
      this.changeEvent.emit(value)
    });
  }

  ngOnDestroy() {
    this.changeEventSubscription.unsubscribe();
    this.changeEventSubscription = null;
  }

}
