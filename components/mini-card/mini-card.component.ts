import {   
  Component,
  ComponentFactory,
  ComponentRef,
  ComponentFactoryResolver,
  Type,
  ViewContainerRef,
  Injector,
  OnInit } from '@angular/core';

import {TetherDialog} from '../tether-dialog/tether-dialog';
import {ContextMenuComponent} from '../context-menu/context-menu.component';

@Component({
  selector: 'app-mini-card',
  templateUrl: './mini-card.component.html',
  styleUrls: ['./mini-card.component.scss'],
  entryComponents: [ ContextMenuComponent ]
})
export class MiniCardComponent implements OnInit {

  constructor(
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private viewContainer: ViewContainerRef,
    private _tetherService: TetherDialog,    
  ) { }

  ngOnInit() {
  }

  openMiniCardMenu(event){
    let component:ComponentRef<ContextMenuComponent> = this.resolver.resolveComponentFactory(ContextMenuComponent).create(this.injector)
    let instance: ContextMenuComponent = component.instance;
    instance.title = "İşlemler";
    instance.data = [
			{action: "gotoLink", parameters: ['sponsor'], icon: 'edit', label: 'Düzenle'},
			{action: "gotoLink", parameters: ['categories'], icon: 'delete', label: 'Sil'},
	
		]

    this._tetherService.context(component,
    {
      target: event.target,
      attachment: "top right",
      targetAttachment: "top right",
      targetOffset: '-13px 0px'
    }).then(result => {
      console.log("promise result : ", result);
    }).catch(reason => {
      console.log("dismiss reason : ", reason);
    });
  }
  getFactory(component: Type<any>):ComponentFactory<any> {
    return this.resolver.resolveComponentFactory(component);
  }  

}
