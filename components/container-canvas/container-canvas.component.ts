import { Component, OnInit, HostBinding,Input } from '@angular/core';

@Component({
  selector: 'app-container-canvas',
  templateUrl: './container-canvas.component.html',
  styleUrls: ['./container-canvas.component.scss']
})
export class ContainerCanvasComponent implements OnInit {
  @HostBinding('class.c-container-canvas') true;
  
  @HostBinding('class.c-container-canvas--empty') 
  @Input() isEmpty: boolean = false;

  @HostBinding('class.c-container-canvas--footer') private isFooter: boolean;
  @HostBinding('class.c-container-canvas--header') private isHeader: boolean;

  @Input() set type(value: string) {
    this.isFooter = value == "footer";
    this.isHeader = value == "header";
  }

  // @HostBinding('class.c-container-canvas--dashed-border') addDashed:boolean = false;
  // @HostBinding('class.overelement-5') overelement:boolean = false;
  // @Input() isBorderDashed:boolean = false;
  // @Input() isOverSibling:boolean = false;

  constructor() { }
  
  ngOnInit() {
    // if(this.isBorderDashed == true) {
    //   this.addDashed = this.isBorderDashed;
    // }
    
    // if(this.isOverSibling == true) {
    //   this.overelement = this.isOverSibling;
    // }
    
  }


  

}
