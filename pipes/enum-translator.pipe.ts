import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumTranslator'
})
export class EnumTranslatorPipe implements PipeTransform {
	private values : Object = {
		Unknown : { TR: '-', EN: '-'},
		Closed : { TR: 'Kapalı', EN: ''},
		OnSale : { TR:'Satışta', EN: ''},
		SoldOut : { TR:'Tükendi', EN: ''},
		Draft : { TR:'Taslak', EN: ''},
		Cancelled : { TR:'İptal Edildi', EN: ''},
		Suspended : { TR:'Beklemede', EN: ''},
		ConfigurationReady : { TR:'Konfigürasyona Hazır', EN: ''},
		Published : { TR:'Yayınlandı', EN: ''},
		Event : { TR:'Etkinlik', EN: ''},
		Merchant : { TR:'Satıcı', EN: ''},
		CarPark : { TR:'Park', EN: ''},
		Fbvr : { TR:'Yemek ve İçecek', EN: ''},
		Membership : { TR:'Üyelik', EN: ''},
		NotSet : { TR:'Seçilmemiş', EN: ''},
		MainSponsor : { TR:'Ana Sponsor', EN: ''},
		MediaSponsor : { TR:'Media Sponsoru', EN: ''},
		EventSponsor : { TR:'Etkinlik Sponsoru', EN: ''},
		SubSponsor : { TR:'Yan Sponsor', EN: ''},
		OtherSponsor : { TR:'Diğer Sponsor', EN: ''},
		Open : { TR:'Açık', EN: ''},
		Selected : { TR:'İşleniyor', EN: ''},
		Expired : { TR:'Süresi Doldu', EN: ''},

		Sponsor : { TR:'Sponsor', EN: ''},
		Individual : { TR:'Bireysel', EN: ''},
		TargetGroup : { TR:'Hedef Kitle', EN: ''},
	}
  	transform(value: any, args?: any): any {
  		let locale = 'TR';
    	return (this.values.hasOwnProperty(value)) ? this.values[value][locale] : 'Enum Translation Not Found!';
  	}

}
