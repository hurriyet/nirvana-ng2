import { CheckboxComponent } from './../../components/checkbox/checkbox.component';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { TabBarComponent } from '../../components/tab-bar/tab-bar.component';
import { MainMenuComponent } from '../../components/main-menu/main-menu.component';
import { TextInputComponent } from '../../components/text-input/text-input.component';
import { PromiseIconComponent } from '../../components/promise-icon/promise-icon.component';

@NgModule({
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule, RouterModule,
  ],
  declarations: [
    TabBarComponent,
    MainMenuComponent,
    TextInputComponent,
    PromiseIconComponent,
    CheckboxComponent
  ],
  exports: [
    TabBarComponent,
    MainMenuComponent,
    TextInputComponent,
    PromiseIconComponent,
    CheckboxComponent
  ]
})
export class UiComponentsModule { }
