import { SimpleTreeviewModule } from './simple-tree-view/simple-tree-view.module';
import { UiComponentsModule } from './../ui-components/ui-components.module';
import { SimpleTreeviewComponent } from './simple-tree-view/simple-tree-view.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule, RouterModule,
    UiComponentsModule,
    SimpleTreeviewModule
  ],
  declarations: [
    
  ],
  exports: [
    SimpleTreeviewComponent
  ]
})
export class ComponentsModule { }
