import { PerformanceGroupSaleComponent } from './routes/performance/performance-group-sale/performance-group-sale.component';
import { PerformanceCancelBlockComponent } from './routes/performance/performance-cancel-block/performance-cancel-block.component';
import { ProductCreateComponent } from './routes/product/product-create/product-create.component';
import { NgModule } from '@angular/core';
import { HashLocationStrategy, Location, LocationStrategy } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthenticationService } from './services/authentication.service';

import { AppComponent } from './app.component';
import { MainComponent } from './routes/main/main.component';
import { IndexComponent } from './routes/index/index.component';
import { LoginComponent } from './routes/login/login.component';
import { PasswordRecoveryComponent } from './routes/login/password-recovery/password-recovery.component';
import { LockComponent } from './routes/login/lock/lock.component';
import { EventsComponent } from './routes/events/events.component';
import { EventComponent } from './routes/event/event.component';
import { EventPerformancesComponent } from './routes/event/event-performances/event-performances.component';
import { EventEventsComponent } from './routes/event/event-events/event-events.component';
import { EventProductsComponent } from './routes/event/event-products/event-products.component';
import { EventSponsorsComponent } from './routes/event/event-sponsors/event-sponsors.component';

import { EventEditComponent } from './routes/event/event-edit/event-edit.component';

import { VenuesComponent } from './routes/venues/venues.component';
import { VenueComponent } from './routes/venue/venue.component';

import { VenueEditComponent } from './routes/venue/venue-edit/venue-edit.component';
import { VenueEventsComponent } from './routes/venue/venue-events/venue-events.component';

import { VenueLayoutsComponent } from './routes/venue/venue-layouts/venue-layouts.component';
import { VenueEventsCalendarComponent } from './routes/venue/venue-events-calendar/venue-events-calendar.component';
import { VenueMediaMaterialsComponent } from './routes/venue/venue-media-materials/venue-media-materials.component';

import { PerformersComponent } from './routes/performers/performers.component';

import { ProductsComponent } from './routes/products/products.component';

import { PerformanceComponent } from './routes/performance/performance.component';
import { PerformanceCreateComponent } from './routes/performance-create/performance-create.component';
import { PerformancePerformersComponent } from './routes/performance/performance-performers/performance-performers.component';
import { PerformanceProductsComponent } from './routes/performance/performance-products/performance-products.component';
import { PerformanceProductStatisticsComponent } from './routes/performance/performance-product-statistics/performance-product-statistics.component';
import { PerformanceSponsorsComponent } from './routes/performance/performance-sponsors/performance-sponsors.component';
import { PerformanceReservationsComponent } from './routes/performance/performance-reservations/performance-reservations.component';
import { PerformanceInvitationsComponent } from './routes/performance/performance-invitations/performance-invitations.component';

import { ProductsByproductsComponent } from './routes/products/products-byproducts/products-byproducts.component';
import { ProductsBundlesComponent } from './routes/products/products-bundles/products-bundles.component';
import { ProductsPerformancesComponent } from './routes/products/products-performances/products-performances.component';

import { BoxofficeComponent } from './routes/boxoffice/boxoffice.component';
import { BoxofficeProductsComponent } from './routes/boxoffice/boxoffice-products/boxoffice-products.component';
import { BoxofficeEventsComponent } from './routes/boxoffice/boxoffice-events/boxoffice-events.component';
import { BoxofficeContentsComponent } from './routes/boxoffice/boxoffice-contents/boxoffice-contents.component';
import { BoxofficePurchaseComponent } from './routes/boxoffice/boxoffice-purchase/boxoffice-purchase.component';
import { TransactionsComponent } from './routes/transactions/transactions.component';
import { BoxofficeEventsSubeventsComponent } from './routes/boxoffice/boxoffice-events-subevents/boxoffice-events-subevents.component';

import { EventsMultiplePerformanceComponent } from './routes/events/events-multiple-performance/events-multiple-performance.component';
import { EventsSinglePerformanceComponent } from './routes/events/events-single-performance/events-single-performance.component';
import { EventsMasterComponent } from './routes/events/events-master/events-master.component';

import { PerformancesComponent } from './routes/performances/performances.component';
import { PerformersMusiciansComponent } from './routes/performers/performers-musicians/performers-musicians.component';
import { PerformersArtistsComponent } from './routes/performers/performers-artists/performers-artists.component';
import { PerformersOthersComponent } from './routes/performers/performers-others/performers-others.component';
import { PerformersIndexComponent } from './routes/performers/performers-index/performers-index.component';
import { EventsIndexComponent } from './routes/events/events-index/events-index.component';
import { PerformancesIndexComponent } from './routes/performances/performances-index/performances-index.component';

import { VenueTemplateCreateComponent } from './routes/venue/venue-template-create/venue-template-create.component';
import { ProductsIndexComponent } from './routes/products/products-index/products-index.component';

const appRoutes: Routes = [
    {
        path: '', component: MainComponent,
        canActivate: [AuthGuardService],
        canActivateChild: [AuthGuardService],
        children: [
            {
                path: 'events', component: EventsComponent,
                children: [
                    { path: 'single-performance', component: EventsSinglePerformanceComponent },
                    { path: 'multiple-performance', component: EventsMultiplePerformanceComponent },
                    { path: 'master', component: EventsMasterComponent },
                    { path: 'index', component: EventsIndexComponent },
                    { path: '', component: EventsIndexComponent }
                ]
            },
            { path: 'event/create', component: EventEditComponent, data: {role:"create"} },
            { path: 'event/:id/edit', component: EventEditComponent, data: {role:"edit"} },
            { path: 'event/:id', component: EventComponent,
                children: [
                    { path: 'performances', component: EventPerformancesComponent },
                    { path: 'events', component: EventEventsComponent },
                    { path: 'products', component: EventProductsComponent },
                    { path: 'statistics', component: EventProductsComponent },
                    { path: 'sponsors', component: EventSponsorsComponent },
                    // { path: '', component: EventPerformancesComponent },
                ]
            },
            { path: 'venues', component: VenuesComponent },
            { path: 'venue/create', component: VenueEditComponent, data: {role:"create"}  },
            { path: 'venue/:id/edit', component: VenueEditComponent, data: {role:"edit"}  },
            { path: 'venue/:id', component: VenueComponent,
                children: [
                    { path: 'events', component: VenueEventsComponent },
                    { path: 'layouts', component: VenueLayoutsComponent },
                    { path: 'events-calendar', component: VenueEventsCalendarComponent },
                    { path: 'media-materials', component: VenueMediaMaterialsComponent },
                    { path: '', redirectTo: 'events', pathMatch: 'full' },
                ]
            },
            { path: 'venue/:id/template/create', component: VenueTemplateCreateComponent},
            { path: 'products', component: ProductsComponent,
                children: [
                    { path: 'performances', component: ProductsPerformancesComponent },
                    { path: 'bundles', component: ProductsBundlesComponent },
                    { path: 'byproducts', component: ProductsByproductsComponent },
                    // { path: 'index', component: ProductsIndexComponent },
                    { path: '', redirectTo: 'performances', pathMatch: 'full' },
                ]
            },
            { path: 'performance/create', component: PerformanceCreateComponent, data: {role:"create"} },
            { path: 'performance/:id/edit', component: PerformanceCreateComponent, data: {role:"edit"} },
            { path: 'performance/:id', component: PerformanceComponent,
                children: [
                    { path: 'performers', component: PerformancePerformersComponent },
                    { path: 'products', component: PerformanceProductsComponent },
                    { path: 'statistics', component: PerformanceProductStatisticsComponent },
                    { path: 'sponsors', component: PerformanceSponsorsComponent },
                    { path: 'reservations', component: PerformanceReservationsComponent },
                    { path: 'invitations', component: PerformanceInvitationsComponent },
                    { path: 'group-sale', component: PerformanceGroupSaleComponent },
                    { path: 'cancel-block', component: PerformanceCancelBlockComponent },
                    { path: '', redirectTo: 'performers', pathMatch: 'full' }
                ]
            },
            {
                path: 'boxoffice', component: BoxofficeComponent,
                children: [
                    { path: ':id/products', component: BoxofficeProductsComponent },
                    { path: 'events', component: BoxofficeEventsComponent},
                    { path: 'events/:mainEventId/events', component: BoxofficeEventsSubeventsComponent,data: {type:"main-events"} },
                    { path: 'venue/:venueId/events', component: BoxofficeEventsSubeventsComponent,data: {type:"venue-events"} },
                    { path: '', component: BoxofficeEventsComponent},
                    { path: 'contents', component: BoxofficeContentsComponent},
                ]
            },
            { path: 'boxoffice/purchase', component: BoxofficePurchaseComponent},
            { path: 'boxoffice/transactions', component: TransactionsComponent},
            { path: 'layouts', component: VenueLayoutsComponent },
            { path: 'events-calendar', component: VenueEventsCalendarComponent },
            { path: 'media-materials', component: VenueMediaMaterialsComponent },
            { path: 'performers', component: PerformersComponent,
                children: [
                    { path: 'musicians', component: PerformersMusiciansComponent },
                    { path: 'artists', component: PerformersArtistsComponent },
                    { path: 'others', component: PerformersOthersComponent },
                    { path: '', component: PerformersIndexComponent },
                ]
            },
            {
                path: 'performances', component: PerformancesComponent,
                children: [
                    { path: '', component: PerformancesIndexComponent },
                ]
            },
            { path: 'product/create', component: ProductCreateComponent, data: {role:"create"}  },
            { path: 'product/:id/edit', component: ProductCreateComponent, data: {role:"edit"}  },
            { path: '', component: IndexComponent }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }]
})
export class AppRoutingModule { }
