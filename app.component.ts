import { Component, HostBinding, Input, OnInit, LOCALE_ID, Inject, Renderer, ElementRef } from '@angular/core';
import {Event as RouterEvent,NavigationStart,NavigationEnd,NavigationCancel,NavigationError,Router} from '@angular/router';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { TetherDialog } from './components/tether-dialog/tether-dialog';
import { AppSettingsService } from './services/app-settings.service';
import { AuthenticationService } from './services/authentication.service';

declare var jQuery;

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	@HostBinding('class.no-scroll')
	isLoading: boolean = false;
	get noScroll():Boolean {
		return this._tetherService.isBodyLocked;
	};

	isLoggedIn:boolean = false;

	private authService: AuthenticationService;
	private _tetherService: TetherDialog;
	private pageRouter: Router;

	constructor(_tetherService:TetherDialog, appSettingsService :AppSettingsService, authService: AuthenticationService, _router: Router, private renderer: Renderer, private element: ElementRef) {
		this._tetherService = _tetherService;
		this.authService = authService;
		this.authService.isLoggedIn$.subscribe( result => this.isLoggedIn = result );
		this.pageRouter = _router;
	}

    ngOnInit() {
        
    }

    ngAfterViewInit() {
        this.pageRouter.events.subscribe((event: RouterEvent) => {
            this.setPageSpinner(event);
        });
    }

    setPageSpinner(event): void {
        if (event instanceof NavigationStart) {
            this.isLoading = true;
        }
        if (event instanceof NavigationEnd) {
            this.isLoading = false;
        }
        if (event instanceof NavigationCancel) {
            this.isLoading = false;
        }
        if (event instanceof NavigationError) {
            this.isLoading = false;
        }
    }
}
