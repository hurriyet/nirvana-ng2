export const localSettings = {
	reservationDelayTimes: [
		{time: 3, label: "3 Saat Ötele" },
		{time: 8, label: "8 Saat Ötele" },
		{time: 24, label: "1 Gün Ötele" },
		{time: 24*5, label: "5 Gün Ötele" },
	]
};
