import { DialogBoxComponent } from './../../components/dialog-box/dialog-box.component';
import { TagGroupComponent } from './../../components/tag-group/tag-group.component';
import { SimpleTreeviewComponent } from './../../modules/components/simple-tree-view/simple-tree-view.component';
import { Observable } from 'rxjs/Rx';
import { TetherDialog } from './../../components/tether-dialog/tether-dialog';
import { Component, OnInit, Input, ChangeDetectorRef, ViewChild, HostBinding } from '@angular/core';
import { AttributeService } from '../../services/attribute.service';
@Component({
  selector: 'app-attributes-select-add',
  templateUrl: './attributes-select-add.component.html',
  styleUrls: ['./attributes-select-add.component.scss'],
  providers: [AttributeService]
})
export class AttributesSelectAddComponent implements OnInit {
  @ViewChild(DialogBoxComponent) dialogBox: DialogBoxComponent;
  @ViewChild(SimpleTreeviewComponent) tree: SimpleTreeviewComponent;
  @ViewChild(TagGroupComponent) tagGroup: TagGroupComponent;

  @HostBinding('class.oc-attributes-select-add') true;

  @Input() type: {name: any, label: string};
  @Input() entityTypeId: number;
  @Input() title: string;

  @Input() attributes: { //Simple Tree Data
    key: string,
    title: string,
    parentId?: number,
    level: number,
    extraFieldType?: string,
    extraFieldValue?: any,
    params?:any,
    items?: {key: string, title: string, parentId?: number, level: number, extraFieldType?: string, extraFieldValue?: any}[]}[]; //extraFieldType: "fuzzy"

  @Input() checkedNodes: { //Tag Group Data
    name: string,
    label: string,
    type?: any,
    params?: any }[];

  private selectedAttributes: { //Tag Group Data
    name: string,
    label: string,
    type?: any,
    params?: any }[] = [];

  private isPromising: boolean;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private tether: TetherDialog,
    private attributeService : AttributeService
  ) { }

  ngOnInit() {
    if(!this.title) this.title = this.type.label + " Tipinde Özellik Ekle";

    let self = this;
    let isDataLoaded: boolean = false;
    setTimeout(function() {
      self.isPromising = !isDataLoaded;
    }, 300);
    this.attributeService.setCustomEndpoint('List');
    this.attributeService.query({page:0,pageSize:1000},[{key:"attributeTypeId", value:this.type.name}, {key:"entitytTypeId", value:this.entityTypeId}]);
    this.attributeService.data.subscribe(attributes => {
    	if(attributes && attributes.length > 0){
			let tree = [],
			    mappedArray = {},
			    arrayElem,
			    mappedElem,
			    attribute,
			    attributeLength = attributes.length;
          
          let node;
				for(let i = 0; i < attributeLength; i++) {
				    attribute = attributes[i];
            if(this.checkedNodes){
              node = this.checkedNodes.find( item => item.name == attribute["Id"]);
            }
				    arrayElem =  {key: attribute["Id"], parentId:attribute["ParentId"], title: attribute["Localization"]["Tr"]["Name"], level: 0, extraFieldType: parseInt(attribute["ValueType"]) ==  2 ? "fuzzy" : null, extraFieldValue: node ? node["extraFieldValue"] : 0}
				    mappedArray[arrayElem["key"]] = arrayElem;
				    mappedArray[arrayElem["key"]]['items'] = [];
				}
			  	for (let id in mappedArray) {
			    	if (mappedArray.hasOwnProperty(id)) {
			      		mappedElem = mappedArray[id];
				      	if (mappedElem["parentId"]) {
				        	mappedArray[mappedElem['parentId']]['items'].push(mappedElem);
				      	}else {
				        	tree.push(mappedElem);
				      	}
			    	}
			  }
			  this.attributes = tree;
    		this.changeDetector.detectChanges();
        isDataLoaded = true;
        this.isPromising = false;
        this.dialogBox.position();
        this.tether.position();
    	}
    });
  }

  ngAfterViewInit() {
    if(this.checkedNodes) {
      let selectedNodes: { key: string, extraFieldValue?: any}[] = [];
      let node:{ key: string, extraFieldValue?: any};
      this.checkedNodes.forEach( checkedNode => {
        node = {key: checkedNode.name};
        node.extraFieldValue = checkedNode["extraFieldValue"];
        if(this.attributes) {
          let attribute = this.attributes.find( item => item.key == checkedNode.name);
          if(attribute) attribute.extraFieldValue = checkedNode["extraFieldValue"];
        }
        selectedNodes.push(node);
      });
      if(this.tree) this.tree.selectedNodes = selectedNodes;
      this.dialogBox.position();
      this.tether.position();
      this.changeDetector.detectChanges();
    }
  }

  treeActionHandler(event:{action: string, data: any}) {
    let node: {  name: string, label: string, type?: any, params?: any };
    switch(event.action) {
      case "add":
        this.selectedAttributes.push({
          name: event.data.key,
          label: event.data.title,
          type: this.type,
          params: {attribute: event.data}
        });
        this.dialogBox.position();
        this.tether.position();
      break;
      case "remove":
        node = this.selectedAttributes.find( attribute => attribute.name == event.data.key );
        if(node) this.selectedAttributes.splice(this.selectedAttributes.indexOf(node), 1);
      break;
      case "patch":
        node = this.selectedAttributes.find( attribute => attribute.name == event.data.key );
        let fValue: number = parseInt(event.data.extraFieldValue);
        if(fValue > 100) fValue = 100;
        if(node) node.label = fValue > 0 ? event.data.title + " [ <i>f:</i><span>" + fValue + "</span>]" : event.data.title;
      break;
    }
  }

  treeChangeHandler(event: {key: string, title: string}[]) {
    let attributes: any[] = [];
    event.forEach( node => {
      attributes.push({
        name: node.key,
        label: node.title,
        type: this.type,
        extraFieldValue: node["extraFieldValue"],
        params: {attribute: node}
      });
      this.selectedAttributes = attributes;
      this.dialogBox.position();
      this.tether.position();
    })
  }

  tagChangeHandler(event) {

  }

  tagActionHandler(event: {action: string, data?:{name: string}}) {
    switch(event.action) {
      case "remove":
      this.tree.select({key: event.data.name}, false);
      break;
    }
  }

  submit() {
    this.selectedAttributes.map(item => item["extraFieldValue"] = item.params.attribute["extraFieldValue"]);
    this.tether.close({
      type: this.type,
      attributes: this.selectedAttributes
    });
  }

}
