import { Component, OnInit, EventEmitter, Output, HostBinding, ChangeDetectorRef } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TetherDialog} from '../../components/tether-dialog/tether-dialog';


@Component({
	selector: 'app-group-sale-contact',
	templateUrl: './group-sale-contact.component.html',
	styleUrls: ['./group-sale-contact.component.scss']
})
export class GroupSaleContactComponent implements OnInit {
  @HostBinding('class.oc-group-sale-contact') true;

	@Output() formSubmitEvent: EventEmitter<any> = new EventEmitter();

	private contactForm: FormGroup = new FormGroup({});

	constructor(
		private tetherService: TetherDialog,
		private changeDetector: ChangeDetectorRef
	) { }

	ngOnInit() {
	}

	ngAfterViewInit() {
		this.changeDetector.detectChanges();
	}

	sendFormData() {
		this.formSubmitEvent.emit(this.contactForm.value);
		this.tetherService.close({contact: this.contactForm.value});
	}

}
