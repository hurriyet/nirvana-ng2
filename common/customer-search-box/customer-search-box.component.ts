import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, HostBinding, ViewChild, EventEmitter, Output, Input,ContentChild,OnDestroy } from '@angular/core';
import { TetherDialog } from '../../components/tether-dialog/tether-dialog';
import { TypeaheadComponent } from '../../components/typeahead/typeahead.component';
import { AuthenticationService } from '../../services/authentication.service';
import { Http, URLSearchParams, Headers, RequestOptions} from "@angular/http";
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-customer-search-box',
  templateUrl: './customer-search-box.component.html',
  styleUrls: ['./customer-search-box.component.scss']
})
export class CustomerSearchBoxComponent implements OnInit {
  @ViewChild(TypeaheadComponent) typeahead: TypeaheadComponent;
  @HostBinding('class.c-customer-search-box') true;

  @HostBinding('class.c-customer-search-box--narrow') private isNarrow: boolean;

  @Output() resultEvent: EventEmitter<any> = new EventEmitter();
  @Output() dismissEvent: EventEmitter<any> = new EventEmitter();
  @Output() actionEvent: EventEmitter<Object> = new EventEmitter();
  @Output() formSubmitEvent: EventEmitter<any> = new EventEmitter();

  @Input() title: string;

  public options:any;

  private presets: Observable<{}> | Observable<{ title: string, list: {id: any, title: string, icon?: string, description?:string, params?: any}[] }[]> = Observable.of([]);
  private searchResults: Observable<{}> | Observable<{ title: string, list: { title?: string, icon?: string, MemberId: any,Name: string,Surname: string,DateOfBirth?: any,Gender: string,PhoneNumber: string,Email: string,ProfilePicture?: string, }[]}[]>;

  @Input() set theme(value:string) {
    this.isNarrow = value == "narrow";
  }

  private guestForm: FormGroup = new FormGroup({});
  private placeholder: {};
  public selectedPlaceholder:string;
  private name:any;
  private surname:any;
  private email:any;
  private phone:any;
  private city?:any;
  private town?:any;
  private formData = [];
  private queryType : number = 1;

  private get titleCase():string {
    return !this.addUser ? "Müşteri Arayın" : "Konuk Hesabı Ekle";
  }

  private addUser:boolean = false;
  private resultEventSubscription:any;
  private dismissEventSubscription:any;
	private get isValid():boolean {
		if(this.email && this.surname && this.phone && this.email){
			return true;
		}else{
			return false
		}
	};
  constructor(
    private tetherService: TetherDialog,
    private http : Http,
    private authenticationService : AuthenticationService
    ) {
  }

  ngOnInit() {
    this.options = [
      {'text':'İsim ile','value': '1'},
			{'text': 'GSM ile','value': '2'},
      {'text': 'E-posta ile','value': '3'}
    ];

    this.placeholder = {
      '1':'İsme Göre Ara',
      '2': '05XX XXX XX XX',
      '3': '@'
    }
    this.selectedPlaceholder = this.placeholder[this.queryType];
  }

  showGuestUserForm() {
    this.addUser = true;
  }

  hideGuestUserForm() {
    this.addUser = false;
  }

  resultHandler(event) {
    this.resultEvent.emit(event);
    this.tetherService.close(event);
  }

  dismissHandler(event) {
    this.dismissEvent.emit(event);
    this.tetherService.dismiss(event);
  }

  inputHandler($event,field) {
    switch(field){
      case 'name':
        this.name = $event;
      break;
      case 'surname':
        this.surname = $event;
      break;
      case 'email':
        this.email = $event;
      break;
      case 'phone':
        this.phone = $event;
    }
  }

  onSelectboxChange(event){
    this.queryType = event;
    this.selectedPlaceholder = this.placeholder[this.queryType];
  }

  townEvent(event) {
    this.city = event.city;
    this.town = event.town;

  }

  sendFormData() {
    //Todo konuk oturumu aç ya da event gönder
    this.formSubmitEvent.emit(this.guestForm.value);
    this.tetherService.close({params: {customer: this.guestForm.value}}); //narrow-client-display için seçilmiş arama sonucu gibi maplendi. Town ve City servisin durumuna göre güncellenecek
  }

  typeaheadActionHandler(event) {
    switch(event.action) {
      case "showGuestUserForm":
        this.showGuestUserForm();
      break;
    }
  }

  search(value){
  	if(value && value.length > 2){
	  	let limit = 10;
	  	let channelCode = this.authenticationService.getUserChannelCode();
	  	let firmCode  = this.authenticationService.getUserFirmCode();
	  	let apiUrl = environment.api.boxoffice;
	    let crmApiUrl = environment.api.boxoffice + '/api/v1.0/'+ firmCode +'/'+ channelCode +'/Crm/CallService/SearchCustomers';
	    	crmApiUrl += '?page=1';
	    	crmApiUrl += '&limit=' + limit;
	    	crmApiUrl += '&query=' + value;
	    	crmApiUrl += '&queryType=' + this.queryType;
	        let listData = []
	        let headers = new Headers();
	        headers.append('Content-Type', 'application/json');
	 		headers.append('Authorization', 'bearer ' + this.authenticationService.getToken());
	    	let member = this.http.post(crmApiUrl, {}, { headers: headers }).subscribe(response => {
	    	let payload = response.json();

	    	if(payload['EntityModel'] && payload['EntityModel']['Items']){
	          for(let i = 0; i < payload['EntityModel']['Items'].length;i++){
	            listData.push({
	              title: payload['EntityModel']['Items'][i]['Name'] + ' ' + payload['EntityModel']['Items'][i]['Surname'],
	              icon:'person',
	              params: {customer: payload['EntityModel']['Items'][i]}
	            });
	          }
	          this.searchResults = Observable.of([{title: 'ARAMA SONUÇLARI', list: listData }]);
	    		}else{
	    			this.searchResults = Observable.of([]);
	    		}
	    	});
    }
  }
}
