import { Component, OnInit } from '@angular/core';
import { TetherDialog } from '../../components/tether-dialog/tether-dialog';

@Component({
  selector: 'app-related-content',
  templateUrl: './related-content.component.html',
  styleUrls: ['./related-content.component.scss']
})
export class RelatedContentComponent implements OnInit {
	private tetherService: TetherDialog;
  	constructor(tetherService: TetherDialog) {
    	this.tetherService = tetherService;
  	}
  ngOnInit() {
  }

}
