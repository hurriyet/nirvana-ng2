import { TextInputComponent } from './../../components/text-input/text-input.component';
import { Variant } from './../../models/variant';
import { PriceList } from './../../models/price-list';
import { PriceAdjustmentType } from './../../models/price-adjustment-type.enum';
import { TetherDialog } from './../../components/tether-dialog/tether-dialog';
import { Component, OnInit, Input, HostBinding, HostListener, ViewChild, ViewChildren, QueryList, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-add-variant-box',
  templateUrl: './add-variant-box.component.html',
  styleUrls: ['./add-variant-box.component.scss']
})
export class AddVariantBoxComponent implements OnInit {
  @ViewChildren(TextInputComponent) textInputs: QueryList<TextInputComponent>;
  @HostBinding('class.oc-add-variant-box') true;

  @HostListener('keyup.enter') enterHandler(){
    this.submit();
  };

  @Input() title: string;
  @Input() priceList: PriceList;
  @Input() variant: Variant;

  private priceAdjustmentTypes: { value: any, text: string }[];

  private validation: {
		Name: { isValid: any, message: string },
		DefaultPrice: { isValid: any, message: string }
	} = {
		Name: {
			message: "Varyant Adı zorunludur.",
			isValid(): boolean {
				return this.variant && this.variant.Localization && this.variant.Localization.Tr && this.variant.Localization.Tr.Name && this.variant.Localization.Tr.Name.length > 0;
			}
		},
		DefaultPrice: {
			message: "Fiyat bilgisi zorunludur!",
			isValid():boolean {
				return this.variant && this.variant.DefaultPrice > 0;
			}
		}
	};

	public get isValid():boolean {
		if( this.priceList && this.validation
			&& this.validation.Name.isValid.call(this)
			&& this.validation.DefaultPrice.isValid.call(this)
			){
			return true;
		}else{
			// if( this.priceList && this.validation) console.log(
			// 	this.validation.Name.isValid.call(this),
			// 	this.validation.BeginDate.isValid.call(this),
			// 	this.validation.EndDate.isValid.call(this),
      //   this.validation.DefaultPrice.isValid.call(this)
			// )
			return false
		}
	};
  
  constructor(
    private tether: TetherDialog,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.priceAdjustmentTypes = [];
    this.priceAdjustmentTypes.push({value: PriceAdjustmentType.NotSet, text: "Σ"});
    this.priceAdjustmentTypes.push({value: PriceAdjustmentType.Percent, text: "%"});
    if(!this.title) this.title = "Varyant Ekle";
    if(!this.variant) {
      this.variant = new Variant();
      this.variant.DefaultServiceFeeAdjType = PriceAdjustmentType.NotSet;
      this.variant.DefaultServiceFee = 0;
      this.variant.IsActive = true;
    } 
    if(!this.variant.DefaultPrice && this.priceList) this.variant.DefaultPrice = this.priceList.NominalPrice;
    this.changeDetector.detectChanges();
  }

  ngAfterViewInit() {
    this.textInputs.first.focus();
  }

  private inputChangeHandler(event, name:string) {
    switch(name) {
      case "Name":
        if(!this.variant.Localization) this.variant.Localization = {};
        if(!this.variant.Localization.Tr) this.variant.Localization.Tr = {};
        this.variant.Localization[name] = event;
        this.variant.Localization.Tr[name] = event;
        console.log(this.variant);
      break;
      case "DefaultServiceFee":
        this.variant.DefaultServiceFeeAdjType = event.select;
        this.variant.DefaultServiceFee = event.value;
      break;
      default:
        this.variant[name] = event;
      break;
    }
  }

  private submit() {
    if(this.isValid) this.tether.close(this.variant);
  }

}
