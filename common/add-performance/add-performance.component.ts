import { Component, OnInit, HostBinding, ViewChild, Input } from '@angular/core';
import { TetherDialog } from '../../components/tether-dialog/tether-dialog';
import { TypeaheadComponent } from '../../components/typeahead/typeahead.component';

@Component({
  selector: 'app-add-performance',
  templateUrl: './add-performance.component.html',
  styleUrls: ['./add-performance.component.scss']
})
export class AddPerformanceComponent implements OnInit {
  @ViewChild(TypeaheadComponent) typeahead: TypeaheadComponent;

  @HostBinding('class.oc-performer-create') true;

  @Input() title: string;

  constructor(private tetherService:TetherDialog) {
    this.tetherService = tetherService;
  }

  ngOnInit() {
  }

  public submit() {
    let result = {
      status: "Sended From add Performance"
    };
    this.tetherService.close(result);
  }

}
