export class BaseModel {
	constructor(object : Object = {}){
    	for (let key in object) {
    		this[key] = object[key];
		}
    }
    /* Check if key exist */
    set(key, value){
    	this[key] = value;
    }
}
