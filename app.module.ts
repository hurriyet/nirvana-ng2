import { ComponentsModule } from './modules/components/components.module';
import { UiComponentsModule } from './modules/ui-components/ui-components.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { LoginRoutingModule } from './login-routing.module';
import { NgUploaderModule } from 'ngx-uploader';
import { TetherDialogModule } from './components/tether-dialog/tether-dialog.module'; //Module

import { AppComponent } from './app.component';
import { MainComponent } from './routes/main/main.component';
import { TabBarComponent } from './components/tab-bar/tab-bar.component';
import { ContextMenuComponent } from './components/context-menu/context-menu.component';
import { CoverImageComponent } from './components/cover-image/cover-image.component';
import { InlineSearchInputComponent } from './components/inline-search-input/inline-search-input.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { OverImageStatsComponent } from './components/over-image-stats/over-image-stats.component';
import { PillGroupComponent } from './components/pill-group/pill-group.component';
import { ResponsiveTableComponent } from './components/responsive-table/responsive-table.component';
import { CollapsibleSidebarComponent } from './components/collapsible-sidebar/collapsible-sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { SelectboxComponent } from './components/selectbox/selectbox.component';
import { CardItemComponent } from './components/card-item/card-item.component';
import { AddButtonComponent } from './components/add-button/add-button.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { ButtonComponent } from './components/button/button.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { DataSourceListComponent } from './components/data-source-list/data-source-list.component';
import { FieldSearchBarComponent } from './components/field-search-bar/field-search-bar.component';
import { HeaderInlineEditComponent } from './components/header-inline-edit/header-inline-edit.component';
import { HeaderSearchBarComponent } from './components/header-search-bar/header-search-bar.component';
import { InlineIconGroupComponent } from './components/inline-icon-group/inline-icon-group.component';
import { ModalSearchBoxComponent } from './components/modal-search-box/modal-search-box.component';
import { MultiSelectionBarComponent } from './components/multi-selection-bar/multi-selection-bar.component';
import { NumberStepperComponent } from './components/number-stepper/number-stepper.component';
import { PrimaryColumnLineComponent } from './components/primary-column-line/primary-column-line.component';
import { ProfilePhotoComponent } from './components/profile-photo/profile-photo.component';
import { PromiseIconComponent } from './components/promise-icon/promise-icon.component';
import { RadioButtonComponent } from './components/radio-button/radio-button.component';
import { SliderInputComponent } from './components/slider-input/slider-input.component';
import { SortTitleComponent } from './components/sort-title/sort-title.component';
import { SortViewComponent } from './components/sort-view/sort-view.component';
import { TypeaheadComponent } from './components/typeahead/typeahead.component';
import { MultiSelectGroupComponent } from './components/multi-select-group/multi-select-group.component';
import { ContainerCanvasComponent } from './components/container-canvas/container-canvas.component';
import { IconGridComponent } from './components/icon-grid/icon-grid.component';
import { VenuesComponent } from './routes/venues/venues.component';
import { PerformersComponent } from './routes/performers/performers.component';
import { EventsComponent } from './routes/events/events.component';
import { VenueComponent } from './routes/venue/venue.component';
import { VenueEventsComponent } from './routes/venue/venue-events/venue-events.component';
import { VenueLayoutsComponent } from './routes/venue/venue-layouts/venue-layouts.component';
import { VenueEventsCalendarComponent } from './routes/venue/venue-events-calendar/venue-events-calendar.component';
import { VenueMediaMaterialsComponent } from './routes/venue/venue-media-materials/venue-media-materials.component';
import { EventComponent } from './routes/event/event.component';
import { SeatingArrangementCreateComponent } from './common/seating-arrangement-create/seating-arrangement-create.component';
import { WizardHeaderComponent } from './components/wizard-header/wizard-header.component';
import { HrefDirective } from './directives/href.directive';
import { PerformerCreateComponent } from './common/performer-create/performer-create.component';
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';
import { FeedbackFormComponent } from './common/feedback-form/feedback-form.component';
import { IndexComponent } from './routes/index/index.component';
import { ConfirmModalComponent } from './components/confirm-modal/confirm-modal.component';
import { HeaderLargeSearchComponent } from './components/header-large-search/header-large-search.component';
import { HeaderTitleService } from './services/header-title.service';
import { MockService } from './services/mock.service';
import { NarrowInlineFeedbackComponent } from './components/narrow-inline-feedback/narrow-inline-feedback.component';

import { EventPerformancesComponent } from './routes/event/event-performances/event-performances.component';
import { EventProductsComponent } from './routes/event/event-products/event-products.component';
import { EventStatisticsComponent } from './routes/event/event-statistics/event-statistics.component';
import { EventSponsorsComponent } from './routes/event/event-sponsors/event-sponsors.component';
import { ProductsComponent } from './routes/products/products.component';

import { CtaBoxComponent } from './components/cta-box/cta-box.component';
import { MiniCardComponent } from './components/mini-card/mini-card.component';
import { AddPerformanceComponent } from './common/add-performance/add-performance.component';

import { InlineEditComponent } from './components/inline-edit/inline-edit.component';
import { ContainerRolloverActionsComponent } from './components/container-rollover-actions/container-rollover-actions.component';
import { TreeViewComponent } from './components/tree-view/tree-view.component';

import { EventService } from './services/event.service';
import { StoreService } from './services/store.service';
import { PerformancesComponent } from './routes/performances/performances.component';
import { StickyDirective } from './directives/sticky.directive';
import { VerticalKvListComponent } from './components/vertical-kv-list/vertical-kv-list.component';
import { ProductItemLineComponent } from './components/product-item-line/product-item-line.component';
import { AppSettingsService } from './services/app-settings.service';
import { ImagePipe } from './pipes/image.pipe';
import { PhoneFormatPipe } from './pipes/phone-format.pipe';
import { TitleSwitcherComponent } from './components/title-switcher/title-switcher.component';
import { NarrowColBasketItemComponent } from './components/narrow-col-basket-item/narrow-col-basket-item.component';
import { PerformanceComponent } from './routes/performance/performance.component';
import { PerformancePerformersComponent } from './routes/performance/performance-performers/performance-performers.component';
import { PerformanceProductsComponent } from './routes/performance/performance-products/performance-products.component';
import { PerformanceProductStatisticsComponent } from './routes/performance/performance-product-statistics/performance-product-statistics.component';
import { PerformanceSponsorsComponent } from './routes/performance/performance-sponsors/performance-sponsors.component';

import { ProductsByproductsComponent } from './routes/products/products-byproducts/products-byproducts.component';
import { ProductsBundlesComponent } from './routes/products/products-bundles/products-bundles.component';
import { ProductsPerformancesComponent } from './routes/products/products-performances/products-performances.component';

import { NarrowClientDisplayComponent } from './components/narrow-client-display/narrow-client-display.component';
import { BoxofficeComponent } from './routes/boxoffice/boxoffice.component';
import { BoxofficeProductsComponent } from './routes/boxoffice/boxoffice-products/boxoffice-products.component';
import { BoxofficeEventsComponent } from './routes/boxoffice/boxoffice-events/boxoffice-events.component';

import { EventsMasterComponent } from './routes/events/events-master/events-master.component';
import { EventsMultiplePerformanceComponent } from './routes/events/events-multiple-performance/events-multiple-performance.component';
import { EventsSinglePerformanceComponent } from './routes/events/events-single-performance/events-single-performance.component';
import { NarrowColActionBlockComponent } from './components/narrow-col-action-block/narrow-col-action-block.component';
import { NarrowColStatusFeedbackComponent } from './components/narrow-col-status-feedback/narrow-col-status-feedback.component';
import { NarrowColActionOfferComponent } from './components/narrow-col-action-offer/narrow-col-action-offer.component';
import { FullscreenCoverComponent } from './components/fullscreen-cover/fullscreen-cover.component';
import { AuthDialogBoxComponent } from './components/auth-dialog-box/auth-dialog-box.component';
import { TextInputComponent } from './components/text-input/text-input.component';
import { LoginComponent } from './routes/login/login.component';
import { PasswordRecoveryComponent } from './routes/login/password-recovery/password-recovery.component';
import { LockComponent } from './routes/login/lock/lock.component';
import { RelatedContentComponent } from './common/related-content/related-content.component';
import { MockBoxComponent } from './components/mock-box/mock-box.component';
import { WideColBasketItemComponent } from './components/wide-col-basket-item/wide-col-basket-item.component';
import { PerformersMusiciansComponent } from './routes/performers/performers-musicians/performers-musicians.component';
import { PerformersArtistsComponent } from './routes/performers/performers-artists/performers-artists.component';
import { PerformersOthersComponent } from './routes/performers/performers-others/performers-others.component';
import { PerformersIndexComponent } from './routes/performers/performers-index/performers-index.component';
import { EventsIndexComponent } from './routes/events/events-index/events-index.component';
import { PerformancesIndexComponent } from './routes/performances/performances-index/performances-index.component';
import { VenueObjectMatcherPipe } from './pipes/venue-object-matcher.pipe';
import { ListItemDirective } from './directives/list-item.directive';
import { GridListComponent } from './components/grid-list/grid-list.component';
import { NarrowColEventCalendarComponent } from './components/narrow-col-event-calendar/narrow-col-event-calendar.component';
import { MultiInlineEditComponent } from './components/multi-inline-edit/multi-inline-edit.component';
import { BasicButtonGroupComponent } from './components/basic-button-group/basic-button-group.component';
import { PerformanceCreateComponent } from './routes/performance-create/performance-create.component';
import { CollapsibleContainerComponent } from './components/collapsible-container/collapsible-container.component';
import { VenueEditComponent } from './routes/venue/venue-edit/venue-edit.component';
import { EventEditComponent } from './routes/event/event-edit/event-edit.component';
import { CityTownSelectorComponent } from './components/city-town-selector/city-town-selector.component';
import { BoxofficeContentsComponent } from './routes/boxoffice/boxoffice-contents/boxoffice-contents.component';
import { RelativeDatePipe } from './pipes/relative-date.pipe';
import { GetIconPipe } from './pipes/get-icon.pipe';
import { RadioGroupComponent } from './components/radio-group/radio-group.component';
import { BasketDetailComponent } from './components/basket-detail/basket-detail.component';
import { TranslateService } from './services/translate.service';
import { VenueTemplateCreateComponent } from './routes/venue/venue-template-create/venue-template-create.component';
import { SplitContainerComponent } from './components/split-container/split-container.component';
import { FirmSearchSelectComponent } from './components/firm-search-select/firm-search-select.component';
import { NoDataComponent } from './components/no-data/no-data.component';
import { VenueSelectBarComponent } from './components/venue-select-bar/venue-select-bar.component';
import { VenueSearchSelectComponent } from './common/venue-search-select/venue-search-select.component';
import { EnumTranslatorPipe } from './pipes/enum-translator.pipe';
import { HelperTextComponent } from './components/helper-text/helper-text.component';
import { TagGroupComponent } from './components/tag-group/tag-group.component';
import { AttributesSelectAddComponent } from './common/attributes-select-add/attributes-select-add.component';
import { AttributesSelectAddBarComponent } from './components/attributes-select-add-bar/attributes-select-add-bar.component';
import { SanitizeHtmlPipe } from './pipes/sanitize-html.pipe';
import { NotificationBarComponent } from './components/notification-bar/notification-bar.component';
import { NotificationService } from './services/notification.service';
import { MultiplePaymentBlockComponent } from './components/multiple-payment-block/multiple-payment-block.component';
import { GenericLoadingBoxComponent } from './components/generic-loading-box/generic-loading-box.component';
import { BoxofficePurchaseComponent } from './routes/boxoffice/boxoffice-purchase/boxoffice-purchase.component';
import { KeysPipe } from './pipes/keys.pipe';
import { LogPipe } from './pipes/log.pipe';
import { ObjectPipe } from './pipes/object.pipe';
import { VenuesByPerformancesPipe } from './pipes/venues-by-performances.pipe';
import { ShoppingCartService } from './services/shopping-cart.service';
import { PerformerSearchSelectComponent } from './components/performer-search-select/performer-search-select.component';
import { PerformanceSearchSelectComponent } from './components/performance-search-select/performance-search-select.component';
import { CustomerSearchBoxComponent } from './common/customer-search-box/customer-search-box.component';
import { ProductsIndexComponent } from './routes/products/products-index/products-index.component';
import { ProductCreateComponent } from './routes/product/product-create/product-create.component';
import { PerformanceCapacitySearchSelectComponent } from './components/performance-capacity-search-select/performance-capacity-search-select.component';
import { ProductSearchSelectComponent } from './components/product-search-select/product-search-select.component';
import { VenueTemplateEditorComponent } from './components/venue-template-editor/venue-template-editor.component';
import { ColorPickerComponent } from './components/color-picker/color-picker.component';
import { MdEditorComponent } from './components/md-editor/md-editor.component';
import { EventSearchSelectComponent } from './components/event-search-select/event-search-select.component';
import { ActionBoxComponent } from './components/action-box/action-box.component';
import { EventEventsComponent } from './routes/event/event-events/event-events.component';
import { PerformanceReservationsComponent } from './routes/performance/performance-reservations/performance-reservations.component';
import { PerformanceInvitationsComponent } from './routes/performance/performance-invitations/performance-invitations.component';
import { ReservationEditorComponent } from './common/reservation-editor/reservation-editor.component';
import { InvitationEditorComponent } from './common/invitation-editor/invitation-editor.component';
import { CapacityEditorComponent } from './common/capacity-editor/capacity-editor.component';
import { PerformanceCancelBlockComponent } from './routes/performance/performance-cancel-block/performance-cancel-block.component';
import { PerformanceGroupSaleComponent } from './routes/performance/performance-group-sale/performance-group-sale.component';
import { GroupSaleContactComponent } from './common/group-sale-contact/group-sale-contact.component';
import { MdParserPipe } from './pipes/md-parser.pipe';
import { TransactionsComponent } from './routes/transactions/transactions.component';
import { ExpandableBlockComponent } from './components/expandable-block/expandable-block.component';
import { ProductPriceBlockComponent } from './components/product-price-block/product-price-block.component';
import { AddVariantBoxComponent } from './common/add-variant-box/add-variant-box.component';
import { AddVariantPriceBoxComponent } from './common/add-variant-price-box/add-variant-price-box.component';
import { TextInputWithSelectComponent } from './components/text-input-with-select/text-input-with-select.component';
import { EventDatesByPerformancesPipe } from './pipes/event-dates-by-performances.pipe';
import { BoxofficeEventsSubeventsComponent } from './routes/boxoffice/boxoffice-events-subevents/boxoffice-events-subevents.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { ConcatPipe } from './pipes/concat.pipe';
import { MenuItemService } from './services/menu-item.service';
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    EventsComponent,
    VenueComponent,
    //TabBarComponent,
    ContextMenuComponent,
    CoverImageComponent,
    InlineSearchInputComponent,
    //MainMenuComponent,
    OverImageStatsComponent,
    PillGroupComponent,
    ResponsiveTableComponent,
    CollapsibleSidebarComponent,
    HeaderComponent,
    PaginationComponent,
    SelectboxComponent,
    CardItemComponent,
    AddButtonComponent,
    AvatarComponent,
    ButtonComponent,
    //CheckboxComponent,
    DataSourceListComponent,
    FieldSearchBarComponent,
    HeaderInlineEditComponent,
    HeaderSearchBarComponent,
    InlineIconGroupComponent,
    ModalSearchBoxComponent,
    MultiSelectionBarComponent,
    NumberStepperComponent,
    PrimaryColumnLineComponent,
    ProfilePhotoComponent,
    //PromiseIconComponent,
    RadioButtonComponent,
    SliderInputComponent,
    SortTitleComponent,
    SortViewComponent,
    TypeaheadComponent,
    VenuesComponent,
    EventsComponent,
    PerformersComponent,
    EventsComponent,
    VenueComponent,
    VenueEventsComponent,
    VenueLayoutsComponent,
    VenueEventsCalendarComponent,
    VenueMediaMaterialsComponent,
    EventComponent,
    MultiSelectGroupComponent,
    IconGridComponent,
	  ContainerRolloverActionsComponent,
    ContainerCanvasComponent,
    SeatingArrangementCreateComponent,
    WizardHeaderComponent,
    HrefDirective,
    PerformerCreateComponent,
    DialogBoxComponent,
    PerformerCreateComponent,
    FeedbackFormComponent,
    IndexComponent,
    ConfirmModalComponent,
    HeaderLargeSearchComponent,
    PerformerCreateComponent,
    IndexComponent,
    NarrowInlineFeedbackComponent,
    EventPerformancesComponent,
    EventProductsComponent,
    EventStatisticsComponent,
    EventSponsorsComponent,
    ProductsComponent,
    CtaBoxComponent,
    FeedbackFormComponent,
    MiniCardComponent,
    AddPerformanceComponent,
    PerformancesComponent,
    InlineEditComponent,
    TreeViewComponent,
    VerticalKvListComponent,
    ImagePipe,
    ProductItemLineComponent,
    StickyDirective,
    ImagePipe,
    PhoneFormatPipe,
    TitleSwitcherComponent,
    NarrowColBasketItemComponent,
    PerformanceComponent,
    PerformancePerformersComponent,
    PerformanceProductsComponent,
    PerformanceProductStatisticsComponent,
    PerformanceSponsorsComponent,
    ProductsByproductsComponent,
    ProductsBundlesComponent,
    ProductsPerformancesComponent,
    BoxofficeComponent,
    BoxofficeProductsComponent,
    EventsMasterComponent,
    EventsMultiplePerformanceComponent,
    EventsSinglePerformanceComponent,
    NarrowClientDisplayComponent,
    BoxofficeComponent,
    BoxofficeProductsComponent,
    NarrowColActionBlockComponent,
    NarrowColStatusFeedbackComponent,
    NarrowColActionOfferComponent,
    FullscreenCoverComponent,
    AuthDialogBoxComponent,
    //TextInputComponent,
    LoginComponent,
    PasswordRecoveryComponent,
    LockComponent,
    RelatedContentComponent,
    MockBoxComponent,
    PerformersMusiciansComponent,
    PerformersArtistsComponent,
    PerformersOthersComponent,
    PerformersIndexComponent,
    BoxofficeEventsComponent,
    EventsIndexComponent,
    PerformancesIndexComponent,
    VenueObjectMatcherPipe,
    ListItemDirective,
    GridListComponent,
    NarrowColEventCalendarComponent,
    WideColBasketItemComponent,
    VenueObjectMatcherPipe,
    MultiInlineEditComponent,
    BasicButtonGroupComponent,
    PerformanceCreateComponent,
    CollapsibleContainerComponent,
    VenueEditComponent,
    EventEditComponent,
    CityTownSelectorComponent,
    BoxofficeContentsComponent,
    RelativeDatePipe,
    GetIconPipe,
    RadioGroupComponent,
    BasketDetailComponent,
    VenueTemplateCreateComponent,
    SplitContainerComponent,
    FirmSearchSelectComponent,
    NoDataComponent,
    VenueSelectBarComponent,
    VenueSearchSelectComponent,
    EnumTranslatorPipe,
    HelperTextComponent,
    TagGroupComponent,
    AttributesSelectAddComponent,
    AttributesSelectAddBarComponent,
    SanitizeHtmlPipe,
    NotificationBarComponent,
    MultiplePaymentBlockComponent,
    GenericLoadingBoxComponent,
    BoxofficePurchaseComponent,
    KeysPipe,
    LogPipe,
    ObjectPipe,
    VenuesByPerformancesPipe,
    PerformerSearchSelectComponent,
    PerformanceSearchSelectComponent,
    CustomerSearchBoxComponent,
    ProductsIndexComponent,
    ProductCreateComponent,
    PerformanceCapacitySearchSelectComponent,
    ProductSearchSelectComponent,
    VenueTemplateEditorComponent,
    ColorPickerComponent,
    MdEditorComponent,
    EventSearchSelectComponent,
    ActionBoxComponent,
    ProductSearchSelectComponent,
    EventEventsComponent,
    PerformanceReservationsComponent,
    PerformanceInvitationsComponent,
    ReservationEditorComponent,
    InvitationEditorComponent,
    CapacityEditorComponent,
    PerformanceCancelBlockComponent,
    PerformanceGroupSaleComponent,
    GroupSaleContactComponent,
    MdParserPipe,
    TransactionsComponent,
    ExpandableBlockComponent,
    ProductPriceBlockComponent,
    AddVariantBoxComponent,
    AddVariantPriceBoxComponent,
    TextInputWithSelectComponent,
    EventDatesByPerformancesPipe,
    BoxofficeEventsSubeventsComponent,
    BreadcrumbComponent,
    ConcatPipe
 ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    LoginRoutingModule,
    TetherDialogModule.forRoot(),
    UiComponentsModule,
    ComponentsModule,
    NgUploaderModule
  ],
  providers: [HeaderTitleService, MockService, StoreService, AppSettingsService, TranslateService, NotificationService, ShoppingCartService, MenuItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
