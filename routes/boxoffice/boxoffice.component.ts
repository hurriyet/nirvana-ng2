import { Component, OnInit, HostBinding, ViewChild } from '@angular/core';
import { ShoppingCartService } from '../../services/shopping-cart.service';
import { NotificationService } from '../../services/notification.service';
import { HeaderTitleService } from '../../services/header-title.service';
import { AuthenticationService } from '../../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http, URLSearchParams, Headers, RequestOptions } from "@angular/http";
import { OkcVariables } from '../../classes/okc-variables';

declare var $: any;

@Component({
	selector: 'app-boxoffice',
	templateUrl: './boxoffice.component.html',
	styleUrls: ['./boxoffice.component.scss'],
})
export class BoxofficeComponent implements OnInit {
	public userData: Object = null;
	private cartItemsData: Object[];
	private isAsideVisible: boolean = true;
	private selectedType: any = "1";
	private customerSearchResult: Observable<{ title: string, list: any[] }[]>;
	private okcVariables = OkcVariables;
	@HostBinding('class.or-boxoffice') true;
	private cartSubTotal: { cartTotal: number, cartCurrency: string };
	constructor(
		private shoppingCartService: ShoppingCartService,
		private router: Router, private route: ActivatedRoute,
		private headerTitleService: HeaderTitleService,
		private http: Http,
		private authenticationService: AuthenticationService,
		private notificationService: NotificationService
	) {
	}
	//pills : Array <any> = [{text:'BUGÜN', filter:'Type eq 2', count:'5', isActive:true},{text:'YARIN', filter:'Type eq 1', count:'88', isActive:false},{text:'HAFTASONU', filter:'Type eq 3', count:'15', isActive:false}];
	onResize(event) { }
	ngOnInit() {
		this.headerTitleService.setTitle('Box Office');
		this.shoppingCartService.cartItemsSubject.subscribe(items => {
			if (items.length > 0) {
				this.cartItemsData = items;
			} else {
				this.cartItemsData = null;
			}
		});
	}
	onSave(event) {
		//console.log(event);
	}
	userEventCatch(user) {
		if (!user) {
			//User silindiğinde birşeyler yap
			return;
		};

		let headers = new Headers({
			'accept': 'application/json',
			'content-type': 'application/json'
		});
		headers.append('Authorization', 'bearer ' + this.authenticationService.getToken());

		let options = new RequestOptions({ headers: headers });
		let memberInfo = {
			"FirstName": user.Name,
			"FamilyName": user.Surname,
			"Email": user.Email,
			"PhoneNumber": user.PhoneNumber,
			"CitySubdivisionName": user.Town && user.Town.Name ? user.Town.Name : "-",
			"CityName": user.City && user.City.Name ? user.City.Name : "-",
			"Address": "test",
			"Country": "Türkiye"
		};
		let channelCode = this.authenticationService.getUserChannelCode();
		let firmCode = this.authenticationService.getUserFirmCode();
		let setMemberInfo = this.http.post(
			this.shoppingCartService.getBaseUrl() + '/' + firmCode + '/' + channelCode + '/Transaction/SetMemberInfo',
			memberInfo,
			options
		);
		setMemberInfo.subscribe(result => {
			this.notificationService.add({ text: 'Müşteri eklendi.', type: 'success' });
			this.shoppingCartService.setCartUser(user);
		}, error => {
			this.notificationService.add({ text: 'Müşteri sepete eklenemedi, eksik bilgi.', type: 'warning' });
		});

	}
	catch($event) {
		console.log("Form Event Catch", $event);
	}

	goToBasket() {
		if (this.shoppingCartService.getCartUser()) {
			// Sepete Gitmeden Önce Okc üzerinde bir işlem var ise iptal edilecek
			let cancelReceiptResponse = window['DeviceIntegrationInstance'].cancelReceipt();
			if (cancelReceiptResponse.result.isTransactionSuccess) {
				if (cancelReceiptResponse.result.deviceResultValue == this.okcVariables.CABLE_ERROR) {
					this.notificationService.add({ text: 'İletişim Problemi !! Lütfen OKC Cihazı Kablosunu Kontrol Ediniz.', type: 'warning' });
				}
				else {
					this.router.navigate(['/boxoffice/purchase']);
				}
			}
			else {
				this.notificationService.add({ text: 'Bir hata oluştu işleme devam edilemiyor', type: 'error' });
			}

		} else {
			this.notificationService.add({ text: 'Devam etmek için müşteri bilgisi eklemeniz gerekmektedir.', type: 'warning' });
		}
	}
	resetBasket() {
		this.shoppingCartService.flushCart();
		this.notificationService.add({ text: 'Sepetteki ürünler kaldırıldı.', type: 'success' });
		this.cartItemsData = [];
		this.router.navigate(['/boxoffice']);
	}
	handleBasketAction(event) {
		switch (event.action) {
			case "removeProduct":
				this.shoppingCartService.removeFromCart(event.variantId);
				break;
		}
	}

}
