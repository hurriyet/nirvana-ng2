import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../../../services/shopping-cart.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from '../../../services/notification.service';
@Component({
  selector: 'app-boxoffice-products',
  templateUrl: './boxoffice-products.component.html',
  styleUrls: ['./boxoffice-products.component.scss']
})
export class BoxofficeProductsComponent implements OnInit {

	private subscription;
	private count : number;
	private event : Object[];
	private errorMessage;
	private eventId : number;
  	constructor(
  		private shoppingCartService:ShoppingCartService,
  		private router: Router,
		private route: ActivatedRoute,
		private notificationService : NotificationService
	) {
  		this.shoppingCartService.setCustomEndpoint('SelectEvent', true);
  	}
  	ngOnInit() {
  		this.route.params.subscribe(param => {
  			if(param){
  				this.eventId = +param["id"];
  			}
  		});
 		this.subscription = this.shoppingCartService.data.subscribe(result => {
			if(result && result["State"] && result["CurrentState"] == 1){
				this.event = result["State"]["Event"];
			}
		},error => {
			console.log('ERROR ', error['Message'])
			this.notificationService.add({text:error['Message'], type:'danger'});
			this.router.navigate(['boxoffice']);
		});
		let save = this.shoppingCartService.create({'EventId':this.eventId});
		save.subscribe(item => {
			if(item && item.CurrentState){
				this.shoppingCartService.setCustomEndpoint('GetCurrentState', true);
				this.shoppingCartService.query({});
			}
		}, error => {
			this.notificationService.add({text:error['Message'], type:'danger'});
			this.router.navigate(['boxoffice']);
		})
  	}
  	maxProductCount(product){
  		return (product && product['MaxAllowedCountOfProducts'] && product['MaxAllowedCountOfProducts'] > 0) ? product['MaxAllowedCountOfProducts'] : 0;
  	}
  	onSave(event){
  		switch(event.action){
  			case "addToCart":
  				this.shoppingCartService.addToCart({event: event.event, variant: event.variant, product: event.product, count: event.piece});
  			break;
  		}
  	}
	ngOnDestroy(){
  		if(this.subscription) this.subscription.unsubscribe();
  	}
}
