import { Component, OnInit,Input } from '@angular/core';
import { EntityService } from '../../../services/entity.service';
import { ShoppingCartService } from '../../../services/shopping-cart.service';
import { EventStatus } from '../../../models/event-status.enum';
import * as moment from 'moment';
@Component({
  selector: 'app-boxoffice-events',
  templateUrl: './boxoffice-events.component.html',
  styleUrls: ['./boxoffice-events.component.scss'],
  providers: [ EntityService ]
})
export class BoxofficeEventsComponent implements OnInit {
	private subscription;
	private count : number;
	private isPromising: boolean = false;
	private isLoading:boolean = false;
	private results : Object;
	private errorMessage;
	private isTodayFilterActive : boolean = false;
	private isTomorrowFilterActive : boolean = false;
  	private pills : Array <any> = [{text:'BUGÜN', filter:'today', isActive:true},{text:'YARIN', filter:'tomorrow', isActive:false}];
  	private noDataInContent :boolean = false;
	private isEventSearchActive : boolean = true;
  	private isVenueSearchActive : boolean = false;
  	private eventStatus = EventStatus;
  	private selectedPill = null;
  	constructor(private entityService:EntityService, private shoppingCartService : ShoppingCartService) {

  	}
  	ngOnInit() {
  		this.entityService.data.subscribe(
			results => {
				this.isPromising = false;
				this.isLoading = false;
				this.results = results;
			},
			error =>  this.errorMessage = <any>error
		);
		this.subscription = this.entityService.queryParamSubject.subscribe(
			params => {
				this.getEventFeed();
			},
			error =>  this.errorMessage = <any>error
		);
		this.entityService.count.subscribe(
			count => { this.count = count; },
			error =>  this.errorMessage = <any>error
		);

  	}
  	search(event, resetPills : boolean = true){
  		let query : any;
  		this.isVenueSearchActive = false;
  		this.isEventSearchActive = false;
  		if(resetPills){
  			this.selectedPill = {};
  		}

  		switch(event.searchType){
  			case 'event':
			  	this.isPromising = true;
			  	this.isEventSearchActive = true;
			  	if(event.searchValue){
				  	query = this.entityService.fromEntity('EEvent')
	        			.where('Status','=',this.eventStatus.OnSale,'EventStatus')
	        			.expand(['Localization'])
	        			.expand(['Performances','VenueTemplate','Venue','Localization'])
	        			.expand(['Performances','VenueTemplate','Venue','Town','City'])
	        			.select(['Localization','Performances','Status','Images','ChildEventCount','Id'])
	        			.selectOnExpand(['VenueTemplate','Date','Id'],1)
	        			.search('Localization/Name',event.searchValue)
	                	.take(40)
	                	.page(0)
	                	.executeQuery();
                }else{
                	this.getEventFeed();
                }
  			break;
  			case 'venue':
  				if(event.searchValue){
				  	this.isPromising = true;
				  	this.isVenueSearchActive = true;
				  	query = this.entityService.fromEntity('VVenue')
	        			.expand(['Localization'])
	        			.search('Localization/Name', event.searchValue)
	                	.take(40)
	                	.page(0)
	                	.executeQuery();
                }else{
                	this.getVenueFeed();
                }
  			break;
  			case 'date':
			  	this.isPromising = true;
			  	this.isEventSearchActive = true;
			  	if(event.searchValue){
				  	let start = moment(event.searchValue).utcOffset(0).startOf('day').toISOString();
				  	let end = moment(event.searchValue).utcOffset(0).endOf('day').toISOString();
				  	query = this.entityService.fromEntity('EEvent')
	        			.where('Status','=',this.eventStatus.OnSale,'EventStatus')
	        			.andRaw('(Performances/any(p:p/Date gt '+start+'))')
	        			.andRaw('(Performances/any(p:p/Date lt '+end+'))')
	        			.expand(['Localization'])
	        			.expand(['Performances','VenueTemplate','Venue','Localization'])
	        			.expand(['Performances','VenueTemplate','Venue','Town','City'])
	        			.select(['Localization','Performances','Status','Images','ChildEventCount','Id'])
	        			.selectOnExpand(['VenueTemplate','Date','Id'],1)
	                	.take(40)
	                	.page(0)
	                	.executeQuery();
	                }else{
	                	this.getEventFeed();
	                }
  			break;
  		}
  	}
  	private getEventFeed(){
  		this.isEventSearchActive = true;
  		this.entityService.setCustomEndpoint("GetAll");
        let query = this.entityService.fromEntity('EEvent')
        			.where('Status','=',this.eventStatus.OnSale,'EventStatus')
        			.expand(['Localization'])
        			.expand(['Performances','VenueTemplate','Venue','Localization'])
        			.expand(['Performances','VenueTemplate','Venue','Town','City'])
        			.select(['Localization','Performances','Status','Images','ChildEventCount','Id'])
        			.selectOnExpand(['VenueTemplate','Date','Id'],1)
                	.take(40)
                	.page(0)
                	.executeQuery();
  		this.results = null;
		this.isLoading = true;

  	}
  	private getVenueFeed(){
  		this.isVenueSearchActive = true;
  		this.entityService.setCustomEndpoint("GetAll");
        this.isPromising = true;
		let query = this.entityService.fromEntity('VVenue')
	        			.expand(['Localization'])
	                	.take(10)
	                	.page(0)
	                	.executeQuery();
  		this.results = null;
		this.isLoading = true;
  	}
  	filterEvents(event){
  		switch(event.filter){
  			case 'today':
	  			if(this.isTodayFilterActive){
	  				this.isTodayFilterActive = false;
	  				this.getEventFeed();
	  			}else{
	  				this.isTodayFilterActive = true;
	  				this.search({searchType:'date',searchValue:moment().utcOffset(0).startOf('day').toISOString()}, false)
	  			}
  			break;
  			case 'tomorrow':
  				if(this.isTomorrowFilterActive){
	  				this.isTomorrowFilterActive = false;
	  				this.getEventFeed();
	  			}else{
	  				this.isTomorrowFilterActive = true;
					this.search({searchType:'date',searchValue:moment().utcOffset(0).add(1,'days').startOf('day').toISOString()}, false)
	  			}
  			break;
  		}
  	}
}
