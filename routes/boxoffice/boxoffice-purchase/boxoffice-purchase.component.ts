import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../../../services/shopping-cart.service';
import { Router } from '@angular/router';
import { NotificationService } from '../../../services/notification.service';
@Component({
  selector: 'app-boxoffice-purchase',
  templateUrl: './boxoffice-purchase.component.html',
  styleUrls: ['./boxoffice-purchase.component.scss'],
  //providers: [ ShoppingCartService ]
})
export class BoxofficePurchaseComponent implements OnInit {
   	tabs: Array<any> = [{}];
   	private result : Object;
   	private subscription;
  	constructor(private shoppingCartService:ShoppingCartService, private router : Router, private notificationService : NotificationService) {
  	}

  	ngOnInit() {
  		/* TODO !!!! CHECK IF USER HAS BEEN SET FOR CART !!!! */
  		this.shoppingCartService.setCustomEndpoint('GotoBasket',true);
		let changeState = this.shoppingCartService.create({});
		changeState.subscribe(state => {
			console.log('state', state);
			if(state && state.length == undefined){
				this.shoppingCartService.setCustomEndpoint('GetCurrentState',true);
				this.shoppingCartService.query({});
				this.subscription = this.shoppingCartService.data.subscribe(results => {
					console.log(results);
					if(results && results['CurrentState'] == 4){
						this.result = results["State"];
					}
				},error => {
					this.notificationService.add({text:error['Message'], type:'danger'});
					this.router.navigate(['boxoffice']);
				});
			}
		},error => {
			this.notificationService.add({text:error['Message'], type:'danger'});
			this.router.navigate(['boxoffice']);
		});
  }
  paymentActionHandler(event){
  	switch (event.action) {
  		case "redirect":
  			this.shoppingCartService.flushCustomEndpoint();
  			this.shoppingCartService.flushCart();
  			this.notificationService.add({text:'Ödeme başarıyla gerçekleştirilmiştir.', type:'success'})
  			this.router.navigate(['boxoffice']);
  		break;
  	}
  }
	ngOnDestroy(){
  		if(this.subscription) this.subscription.unsubscribe();
  	}

}
