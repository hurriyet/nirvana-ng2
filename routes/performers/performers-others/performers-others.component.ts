import { Component, OnInit, HostBinding } from '@angular/core';
import { Performer } from './../../../models/performer';
import { Observable } from 'rxjs/Observable';
import { PerformerService } from '../../../services/performer.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-performers-others',
    templateUrl: './performers-others.component.html',
    styleUrls: ['./performers-others.component.scss']
})
export class PerformersOthersComponent implements OnInit {
    @HostBinding('class.app-performers-list-view') true;
    performers: Performer[];
    params: Object;
    sort: Array<Object> = [];
    pageSizes: Array<Object> = [{ text: '10', value: 10 }, { text: '20', value: 20 }];
    pageSize: number = 10;
    page: number = 0;
    constructor(
        private performerService: PerformerService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        performerService.setQueryParams({ filter: [{ filter: 'Type eq Other' }], sort: [], pageSize: 10, page: 1 });
    }
    count: number;
    errorMessage: any;
    selectedItems: Array<Object> = [];
    isAllSelected: boolean = false;
    actionButtons: Array<Object> = [
        { label: 'Kopyala', icon: 'layers', action: 'copy' },
        { label: 'Gizle', icon: 'visibility', action: 'visibilityOn' },
        { label: 'Göster', icon: 'visibility_off', action: 'visibilityOff' },
        { label: 'Arşivle', icon: 'archive', action: 'archive' },
        { label: 'Sil', icon: 'delete', action: 'delete' },
    ];
    isListViewActive: boolean = true;
    isCardViewActive: boolean = false;
    subscription;
    ngOnInit() {
        this.performerService.data.subscribe(
            performers => {
                this.performers = performers
            },
            error => this.errorMessage = <any>error
        );
        this.subscription = this.performerService.queryParamSubject.subscribe(
            params => {
                this.performerService.gotoPage(params);
            },
            error => this.errorMessage = <any>error
        );

        this.performerService.getCount().subscribe(
            count => { this.count = count; },
            error => this.errorMessage = <any>error
        );
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    transistPage(page) {
        this.page = page;
        this.performerService.setPage(page);
    }
    getRawData() {
        console.log('raw', this.performerService.getRawData());
    }
    find() {
        this.performerService.find(3).subscribe(
            event => { console.log(event.Code) }
        );
    }
    toggleSortTitle(sort) {
        this.performerService.setOrder(sort);
    }
    changePageSize(pageSize) {
        this.performerService.setPageSize(pageSize);
        this.pageSize = pageSize;
    }
    callSelectedItemsAction(action: string) {
        console.log(action);
    }
    selectAllItems(selectAll: boolean): void {
        if (selectAll && this.selectedItems.length < this.performers.length) {
            this.selectedItems = [];
            this.performers.forEach(item => {
                this.selectedItems.push(item);
            });
            this.isAllSelected = true;
        }
        if (!selectAll) {
            this.isAllSelected = false;
            this.selectedItems = [];
        }
    }
    selectItem(isSelected: boolean, event: Event): void {
        if (isSelected) {
            this.selectedItems.push(event);
        } else {
            let selectedPerformer = this.selectedItems.filter(item => {
                return (event === item);
            })[0];
            this.selectedItems.splice(this.selectedItems.indexOf(selectedPerformer), 1);
        }
    }
    get isMultiSelectionActive(): boolean {
        return this.selectedItems.length > 0;
    }

}
