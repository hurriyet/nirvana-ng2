import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthDialogBoxComponent } from '../../../components/auth-dialog-box/auth-dialog-box.component';

@Component({
  selector: 'app-lock',
  templateUrl: './lock.component.html',
  styleUrls: ['./lock.component.scss']
})
export class LockComponent implements OnInit {
  @ViewChild(AuthDialogBoxComponent) authDialogBox: AuthDialogBoxComponent;

  private authAlert: {type: string, title: string, description: string}
  private isAuthenticatedUser: boolean;

  constructor() { }

  ngOnInit() {
  }

  authSubmitEventHandler($event) {
    if($event.username == "demo" && $event.password == "demo") {
      this.isAuthenticatedUser = true;
      console.log("Kullanıcı girişi yapıldı. Sayfa yönlendirmesi yapılabilir");
    }else{
      this.authDialogBox.reset();
      this.authAlert = {
        type: "warning",
        title: "Kullanıcı adı veya parolanız hatalı",
        description: "Yeniden deneyin ya da sifremi unuttum linkine tıklayarak parolanızı sıfırlaryın."
      }
    }
  }

}
