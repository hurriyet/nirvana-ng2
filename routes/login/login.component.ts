import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthDialogBoxComponent } from '../../components/auth-dialog-box/auth-dialog-box.component';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/authentication.service';
import { AppSettingsService } from '../../services/app-settings.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild(AuthDialogBoxComponent) authDialogBox: AuthDialogBoxComponent;

  private authAlert: {type: string, title: string, description: string}
  private isAuthenticatedUser: boolean;
  private advanceMessage: string;
  private appSettings : AppSettingsService;
  user: User;

  constructor(
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private appSettingsService:AppSettingsService) {
  	this.appSettings = appSettingsService;

    }

  ngOnInit() {
    if(this.route.snapshot.data['action'] == 'logout'){
      this.authService.logout();
    }
  }

  authSubmitEventHandler($event) {
    this.authService.login($event.username, $event.password).subscribe(
      response => {
      	this.appSettings.getClientSettings().subscribe(items => {
			  this.appSettings.setClientSettings(items);
		});
        this.authService.loginComplete();
      },
      error => {
        this.authDialogBox.reset();
        this.authAlert = {
        type: "warning",
        title: "Kullanıcı adı veya parolanız hatalı",
        description: "Yeniden deneyin ya da şifremi unuttum linkine tıklayarak parolanızı sıfırlaryın."
       }
      }
    )
    // this.authService.login($event.username, $event.password).then( result => {
    //   //this.isAuthenticatedUser = true;
    //   //this.advanceMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam porta ex quis lacinia vehicula. In accumsan molestie fringilla.";
    //   //this.authService.setUser(this.user);
    //   console.log(this.authService.authenticatedUser);
    //   if(this.authService.authenticatedUser) {
    //     this.authService.redirect();
    //   }
    // }).catch( reason => {
    //   this.authDialogBox.reset();
    //   this.authAlert = {
    //     type: "warning",
    //     title: "Kullanıcı adı veya parolanız hatalı",
    //     description: "Yeniden deneyin ya da sifremi unuttum linkine tıklayarak parolanızı sıfırlaryın."
    //   }
    // });
  }

  advanceAuthSubmitEventHandler($event) {
    this.authService.loginComplete();
  }

}
