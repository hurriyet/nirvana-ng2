import { Component, OnInit, ElementRef, ViewChild, Renderer, HostBinding } from '@angular/core';
import { HeaderTitleService } from '../../services/header-title.service';
import { TetherDialog } from '../../components/tether-dialog/tether-dialog';
import { Event } from '../../models/event';
import { EventService } from '../../services/event.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    selector: 'app-events',
    templateUrl: './events.component.html',
    styleUrls: ['./events.component.scss'],
    providers: [EventService]
})
export class EventsComponent implements OnInit {
    @HostBinding('class.or-events') true;
    constructor(
        private renderer: Renderer,
        private headerTitleService: HeaderTitleService,
        private eventService: EventService,
        private router: Router,
        private route: ActivatedRoute
    ) {

    }
    pills: Array<any> = [{ text: 'TASLAK', filter: '(Status eq 4 or Status eq 10)', isActive: false, type: 'and' }, { text: 'SATIŞTA', filter: 'Status eq 2', isActive: false, type: 'and' }, { text: 'SATIŞTA DEĞİL', filter: '(Status eq 3 or Status eq 5 or Status eq 6)', isActive: false, type: 'and' }, { text: 'GEÇMİŞ', filter: 'Status eq 1', isActive: false, type: 'and' }];
    tabs: Array<any> = [{ label: 'TÜMÜ', routerLink: '/events/index' }, { label: 'TEK PERFORMANSLI ETKİNLİKLER', routerLink: '/events/single-performance' }, { label: 'ÇOK PERFORMANSLI ETKİNLİKLER', routerLink: '/events/multiple-performance' }, { label: 'ÇATI ETKİNLİKLERİ', routerLink: '/events/master' }];
    sortParams: Array<any> = [{text:'SEÇİNİZ',value: ''}, { text: 'ADA GÖRE[Z-A]', value: JSON.stringify({ sortBy: "Name", type: "desc" }) },{ text: 'ADA GÖRE[A-Z]', value: JSON.stringify({ sortBy: "Name", type: "asc" }) }, { text: 'TARİHE GÖRE[Önce Eski]', value: JSON.stringify({ sortBy: "BeginDate", type: "asc" }) }, { text: 'TARİHE GÖRE[Önce Yeni]', value: JSON.stringify({ sortBy: "BeginDate", type: "desc" }) },
    { text: 'SATIŞ TARİHİNE GÖRE[Önce Eski]', value: JSON.stringify({ sortBy: "SalesBeginDate", type: "asc" }) }, { text: 'SATIŞ TARİHİNE GÖRE[Önce Yeni]', value: JSON.stringify({ sortBy: "SalesBeginDate", type: "desc" }) }
    ];
    events: Event[];
    errorMessage: any;
    filter: Array<Object> = [];
    pageSize: number = 10;
    ngOnInit(): void {
        this.headerTitleService.setTitle('Etkinlikler');
        this.eventService.setCustomEndpoint('GetEventList');
    }
    onResize(event) {

    }
    onInputChange(event) {
        this.eventService.setSearch({ key: 'Name', value: event });
    }
    ngAfterViewInit() {
    }
    sortEvents(sort) {
    	if(sort){
        	this.eventService.setOrder(sort, true);
    	}else{
    		this.eventService.flushOrder();
    	}
    }
    filterEvents(pill) {
        this.eventService.setFilter(pill);
    }
    changeView(viewType) {
        
        this.eventService.setActiveViewType(viewType);
    }



}
