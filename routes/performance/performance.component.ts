import { Observable } from 'rxjs/Observable';
import { TitleSwitcherComponent } from './../../components/title-switcher/title-switcher.component';
import { Venue } from './../../models/venue';
import { Template } from './../../models/template';
import { Performance } from './../../models/performance';
import { PerformanceStatus } from './../../models/performance-status.enum';
import { HeaderTitleService } from './../../services/header-title.service';
import { PerformanceService } from '../../services/performance.service';
import { TemplateService } from '../../services/template.service';
import { VenueService } from '../../services/venue.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, HostBinding, ViewChild } from '@angular/core';

@Component({
    selector: 'app-performance',
    templateUrl: './performance.component.html',
    styleUrls: ['./performance.component.scss'],
    providers: [PerformanceService, TemplateService, VenueService]
})
export class PerformanceComponent implements OnInit {
    @ViewChild(TitleSwitcherComponent) titleSwitcher: TitleSwitcherComponent;
    @HostBinding('class.or-performance') true;
    pageID: number;
    tabs: Array<any> = [
        { label: 'SANATÇILAR', routerLink: ['/performance', this.pageID, 'performers'] },
        { label: 'ÜRÜNLER', routerLink: ['/performance', this.pageID, 'products'] },
        { label: 'REZERVASYON', routerLink: ['/performance', this.pageID, 'reservations'] },
        { label: 'DAVETİYE', routerLink: ['/performance', this.pageID, 'invitations'] },
        { label: 'SATIŞLAR', routerLink: ['/performance', this.pageID, 'cancel-block'] },
    ];

    performance: Performance;
    template: Template;
    venue: Venue;
    joinedPerformance;

    subscription;

    constructor(
        private router: Router,
        private performanceService: PerformanceService,
        private route: ActivatedRoute,
        private templateService: TemplateService,
        private venueService: VenueService,
        private headerTitleService: HeaderTitleService
    ) { }

    ngOnInit() {
        this.headerTitleService.setTitle('Performanslar');
        this.subscription = this.route.params.subscribe(params => {
            let param = +params['id'];
            this.pageID = param;
            this.tabs.forEach(item => {
                item.routerLink[1] = param;
            });
            this.performance = null;
            this.performanceService.flushCustomEndpoint();
            this.performanceService.find(this.pageID, true);
        })

        this.performanceService.data.subscribe(performanceData => {
            if(performanceData) {
                if(!this.performance) {
                    if(performanceData[0]) this.performance = new Performance(performanceData[0]);
                    if(this.performance) { // to get capacity
                        this.templateService.find(this.performance.VenueTemplateId, true);
                        this.performanceService.setCustomEndpoint('GetPerformance');
                        this.performanceService.query({ pageSize: 1 }, [{key: "performanceId", value: this.performance.Id}], true);

                        if(this.performance.Status == PerformanceStatus.OnSale) this.tabs.push({ label: 'GRUP SATIŞ', routerLink: ['/performance', this.pageID, 'group-sale'] });
                    }
                }else {
                    if(!this.joinedPerformance && performanceData[0]["Name"]) {
                        this.joinedPerformance = performanceData[0];
                    }
                    if(this.titleSwitcher && this.titleSwitcher.finder) {
                        let result: {}[] = [];
                        performanceData.forEach( performanceItem => {
                            result.push({
                                id: performanceItem.Id,
                                title: performanceItem.PerformanceName,
                                icon: "audiotrack",
                                params: {performance: performanceItem}
                            })
                        });

                        this.titleSwitcher.finderSearchResults = Observable.of([{
                            title: "ARAMA SONUÇLARI",
                            list: result
                        }]);
                    }
                }
            } else {
                if(this.titleSwitcher && this.titleSwitcher.finder) this.titleSwitcher.finderSearchResults = Observable.of([]);
            }
        });

        this.templateService.data.subscribe( templateData => {
            if(templateData && templateData.length > 0) {
                if(!this.template) {
                    this.template = new Template(templateData[0]);
                    if(this.template) this.venueService.find(this.template.VenueId, true);
                }
            }
        });

        this.venueService.data.subscribe( venueData => {
            if(venueData && venueData.length > 0) {
                if(!this.venue) {
                    this.venue = new Venue(venueData[0]);
                }
            }
        });
    }

    titleSearchHandler(value) {
        this.performanceService.setCustomEndpoint('GetPerformanceList');
        if(value && value.length > 0) this.performanceService.query({ page: 0, pageSize: 10, search: {key:'PerformanceName', value:value}});
    }

    titleChangeHandler(event) {
        this.router.navigate(["performance", event.id]);
    }
}
