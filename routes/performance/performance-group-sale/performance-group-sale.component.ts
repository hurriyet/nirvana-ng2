import { TetherDialog } from './../../../components/tether-dialog/tether-dialog';
import { GroupSaleContactComponent } from './../../../common/group-sale-contact/group-sale-contact.component';
import { Performance } from './../../../models/performance';
import { NotificationService } from './../../../services/notification.service';
import { VenueSeatService } from './../../../services/venue-seat.service';
import { VenueTemplateEditorComponent } from './../../../components/venue-template-editor/venue-template-editor.component';
import { VenueSeat } from './../../../models/venue-seat';
import { SeatStatus } from './../../../models/seat-status.enum';
import { Product } from './../../../models/product';
import { EntityService } from './../../../services/entity.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Inject, ChangeDetectorRef, ViewChild, ComponentRef, ComponentFactoryResolver, Injector } from '@angular/core';

@Component({
  selector: 'app-performance-group-sale',
  templateUrl: './performance-group-sale.component.html',
  styleUrls: ['./performance-group-sale.component.scss'],
  providers: [
    {provide: 'performanceEntityService', useClass: EntityService },
    {provide: 'totalCapacityCountService', useClass: EntityService },
    {provide: 'onSaleCountService', useClass: EntityService },
    {provide: 'soldCountService', useClass: EntityService },
    {provide: 'blockedCountService', useClass: EntityService },
    {provide: 'canceledCountService', useClass: EntityService },

    VenueSeatService
  ],
  entryComponents: [GroupSaleContactComponent]
})
export class PerformanceGroupSaleComponent implements OnInit {

  @ViewChild(VenueTemplateEditorComponent) venueEditor: VenueTemplateEditorComponent;

  private performance: Performance;
  private venueSeats: VenueSeat[];
  private contactInfo: {};

  private selectedSeats: {Status: number}[];
  private get selectedPendingSeats(): {Status: number}[] { 
    return !this.selectedSeats ? null : this.selectedSeats.filter( item => item.Status == SeatStatus.Pending);
  }
  private get selectedSoldSeats(): {Status: number}[] { 
    return !this.selectedSeats ? null : this.selectedSeats.filter( item => item.Status == SeatStatus.Sold);
  }
  private get selectedOnSaleSeats(): {Status: number}[] { 
    return !this.selectedSeats ? null : this.selectedSeats.filter( item => item.Status == SeatStatus.OnSale || item.Status == SeatStatus.Hold);
  }
  private get selectedKilledSeats(): {Status: number}[] { 
    return !this.selectedSeats ? null : this.selectedSeats.filter( item => item.Status == SeatStatus.Killed);
  }

  private statistics: {}[];
  private infoStatistics: {}[];
  private statusStatistics: {}[];
  private totalCapacityCount: number;
  private onSaleCount: number;
  private soldCount: number;
  private blockedCount: number;
  private canceledCount: number;

  private routeSubscription: any;
  private isLoading: boolean = false;

  constructor(
    @Inject('performanceEntityService') private performanceEntityService: EntityService,

    @Inject('totalCapacityCountService') private totalCapacityCountService: EntityService,
    @Inject('onSaleCountService') private onSaleCountService: EntityService,
    @Inject('soldCountService') private soldCountService: EntityService,
    @Inject('blockedCountService') private blockedCountService: EntityService,
    @Inject('canceledCountService') private canceledCountService: EntityService,

    private venueSeatService: VenueSeatService,
    private notificationService: NotificationService,

    private route: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
    private resolver: ComponentFactoryResolver,
    private injector: Injector,
    private tetherService: TetherDialog
  ) { }

  ngOnInit() {
    this.routeSubscription = this.route.parent.params.subscribe( params =>  {
      if(!params) return;
      this.isLoading = true;
      this.performanceEntityService.data.subscribe( entities => {
        if(entities[0]) {
          this.performance = entities[0];

          this.resetStatistics();

        }
      });

      this.statisticsDataHandler();

      this.performanceEntityService.setCustomEndpoint('GetAll');
      this.performanceEntityService.fromEntity('EPerformance')
        .where('Id', '=', params["id"])
        .take(1).page(0)
        .executeQuery();
    });
  }

  ngAfterViewInit(){
    
  }

  ngOnDestroy() {
    this.routeSubscription = null;
  }

  private resetStatistics(){
    this.totalCapacityCountService.setCustomEndpoint('GetAll');
    this.totalCapacityCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).take(1).page(0).executeQuery();

    this.onSaleCountService.setCustomEndpoint('GetAll');
    this.onSaleCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.OnSale+"', Nirvana.Shared.Enums.SeatStatus)").take(1).page(0).executeQuery();

    this.soldCountService.setCustomEndpoint('GetAll');
    this.soldCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Sold+"', Nirvana.Shared.Enums.SeatStatus)").take(1).page(0).executeQuery();

    this.blockedCountService.setCustomEndpoint('GetAll');
    this.blockedCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Pending+"', Nirvana.Shared.Enums.SeatStatus)").take(1).page(0).executeQuery();

    this.canceledCountService.setCustomEndpoint('GetAll');
    this.canceledCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Killed+"', Nirvana.Shared.Enums.SeatStatus)").take(1).page(0).executeQuery();

  }

  private statisticsDataHandler() {
    this.totalCapacityCountService.count.subscribe( count => this.setStatistics('totalCapacityCount', count) );
    this.onSaleCountService.count.subscribe( count => this.setStatistics('onSaleCount', count) );
    this.soldCountService.count.subscribe( count => this.setStatistics('soldCount', count) );
    this.blockedCountService.count.subscribe( count => this.setStatistics('blockedCount', count) );
    this.canceledCountService.count.subscribe( count => this.setStatistics('canceledCount', count) );
  }

  private setStatistics(prop, value) {
    this[prop] = value;
    this.infoStatistics = [];
    if(this.totalCapacityCount != null) this.infoStatistics.push({key: 'totalCapacityCount', label: 'TOPLAM KAPASİTE', value: this.totalCapacityCount});
    if(this.onSaleCount != null) this.infoStatistics.push({key: 'onSaleCount', label: 'SATIŞA AÇIK', value: this.onSaleCount});
    this.statusStatistics = [];
    if(this.soldCount != null) this.statusStatistics.push({color: '61B100', key: 'soldCount', label: 'SATILAN', value: this.soldCount});
    if(this.blockedCount != null) this.statusStatistics.push({color: 'FCC200', key: 'blockedCount', label: 'BLOKE', value: this.blockedCount});
    if(this.canceledCount != null) this.statusStatistics.push({color: 'E03336', key: 'canceledCount', label: 'İPTAL', value: this.canceledCount});
    this.statistics = [this.infoStatistics, this.statusStatistics];
    this.changeDetector.detectChanges();
  }

  private venueEditorEventHandler(event) {
    switch(event.type) {
      case VenueTemplateEditorComponent.EVENT_READY:
        this.isLoading = false;
      break;
      case VenueTemplateEditorComponent.EVENT_SELECT:
        this.selectedSeats = event.payload.filter( item => item["Status"] != null);
      break;
    }
    this.changeDetector.detectChanges();
  }

  private actionBoxActionHandler(event) {
    if(!event.params.target) return;
    switch(event.action) {
      case "makeGroupSale":
        let venueSeat: VenueSeat;
        this.venueSeats = [];
        event.params.target.forEach( seat => {
          venueSeat = new VenueSeat();
          venueSeat.Id = seat["Id"];
          venueSeat.VenueGateId = seat["GateId"];
          venueSeat.IsStanding = seat["IsStanding"];
          venueSeat.VenueRowId = seat["RowId"];
          venueSeat.SeatPriority = seat["SeatPriority"];
          venueSeat.Status = seat["Status"];
          venueSeat.TicketType = seat["TicketType"];
          this.venueSeats.push(venueSeat);
        });
        venueSeat = null;
        this.openGroupSaleContact();
    }
  }

  private openGroupSaleContact(){
    let component:ComponentRef<GroupSaleContactComponent> = this.resolver.resolveComponentFactory(GroupSaleContactComponent).create(this.injector);

    this.tetherService.modal(component, {
			escapeKeyIsActive: true,
			dialog: {
				style: { maxWidth: "600px", width: "80vw", height: "auto" }
			},
		}).then(result => {
      this.contactInfo = result["contact"];
      this.saveGroupSale();
    }).catch( reason => {});
    window.dispatchEvent(new Event("resize"));
  }

  private saveGroupSale() {
    if(!this.venueSeats || !this.contactInfo) return;
    this.isLoading = true;
    console.log("KOLTUKLAR");
    console.log("=========");
    this.venueSeats.forEach( item => console.log("Id ", item.Id, "No: ", item.SeatNo));
    console.log("İLETİŞİM BİLGİLERİ");
    console.log("==================");
    console.log(JSON.stringify(this.contactInfo));
    let self = this;
    setTimeout(function() {
      self.isLoading = false;
      self.notificationService.add({type: "success", text: "Grup satış işlemi başarıyla gerçekleştirildi"});
    }, 2000);
    // this.venueSeatService.setCustomEndpoint('PatchAll');
    // this.venueSeatService.update(venueSeats).subscribe( result => {
    //   this.isLoading = false;
    //   this.resetStatistics();
    //   this.notificationService.add({type: "success", text: "Koltuklar başarıyla güncellendi"});
    //   //if(this.venueEditor) this.venueEditor.update(venueSeats);
    // });
  }

}
