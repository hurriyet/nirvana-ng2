import { Component, OnInit, HostBinding, Inject, Injector, ComponentRef, ComponentFactoryResolver } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EntityService } from '../../../services/entity.service';
import { ReservationService } from '../../../services/reservation.service';
import { NotificationService } from '../../../services/notification.service';
import { AppSettingsService } from '../../../services/app-settings.service';
import { ReservationStatus } from '../../../models/reservation-status.enum';
import { ContextMenuComponent } from '../../../components/context-menu/context-menu.component';
import { TetherDialog } from '../../../components/tether-dialog/tether-dialog';


@Component({
	selector: 'app-performance-reservations',
	templateUrl: './performance-reservations.component.html',
	styleUrls: ['./performance-reservations.component.scss'],
	entryComponents: [ContextMenuComponent],
	providers:[
		EntityService, ReservationService, AppSettingsService,
		{provide: 'entityServiceInstance1', useClass: EntityService },
		{provide: 'entityServiceInstance2', useClass: EntityService },
		{provide: 'entityServiceInstance3', useClass: EntityService },
		{provide: 'entityServiceInstance4', useClass: EntityService },
		{provide: 'entityServiceInstance5', useClass: EntityService },
	]
})
export class PerformanceReservationsComponent implements OnInit {
	subscription;
	errorMessage: any;
	reservationStatus = ReservationStatus;

	totalCapacity: number = 0;
	openCapacity: number = 0;

	openReservationCount: number = 0;
	CancelledReservationCount: number = 0;
	ExpiredReservationCount: number = 0;

	reservations = [];
	count: number;
	selectedItems: Array<Object> = [];
	isAllSelected: boolean = false;

	showPagination: boolean = true;
	pageSizes: Array<Object> = [{ text: '10', value: 10 }, { text: '20', value: 20 }];
	pageSize: number = 10;
	page: number = 1;
	private currentPage : number;

	private isLoading: boolean = false;
	private noDataInContent: boolean = true;
	private query;

	constructor(
		private resolver: ComponentFactoryResolver,
		private injector: Injector,
		private tetherService: TetherDialog,
		private route: ActivatedRoute,
		private router: Router,
		private notificationService: NotificationService,
		private appSettings: AppSettingsService,
		private entityService: EntityService,
		private reservationService: ReservationService,
		@Inject('entityServiceInstance1') private capacityService: EntityService,
		@Inject('entityServiceInstance2') private OpenCapacityService: EntityService,
		@Inject('entityServiceInstance3') private OpenReservationService: EntityService,
		@Inject('entityServiceInstance4') private CancelledReservationService: EntityService,
		@Inject('entityServiceInstance5') private ExpiredReservationService: EntityService,
		// @Inject('instance2') private _pearEditorService: EntityService
	) {
		this.changePageSize(this.pageSize);
	}

	ngOnInit() {

		this.subscription = this.route.parent.params.subscribe(params => {
			this.isLoading = true;
			this.entityService.setCustomEndpoint('GetAll');
			this.query = this.entityService.fromEntity('BReservation')
			.where('PerformanceId', '=', params['id']);
			// .expand(['Product']);
			// .expand(['Product', 'PriceLists','Variants'])
			// .expand(['Product', 'Currency'])
			// .expand(['Performance'])

			this.capacityService.setCustomEndpoint('GetAll');
			this.capacityService.fromEntity('EVenueSeat')
			.where('VenueRow/PerformanceId', '=', params['id'])
			.take(1).page(0)
			.executeQuery();

			this.OpenCapacityService.setCustomEndpoint('GetAll');
			this.OpenCapacityService.fromEntity('EVenueSeat')
			.where('VenueRow/PerformanceId', '=', params['id'])
			.and('Status', '=', "cast('1', Nirvana.Shared.Enums.SeatStatus)")
			.take(1).page(0)
			.executeQuery();

			this.OpenReservationService.setCustomEndpoint('GetAll');
			this.OpenReservationService.fromEntity('BReservation')
			.where('PerformanceId', '=', params['id'])
			.and('Status', '=', `cast('${this.reservationStatus['Open']}', Nirvana.Shared.Enums.ReservationStatus)`)
			.take(1).page(0)
			.executeQuery();

			this.CancelledReservationService.setCustomEndpoint('GetAll');
			this.CancelledReservationService.fromEntity('BReservation')
			.where('PerformanceId', '=', params['id'])
			.and('Status', '=', `cast('${this.reservationStatus['Cancelled']}', Nirvana.Shared.Enums.ReservationStatus)`)
			.take(1).page(0)
			.executeQuery();

			this.ExpiredReservationService.setCustomEndpoint('GetAll');
			this.ExpiredReservationService.fromEntity('BReservation')
			.where('PerformanceId', '=', params['id'])
			.and('Status', '=', `cast('${this.reservationStatus['Expired']}', Nirvana.Shared.Enums.ReservationStatus)`)
			.take(1).page(0)
			.executeQuery();
		});

		this.subscription = this.entityService.queryParamSubject.subscribe(
			params => {
				this.isLoading = true;
				let sort = params["sort"] ? (typeof params["sort"] == 'string'  ? JSON.parse(params["sort"]) : params["sort"]) : null;
				let query = this.query
				.take(params['pageSize'])
				.page(params['page']);

				if(sort && sort[0]){
					query.orderBy(sort[0]["sortBy"],sort[0]["type"])
				}
				if(params["search"]){
					query.search(params["search"]["key"], params["search"]["value"]);
				}
				query.executeQuery();
			},
			error => this.errorMessage = <any>error
		);

		this.entityService.data.subscribe(entities => {
			// this.reservations = [
			// 	{Id: 1, UserId: 4, Status: 2, Quantity: 6, ExpirationDate: new Date("2017-3-31")},
			// 	{Id: 2, UserId: 2, Status: 1, Quantity: 5, ExpirationDate: new Date("2017-4-1")},
			// 	{Id: 3, UserId: 1, Status: 3, Quantity: 7, ExpirationDate: new Date("2017-4-10")},
			// 	{Id: 4, UserId: 7, Status: 4, Quantity: 2, ExpirationDate: new Date("2017-4-10")},
			// ];
			this.reservations = entities;
			this.isLoading = false;
			if(this.reservations.length == 0) {
				this.noDataInContent = true;
			} else {
				this.noDataInContent = false;
			}
		});

		this.entityService.getCount().subscribe(
			count => { this.count = count; },
			error => this.errorMessage = <any>error
		);

		this.capacityService.getCount().subscribe(
			count => { this.totalCapacity = count; },
			error => this.errorMessage = <any>error
		);

		this.OpenCapacityService.getCount().subscribe(
			count => { this.openCapacity = count; },
			error => this.errorMessage = <any>error
		);

		this.OpenReservationService.getCount().subscribe(
			count => { this.openReservationCount = count; },
			error => this.errorMessage = <any>error
		);

		this.CancelledReservationService.getCount().subscribe(
			count => { this.CancelledReservationCount = count; },
			error => this.errorMessage = <any>error
		);

		this.ExpiredReservationService.getCount().subscribe(
			count => { this.ExpiredReservationCount = count; },
			error => this.errorMessage = <any>error
		);
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	selectAllItems(selectAll: boolean): void {
		if (selectAll && this.selectedItems.length < this.reservations.length) {
			this.selectedItems = [];
			this.reservations.forEach(item => {
				this.selectedItems.push(item);
			});
			this.isAllSelected = true;
		}
		if (!selectAll) {
			this.isAllSelected = false;
			this.selectedItems = [];
		}
	}

	selectItem(isSelected: boolean, reservation: Event): void {
		if (isSelected) {
			this.selectedItems.push(reservation);
		} else {
			let selectedEvent = this.selectedItems.filter(item => {
				return (reservation === item);
			})[0];
			this.selectedItems.splice(this.selectedItems.indexOf(selectedEvent), 1);
		}
	}

	get isMultiSelectionActive(): boolean {
		return this.selectedItems.length > 0;
	}

	onInputChange(value) {
		this.entityService.setSearch({ key: 'UserId', value: value });
	}

	toggleSortTitle(sort) {
		if(sort){
			this.entityService.setOrder(sort, true);
		} else {
			this.entityService.flushOrder();
		}
	}

	changePageSize(pageSize) {
		this.entityService.setPageSize(pageSize);
		this.pageSize = pageSize;
	}

	transistPage(page) {
		this.currentPage = page;
		this.entityService.setPage(page);
	}

	openEventsContextMenu(e, reservation) {
		let component: ComponentRef<ContextMenuComponent> = this.resolver.resolveComponentFactory(ContextMenuComponent).create(this.injector);
		let instance: ContextMenuComponent = component.instance;

		instance.title = "REZERVASYON İŞLEMLERİ";
		instance.data = []

		this.appSettings.getLocalSettings('reservationDelayTimes').forEach(item=> {
			instance.data.push({
				action: 'delay',
				icon: 'timelapse',
				label: item.label,
				delayTime: item.time,
			})
		});
		instance.data.push({ label: 'İptal Et', icon: 'cancel', action: 'cancel', type: 'aside'})

		this.tetherService.context(component,
			{
				target: event.target,
				attachment: "top right",
				targetAttachment: "top right",
			}
		).then(result => {
			if (result) {
				switch (result['action']) {
					case "cancel":
						this.isLoading = true;
						this.reservationService.flushCustomEndpoint();
						this.reservationService.update(
							{
								Id: reservation.Id,
								Status: this.reservationStatus['Cancelled']
							}
						).subscribe(
							response => {
								this.notificationService.add({text: "Rezervasyon İptal edildi.", type:'success'});
								this.entityService.reload();
							},
							error => {
								this.notificationService.add({text: error['Message'], type:'danger'});
							},
							complete => {
								this.isLoading = false;
							}
						);
						break;
					case "delay":
						if(reservation.Status == this.reservationStatus['Cancelled']) {
							this.notificationService.add({text: "İptal edilmiş rezervasyonun tarihi ertelenemez.", type:'danger'});
							return;
						}

						this.isLoading = true;
						let time = new Date(reservation.ExpirationDate);
						time.setTime(time.getTime() + result['delayTime']*60*60*1000)

						this.reservationService.flushCustomEndpoint();
						this.reservationService.update(
							{
								Id: reservation.Id,
								ExpirationDate: time.toJSON()
							}
						).subscribe(
							response => {
								this.notificationService.add({text: "Rezervasyon Ertelendi.", type:'success'});
								 this.entityService.reload();
							},
							error => {
								this.notificationService.add({text: error['Message'], type:'danger'});
							},
							complete => {
								this.isLoading = false;
							}
						);
						break;
				}
			}
		}).catch(reason => {
			console.log("dismiss reason : ", reason);
		});
	}
}
