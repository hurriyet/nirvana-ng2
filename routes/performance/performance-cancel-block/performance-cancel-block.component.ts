import { Performance } from './../../../models/performance';
import { NotificationService } from './../../../services/notification.service';
import { VenueSeatService } from './../../../services/venue-seat.service';
import { VenueTemplateEditorComponent } from './../../../components/venue-template-editor/venue-template-editor.component';
import { VenueSeat } from './../../../models/venue-seat';
import { SeatStatus } from './../../../models/seat-status.enum';
import { TicketType } from './../../../models/ticket-type.enum';
import { PerformanceStatus } from './../../../models/performance-status.enum';
import { Product } from './../../../models/product';
import { EntityService } from './../../../services/entity.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Inject, ChangeDetectorRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-performance-cancel-block',
  templateUrl: './performance-cancel-block.component.html',
  styleUrls: ['./performance-cancel-block.component.scss'],
  providers: [
    {provide: 'performanceEntityService', useClass: EntityService },

    {provide: 'totalCapacityCountService', useClass: EntityService },
    {provide: 'onSaleCountService', useClass: EntityService },
    {provide: 'pendingCountService', useClass: EntityService },
    {provide: 'soldCountService', useClass: EntityService },
    {provide: 'blockedCountService', useClass: EntityService },
    {provide: 'canceledCountService', useClass: EntityService },

    {provide: 'blockedSeatsService', useClass: EntityService },
    {provide: 'availableSeatsService', useClass: EntityService },
    {provide: 'canceledSeatsService', useClass: EntityService },

    {provide: 'soldIndividualCountService', useClass: EntityService },
    {provide: 'soldSeasonalCountService', useClass: EntityService },
    {provide: 'soldCompCountService', useClass: EntityService },
    {provide: 'soldGroupCountService', useClass: EntityService },

    {provide: 'reservedIndividualCountService', useClass: EntityService },
    {provide: 'reservedCompCountService', useClass: EntityService },

    VenueSeatService
  ]
})
export class PerformanceCancelBlockComponent implements OnInit {
  @ViewChild(VenueTemplateEditorComponent) venueEditor: VenueTemplateEditorComponent;

  private performance: Performance;

  private selectedSeats: {Status: number}[];
  private get selectedHoldSeats(): {Status: number}[] { 
    return !this.selectedSeats ? null : this.selectedSeats.filter( item => item.Status == SeatStatus.Hold);
  }
  private get selectedSoldSeats(): {Status: number}[] { 
    return !this.selectedSeats ? null : this.selectedSeats.filter( item => item.Status == SeatStatus.Sold);
  }
  private get selectedAvailableSeats(): {Status: number}[] { 
    return !this.selectedSeats ? null : this.selectedSeats.filter( item => item.Status == SeatStatus.OnSale || item.Status == SeatStatus.Pending);
  }
  private get selectedKilledSeats(): {Status: number}[] { 
    return !this.selectedSeats ? null : this.selectedSeats.filter( item => item.Status == SeatStatus.Killed);
  }

  private statistics: {}[];
  private infoStatistics: {}[];
  private statusStatistics: {}[];
  
  private totalCapacityCount: number;
  private onSaleCount: number;

  private pendingCount: number;
  private soldCount: number;
  private blockedCount: number;
  private canceledCount: number;

  private soldIndividualCount: number;
  private soldSeasonalCount: number;
  private soldCompCount: number;
  private soldGroupCount: number;

  private reservedIndividualCount: number;
  private reservedCompCount: number;

  private routeSubscription: any;
  private isLoading: boolean = false;
  private isStandingLayout: boolean = false;
  private currentAction: string;
  private currentValue: number;

  private editorRole: string = VenueTemplateEditorComponent.ROLE_CANCEL_BLOCK;

  constructor(
    @Inject('performanceEntityService') private performanceEntityService: EntityService,

    @Inject('totalCapacityCountService') private totalCapacityCountService: EntityService,
    @Inject('onSaleCountService') private onSaleCountService: EntityService,
    @Inject('pendingCountService') private pendingCountService: EntityService,
    @Inject('soldCountService') private soldCountService: EntityService,
    @Inject('blockedCountService') private blockedCountService: EntityService,
    @Inject('canceledCountService') private canceledCountService: EntityService,

    @Inject('blockedSeatsService') private blockedSeatsService: EntityService,
    @Inject('availableSeatsService') private availableSeatsService: EntityService,
    @Inject('canceledSeatsService') private canceledSeatsService: EntityService,

    @Inject('soldIndividualCountService') private soldIndividualCountService: EntityService,
    @Inject('soldSeasonalCountService') private soldSeasonalCountService: EntityService,
    @Inject('soldCompCountService') private soldCompCountService: EntityService,
    @Inject('soldGroupCountService') private soldGroupCountService: EntityService,

    @Inject('reservedIndividualCountService') private reservedIndividualCountService: EntityService,
    @Inject('reservedCompCountService') private reservedCompCountService: EntityService,

    private venueSeatService: VenueSeatService,
    private notificationService: NotificationService,

    private route: ActivatedRoute,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.routeSubscription = this.route.parent.params.subscribe( params =>  {
      if(!params) return;
      this.isLoading = true;
      this.performanceEntityService.data.subscribe( entities => {
        if(entities[0]) {
          this.performance = entities[0];

          this.resetStatistics();

          this.isStandingLayout = this.performance.VenueTemplate.IsStanding;
          if(this.isStandingLayout) {
            this.isLoading = false;
            this.availableSeatsServicesDataHandler();
          }
        }
      });

      this.statisticsDataHandler();

      this.performanceEntityService.setCustomEndpoint('GetAll');
      this.performanceEntityService.fromEntity('EPerformance')
        .where('Id', '=', params["id"])
        .expand(['VenueTemplate'])
        .take(1).page(0)
        .executeQuery();
    });
  }

  ngAfterViewInit(){
    
  }

  ngOnDestroy() {
    this.routeSubscription = null;
  }

  private resetStatistics(){
    this.totalCapacityCountService.setCustomEndpoint('GetAll');
    this.totalCapacityCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).take(1).page(0).executeQuery();

    this.onSaleCountService.setCustomEndpoint('GetAll');
    this.onSaleCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.OnSale+"', Nirvana.Shared.Enums.SeatStatus)").take(1).page(0).executeQuery();

    this.pendingCountService.setCustomEndpoint('GetAll');
    this.pendingCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Pending+"', Nirvana.Shared.Enums.SeatStatus)").take(1).page(0).executeQuery();

    this.soldCountService.setCustomEndpoint('GetAll');
    this.soldCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Sold+"', Nirvana.Shared.Enums.SeatStatus)").take(1).page(0).executeQuery();

    this.blockedCountService.setCustomEndpoint('GetAll');
    this.blockedCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Hold+"', Nirvana.Shared.Enums.SeatStatus)").take(1).page(0).executeQuery();

    this.canceledCountService.setCustomEndpoint('GetAll');
    this.canceledCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Killed+"', Nirvana.Shared.Enums.SeatStatus)").take(1).page(0).executeQuery();

    this.soldIndividualCountService.setCustomEndpoint('GetAll');
    this.soldIndividualCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Sold+"', Nirvana.Shared.Enums.SeatStatus)").and('TicketType', '=', "cast('"+TicketType.Sale+"', Nirvana.Shared.Enums.TicketType)").take(1).page(0).executeQuery();

    this.soldSeasonalCountService.setCustomEndpoint('GetAll');
    this.soldSeasonalCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Sold+"', Nirvana.Shared.Enums.SeatStatus)").and('TicketType', '=', "cast('"+TicketType.Seosanal+"', Nirvana.Shared.Enums.TicketType)").take(1).page(0).executeQuery();

    this.soldCompCountService.setCustomEndpoint('GetAll');
    this.soldCompCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Sold+"', Nirvana.Shared.Enums.SeatStatus)").and('TicketType', '=', "cast('"+TicketType.Comp+"', Nirvana.Shared.Enums.TicketType)").take(1).page(0).executeQuery();

    this.soldGroupCountService.setCustomEndpoint('GetAll');
    this.soldGroupCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Sold+"', Nirvana.Shared.Enums.SeatStatus)").and('TicketType', '=', "cast('"+TicketType.Group+"', Nirvana.Shared.Enums.TicketType)").take(1).page(0).executeQuery();

    this.reservedIndividualCountService.setCustomEndpoint('GetAll');
    this.reservedIndividualCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Reserved+"', Nirvana.Shared.Enums.SeatStatus)").and('TicketType', '=', "cast('"+TicketType.Sale+"', Nirvana.Shared.Enums.TicketType)").take(1).page(0).executeQuery();

    this.reservedCompCountService.setCustomEndpoint('GetAll');
    this.reservedCompCountService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Reserved+"', Nirvana.Shared.Enums.SeatStatus)").and('TicketType', '=', "cast('"+TicketType.Comp+"', Nirvana.Shared.Enums.TicketType)").take(1).page(0).executeQuery();
  }

  private availableSeatsServicesDataHandler() {
    this.blockedSeatsService.data.subscribe( entities => this.setStatusAndSaveSeat(entities), error => this.isLoading = false );
    this.canceledSeatsService.data.subscribe( entities => this.setStatusAndSaveSeat(entities), error => this.isLoading = false );
    this.availableSeatsService.data.subscribe( entities => this.setStatusAndSaveSeat(entities), error => this.isLoading = false );
  }

  private setStatusAndSaveSeat(seats) {
    if(!seats || seats.length == 0) return;
    this.selectedSeatsActionHandler({action: this.currentAction, params:{target: seats}});
    this.currentAction = null;
    this.currentValue = 0;
    this.changeDetector.detectChanges();
    this.currentValue = null;
    this.changeDetector.detectChanges();
  }

  private statisticsDataHandler() {
    this.totalCapacityCountService.count.subscribe( count => this.setStatistics('totalCapacityCount', count) );
    this.onSaleCountService.count.subscribe( count => this.setStatistics('onSaleCount', count) );

    this.pendingCountService.count.subscribe( count => this.setStatistics('pendingCount', count) );
    this.soldCountService.count.subscribe( count => this.setStatistics('soldCount', count) );
    this.blockedCountService.count.subscribe( count => this.setStatistics('blockedCount', count) );
    this.canceledCountService.count.subscribe( count => this.setStatistics('canceledCount', count) );

    this.soldIndividualCountService.count.subscribe( count => this.setStatistics('soldIndividualCount', count) );
    this.soldSeasonalCountService.count.subscribe( count => this.setStatistics('soldSeasonalCount', count) );
    this.soldCompCountService.count.subscribe( count => this.setStatistics('soldCompCount', count) );
    this.soldGroupCountService.count.subscribe( count => this.setStatistics('soldGroupCount', count) );

    this.reservedIndividualCountService.count.subscribe( count => this.setStatistics('reservedIndividualCount', count) );
    this.reservedCompCountService.count.subscribe( count => this.setStatistics('reservedCompCount', count) );
  }

  private setStatistics(prop, value) {
    console.log(prop, value);
    this[prop] = value;
    this.statistics = null;
    
    let infoStatistics = [];
    if(this.totalCapacityCount != null) infoStatistics.push({key: 'totalCapacityCount', label: 'TOPLAM KAPASİTE', value: this.totalCapacityCount});
    if(this.onSaleCount != null) infoStatistics.push({key: 'onSaleCount', label: 'SATIŞA AÇIK', value: this.onSaleCount});
    
    let statusStatistics = [];
    if(this.soldCount != null) statusStatistics.push({color: '009D4E', key: 'soldCount', label: 'SATILAN', value: this.soldCount});
    if(this.blockedCount != null) statusStatistics.push({color: 'FF495C', key: 'blockedCount', label: 'BLOKE', value: this.blockedCount});
    if(this.canceledCount != null) statusStatistics.push({color: 'D9272E', key: 'canceledCount', label: 'İPTAL', value: this.canceledCount});

    let soldStatistics = [];
    if(this.soldIndividualCount != null) soldStatistics.push({color: '009D4E', key: 'soldIndividualCount', label: 'TEK BİLET', value: this.soldIndividualCount});
    if(this.soldSeasonalCount != null) soldStatistics.push({color: '00B550', key: 'soldSeasonalCount', label: 'SEZON BİLETİ', value: this.soldSeasonalCount});
    if(this.soldCompCount != null) soldStatistics.push({color: '05CE7C', key: 'soldCompCount', label: 'ÜCRETSİZ BİLET', value: this.soldCompCount});
    if(this.soldGroupCount != null) soldStatistics.push({color: 'F4DFE0', key: 'soldGroupCount', label: 'GRUP BİLET', value: this.soldGroupCount});

    let reservedStatistics = [];
    if(this.reservedIndividualCount != null) reservedStatistics.push({color: 'FF5000', key: 'reservedIndividualCount', label: 'RESERVE BİLET', value: this.reservedIndividualCount});
    if(this.reservedCompCount != null) reservedStatistics.push({color: 'FF9012', key: 'reservedCompCount', label: 'ÜCRETSİZ RESERVE BİLET', value: this.reservedCompCount});

    this.statistics = [infoStatistics, statusStatistics, soldStatistics, reservedStatistics];
    this.changeDetector.detectChanges();
  }

  private venueEditorEventHandler(event) {
    switch(event.type) {
      case VenueTemplateEditorComponent.EVENT_READY:
        this.isLoading = false;
      break;
      case VenueTemplateEditorComponent.EVENT_SELECT:
        this.selectedSeats = event.payload.filter( item => item["Status"] != null);
        console.log(event);
      break;
    }
    this.changeDetector.detectChanges();
  }

  private selectedSeatsActionHandler(event) {
    if(!event.params.target) return;
    let venueSeat: VenueSeat;
    let venueSeats: VenueSeat[] = [];
    let status;
    switch(event.action) {
      case "openToAvailable":
        status = this.performance.Status == PerformanceStatus.OnSale ? SeatStatus.OnSale : SeatStatus.Pending;
      break;
      case "blockSeats":
        status = SeatStatus.Hold;
      break;
      case "cancelSeats":
        status = SeatStatus.Killed;
      break;
    }
    event.params.target.forEach( seat => {
      venueSeat = new VenueSeat();
      venueSeat.Id = seat["Id"];
      venueSeat.Status = status;
      venueSeats.push(venueSeat);
      console.log(event.action, status, venueSeat);
    });
    this.saveSeats(venueSeats);
    venueSeats = venueSeat = status = null;
  }


  private availableSeatsActionHandler(event, targetService) {
    this.currentAction = event.action;
    this.isLoading = true;
    switch(targetService) {
      case "availableSeatsService":
        if(event.value > this.onSaleCount) {
          event.value = this.onSaleCount;
          this.notificationService.add({text: "Eklediğiniz kapasite değeri daha fazla olduğu için maksimum kapasiteye eşitlenmiştir.", type: "warning"});
        }

        this.availableSeatsService.setCustomEndpoint('GetAll');
        this.availableSeatsService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).or([['Status', '=', "cast('"+SeatStatus.OnSale+"', Nirvana.Shared.Enums.SeatStatus)"], ['Status', '=', "cast('"+SeatStatus.Pending+"', Nirvana.Shared.Enums.SeatStatus)"]]).take(event.value).page(0).executeQuery();
      break;
      case "blockedSeatsService":
        if(event.value > this.blockedCount) {
          event.value = this.blockedCount;
          this.notificationService.add({text: "Eklediğiniz kapasite değeri daha fazla olduğu için maksimum kapasiteye eşitlenmiştir.", type: "warning"});
        }

        this.blockedSeatsService.setCustomEndpoint('GetAll');
        this.blockedSeatsService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Hold+"', Nirvana.Shared.Enums.SeatStatus)").take(event.value).page(0).executeQuery();
      break;
      case "canceledSeatsService":
        if(event.value > this.canceledCount) {
          event.value = this.canceledCount;
          this.notificationService.add({text: "Eklediğiniz kapasite değeri daha fazla olduğu için maksimum kapasiteye eşitlenmiştir.", type: "warning"});
        }

        this.canceledSeatsService.setCustomEndpoint('GetAll');
        this.canceledSeatsService.fromEntity('EVenueSeat').where('VenueRow/PerformanceId', '=', this.performance.Id).and('Status', '=', "cast('"+SeatStatus.Killed+"', Nirvana.Shared.Enums.SeatStatus)").take(event.value).page(0).executeQuery();
      break;
    }
  }

  private saveSeats(venueSeats: VenueSeat[]) {
    this.isLoading = true;
    this.venueSeatService.setCustomEndpoint('PatchAll');
    this.venueSeatService.update(venueSeats).subscribe( result => {
      this.isLoading = false;
      this.resetStatistics();
      this.notificationService.add({type: "success", text: "Koltuklar başarıyla güncellendi"});
      let seat;
      venueSeats.forEach( venueSeat => {
        seat = this.selectedSeats.find( item => item["Id"] == venueSeat.Id);
        if(seat) {
          seat.Status = venueSeat.Status;
          this.venueEditor.setSeat(seat);
        }
      });
    }, error => {
      this.notificationService.add({type: "danger", text: "Koltuk durumu değiştirilemedi"});
      this.isLoading = false
    });
  }

}
