import { Component, OnInit, HostBinding, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../../../services/product.service';
import { EntityService } from '../../../services/entity.service';


@Component({
	selector: 'app-performance-products',
	templateUrl: './performance-products.component.html',
	styleUrls: ['./performance-products.component.scss'],
	providers:[
		ProductService, EntityService,
		{provide: 'entityServiceInstance1', useClass: EntityService },
		{provide: 'entityServiceInstance2', useClass: EntityService },
		{provide: 'entityServiceInstance3', useClass: EntityService },
	]
})
export class PerformanceProductsComponent implements OnInit {
	@HostBinding('class.or-performance-products') true;
	subscription;
	errorMessage: any;

	products = [];
	private isLoading: boolean = false;
	private noDataInContent: boolean = true;

	totalCapacity: number = 0;
	openCapacity: number = 0;
	soldTicketCount: number = 0;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private productService: ProductService,
		private entityService: EntityService,
		@Inject('entityServiceInstance1') private capacityService: EntityService,
		@Inject('entityServiceInstance2') private OpenCapacityService: EntityService,
		@Inject('entityServiceInstance3') private soldTicketService: EntityService,
	) { }

	ngOnInit() {
		this.subscription = this.route.parent.params.subscribe(params => {
			this.isLoading = true;
			this.entityService.setCustomEndpoint('GetAll');
			this.entityService.fromEntity('EPerformanceProduct')
			.where('PerformanceId', '=', params['id'])
			.expand(['Product', 'Localization'])
			.expand(['Product', 'PriceLists','Variants'])
			.expand(['Product', 'Currency'])
			.expand(['Performance'])
			.take(40)
			.page(0)
			.executeQuery();

			this.capacityService.setCustomEndpoint('GetAll');
			this.capacityService.fromEntity('EVenueSeat')
			.where('VenueRow/PerformanceId', '=', params['id'])
			.take(1).page(0)
			.executeQuery();

			this.soldTicketService.setCustomEndpoint('GetAll');
			this.soldTicketService.fromEntity('EVenueSeat')
			.where('VenueRow/PerformanceId', '=', params['id'])
			.and('Status', '=', "cast('2', Nirvana.Shared.Enums.SeatStatus)")
			.take(1).page(0)
			.executeQuery();

			this.OpenCapacityService.setCustomEndpoint('GetAll');
			this.OpenCapacityService.fromEntity('EVenueSeat')
			.where('VenueRow/PerformanceId', '=', params['id'])
			.and('Status', '=', "cast('1', Nirvana.Shared.Enums.SeatStatus)")
			.take(1).page(0)
			.executeQuery();
		});

		this.entityService.data.subscribe(entities => {
			this.products = [];
			entities.forEach(entity => {
				let product = entity['Product'];
				let price = product['PriceLists'][0];
				if(price) {
					product.CurrentPrice = price['NominalPrice'];
					product.SalesBeginDate = price['BeginDate'];
					product.SalesEndDate = price['EndDate'];
				}
				this.products.push(product);
			});
			this.isLoading = false;
			if(this.products.length == 0) {
				this.noDataInContent = true;
			} else {
				this.noDataInContent = false;
			}
		});

		this.capacityService.getCount().subscribe(
			count => { this.totalCapacity = count; },
			error => this.errorMessage = <any>error
		);

		this.soldTicketService.getCount().subscribe(
			count => { this.soldTicketCount = count; },
			error => this.errorMessage = <any>error
		);

		this.OpenCapacityService.getCount().subscribe(
			count => { this.openCapacity = count; },
			error => this.errorMessage = <any>error
		);
	}

	productActionHandler(event) {
		console.log(event);
		switch(event.action) {
			case "edit":
			this.router.navigate(['product', event.params.id, 'edit']);
			break;
		}
	}

}
