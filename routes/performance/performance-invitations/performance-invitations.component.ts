import { Component, OnInit, HostBinding, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EntityService } from '../../../services/entity.service';
import { InvitationType } from '../../../models/invitation-type.enum';


@Component({
	selector: 'app-performance-invitations',
	templateUrl: './performance-invitations.component.html',
	styleUrls: ['./performance-invitations.component.scss'],
	providers:[
		EntityService,
		{provide: 'entityServiceInstance1', useClass: EntityService },
		{provide: 'entityServiceInstance2', useClass: EntityService },
		{provide: 'entityServiceInstance3', useClass: EntityService },
		{provide: 'entityServiceInstance4', useClass: EntityService },
		{provide: 'entityServiceInstance5', useClass: EntityService },
	]
})
export class PerformanceInvitationsComponent implements OnInit {
	subscription;
	errorMessage: any;
	invitationStatus = InvitationType;

	totalCapacity: number = 0;
	openCapacity: number = 0;

	SponsorInvitationCount: number = 0;
	IndividualInvitationCount: number = 0;
	TargetGroupInvitationCount: number = 0;

	invitations = [];
	count: number;
	selectedItems: Array<Object> = [];
	isAllSelected: boolean = false;

	showPagination: boolean = true;
	pageSizes: Array<Object> = [{ text: '10', value: 10 }, { text: '20', value: 20 }];
	pageSize: number = 10;
	page: number = 1;
	private currentPage : number;

	private isLoading: boolean = false;
	private noDataInContent: boolean = true;
	private query;

	constructor(
		private entityService: EntityService,
		private route: ActivatedRoute,
		@Inject('entityServiceInstance1') private capacityService: EntityService,
		@Inject('entityServiceInstance2') private OpenCapacityService: EntityService,
		@Inject('entityServiceInstance3') private SponsorInvitationService: EntityService,
		@Inject('entityServiceInstance4') private IndividualInvitationService: EntityService,
		@Inject('entityServiceInstance5') private TargetGroupInvitationService: EntityService,
	) {
		this.changePageSize(this.pageSize);
		this.entityService.setCustomEndpoint('GetAll');
	}

	ngOnInit() {

		this.subscription = this.route.parent.params.subscribe(params => {
			this.isLoading = true;
			this.query = this.entityService.fromEntity('BRsvp')
			.where('PerformanceId', '=', params['id']);
			// .expand(['Product']);
			// .expand(['Product', 'PriceLists','Variants'])
			// .expand(['Product', 'Currency'])
			// .expand(['Performance'])

			this.capacityService.setCustomEndpoint('GetAll');
			this.capacityService.fromEntity('EVenueSeat')
			.where('VenueRow/PerformanceId', '=', params['id'])
			.take(1).page(0)
			.executeQuery();

			this.OpenCapacityService.setCustomEndpoint('GetAll');
			this.OpenCapacityService.fromEntity('EVenueSeat')
			.where('VenueRow/PerformanceId', '=', params['id'])
			.and('Status', '=', "cast('1', Nirvana.Shared.Enums.SeatStatus)")
			.take(1).page(0)
			.executeQuery();

			this.SponsorInvitationService.setCustomEndpoint('GetAll');
			this.SponsorInvitationService.fromEntity('BRsvp')
			.where('PerformanceId', '=', params['id'])
			.and('RsvpType', '=', `cast('${this.invitationStatus['Sponsor']}', Nirvana.Shared.Enums.RsvpType)`)
			.take(1).page(0)
			.executeQuery();

			this.IndividualInvitationService.setCustomEndpoint('GetAll');
			this.IndividualInvitationService.fromEntity('BRsvp')
			.where('PerformanceId', '=', params['id'])
			.and('RsvpType', '=', `cast('${this.invitationStatus['Individual']}', Nirvana.Shared.Enums.RsvpType)`)
			.take(1).page(0)
			.executeQuery();

			this.TargetGroupInvitationService.setCustomEndpoint('GetAll');
			this.TargetGroupInvitationService.fromEntity('BRsvp')
			.where('PerformanceId', '=', params['id'])
			.and('RsvpType', '=', `cast('${this.invitationStatus['TargetGroup']}', Nirvana.Shared.Enums.RsvpType)`)
			.take(1).page(0)
			.executeQuery();
		});

		this.subscription = this.entityService.queryParamSubject.subscribe(
			params => {
				this.isLoading = true;
				let sort = params["sort"] ? (typeof params["sort"] == 'string'  ? JSON.parse(params["sort"]) : params["sort"]) : null;
				let query = this.query
				.take(params['pageSize'])
				.page(params['page']);

				if(sort && sort[0]){
					query.orderBy(sort[0]["sortBy"],sort[0]["type"])
				}
				if(params["search"]){
					query.search(params["search"]["key"], params["search"]["value"]);
				}
				query.executeQuery();
			},
			error => this.errorMessage = <any>error
		);

		this.entityService.data.subscribe(entities => {
			// this.invitations = [
			// 	{RsvpName: "Name", RsvpType: 1, Capacity: 3, Count: 4, TicketPerUser: 5},
			// 	{RsvpName: "Name", RsvpType: 2, Capacity: 3, Count: 4, TicketPerUser: 5},
			// 	{RsvpName: "Name", RsvpType: 3, Capacity: 3, Count: 4, TicketPerUser: 5},
			// ];
			this.invitations = entities;
			this.isLoading = false;
			if(this.invitations.length == 0) {
				this.noDataInContent = true;
			} else {
				this.noDataInContent = false;
			}
		});

		this.entityService.getCount().subscribe(
			count => { this.count = count; },
			error => this.errorMessage = <any>error
		);

		this.capacityService.getCount().subscribe(
			count => { this.totalCapacity = count; },
			error => this.errorMessage = <any>error
		);

		this.OpenCapacityService.getCount().subscribe(
			count => { this.openCapacity = count; },
			error => this.errorMessage = <any>error
		);

		this.SponsorInvitationService.getCount().subscribe(
			count => { this.SponsorInvitationCount = count; },
			error => this.errorMessage = <any>error
		);

		this.IndividualInvitationService.getCount().subscribe(
			count => { this.IndividualInvitationCount = count; },
			error => this.errorMessage = <any>error
		);

		this.TargetGroupInvitationService.getCount().subscribe(
			count => { this.TargetGroupInvitationCount = count; },
			error => this.errorMessage = <any>error
		);
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	selectAllItems(selectAll: boolean): void {
		if (selectAll && this.selectedItems.length < this.invitations.length) {
			this.selectedItems = [];
			this.invitations.forEach(item => {
				this.selectedItems.push(item);
			});
			this.isAllSelected = true;
		}
		if (!selectAll) {
			this.isAllSelected = false;
			this.selectedItems = [];
		}
	}

	selectItem(isSelected: boolean, invitation: Event): void {
		if (isSelected) {
			this.selectedItems.push(invitation);
		} else {
			let selectedEvent = this.selectedItems.filter(item => {
				return (invitation === item);
			})[0];
			this.selectedItems.splice(this.selectedItems.indexOf(selectedEvent), 1);
		}
	}

	get isMultiSelectionActive(): boolean {
		return this.selectedItems.length > 0;
	}

	onInputChange(value) {
		this.entityService.setSearch({ key: 'UserId', value: value });
	}

	toggleSortTitle(sort) {
		if(sort){
			this.entityService.setOrder(sort, true);
		} else {
			this.entityService.flushOrder();
		}
	}

	changePageSize(pageSize) {
		this.entityService.setPageSize(pageSize);
		this.pageSize = pageSize;
	}

	transistPage(page) {
		this.currentPage = page;
		this.entityService.setPage(page);
	}

	openEventsContextMenu(e, invitation) {
		console.log(invitation);
	}

}
