import { VariantPrice } from './../../../models/variant-price';
import { VariantPriceService } from './../../../services/variant-price.service';
import { VariantService } from './../../../services/variant.service';
import { Variant } from './../../../models/variant';
import { ProductPriceBlockComponent } from './../../../components/product-price-block/product-price-block.component';
import { PriceListService } from './../../../services/price-list.service';
import { PriceList } from './../../../models/price-list';
import { VenueSeatService } from './../../../services/venue-seat.service';
import { VenueSeat } from './../../../models/venue-seat';
import { ProductProductService } from './../../../services/product-product.service';
import { ProductProduct } from './../../../models/product-product';
import { ProductSearchSelectComponent } from './../../../components/product-search-select/product-search-select.component';
import { PerformanceCapacitySearchSelectComponent } from './../../../components/performance-capacity-search-select/performance-capacity-search-select.component';
import { TetherDialog } from './../../../components/tether-dialog/tether-dialog';
import { VenueTemplateEditorComponent } from './../../../components/venue-template-editor/venue-template-editor.component';
import { PerformanceProductService } from './../../../services/performance-product.service';
import { NotificationService } from './../../../services/notification.service';
import { Performance } from './../../../models/performance';
import { EntityService } from './../../../services/entity.service';
import { PerformanceProduct } from './../../../models/performance-product';
import { PerformanceStatus } from './../../../models/performance-status.enum';
import { MultiSelectGroupComponent } from './../../../components/multi-select-group/multi-select-group.component';
import { Product } from './../../../models/product';
import { ProductService } from './../../../services/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderTitleService } from './../../../services/header-title.service';
import { Component, OnInit, ChangeDetectorRef, ViewChild, Inject, ViewChildren, QueryList } from '@angular/core';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.scss'],
  providers: [
	{ provide: 'productEntityService', useClass: EntityService }, 
	{ provide: 'productProductEntityService', useClass: EntityService }, 
	{ provide: 'performanceProductEntityService', useClass: EntityService }, 
	{ provide: 'currencyEntityService', useClass: EntityService }, 
	{ provide: 'venueSeatEntityService', useClass: EntityService }, 
	ProductService, EntityService, PerformanceProductService, ProductProductService, VenueSeatService, PriceListService, VariantService, VariantPriceService]
})
export class ProductCreateComponent implements OnInit {
	@ViewChild(MultiSelectGroupComponent) typeSelector: MultiSelectGroupComponent;
	@ViewChild(VenueTemplateEditorComponent) venueEditor: VenueTemplateEditorComponent;
	@ViewChild(PerformanceCapacitySearchSelectComponent) performanceCapacitySearchSelect: PerformanceCapacitySearchSelectComponent;
	@ViewChild(ProductSearchSelectComponent) productSearchSelect: ProductSearchSelectComponent;
	@ViewChildren(ProductPriceBlockComponent) priceBlocks: QueryList<ProductPriceBlockComponent>;

  	private role: string = "create";
	private isEditMode: boolean = false;
	private isBundle: boolean = false;
	private isLoading: boolean;
	private isPromising: boolean;
	private editorIsShown: boolean = false;
	private onSaveComplete: any;
	private title: string;

	private product: Product;
	private performanceProduct: PerformanceProduct;
	private performance: Performance;
	private products: Product[];
	private venueSeats: VenueSeat[];
	private selectedSeats: {}[];
	private currencyList: { value: any, text: string }[];
	private vatList: { value: any, text: string }[];
	private priceListActions: {action: string, label: string, icon?: string, params?: any, group?: any }[];

	private promises: {
		product: { name:string, old: any, new: any, saved: {performanceProduct: boolean, products: boolean, venueSeats: boolean, priceLists: boolean, variants: boolean, variantPrices: boolean}}, 
		products: { name: string, old: ProductProduct[], new: ProductProduct[], saved: {create: boolean, update: boolean, delete: boolean} },
		performanceProduct: { name: string, old: PerformanceProduct, new: PerformanceProduct, saved: boolean },
		venueSeats: { name: string, old: VenueSeat[], new: VenueSeat[], saved: {create: boolean, update: boolean, delete: boolean} },
		priceLists: { name: string, old: PriceList[], new: PriceList[], saved: {create: boolean, update: boolean, delete: boolean} },
		variants: { name: string, old: Variant[], new: Variant[], saved: {create: boolean, update: boolean, delete: boolean} },
		variantPrices: { name: string, old: VariantPrice[], new: VariantPrice[], saved: {create: boolean, update: boolean, delete: boolean} } } = {
			
		product: { name: "product", old: null, new: null, saved: {performanceProduct: false, products: false, venueSeats: false, priceLists: false, variants: false, variantPrices: false}},
		products: { name: "products", old: [], new: [], saved: {create: false, update: false, delete: false} },
		performanceProduct: { name: "performance", old: null, new: null, saved: false },
		venueSeats: { name: "venueSeats", old: [], new: [], saved: {create: false, update: false, delete: false} },
		priceLists: { name: "priceLists", old: [], new: [], saved: {create: false, update: false, delete: false} },
		variants: { name: "variants", old: [], new: [], saved: {create: false, update: false, delete: false} },
		variantPrices: { name: "variantPrices", old: [], new: [], saved: {create: false, update: false, delete: false} },
	};

	private validation: {
		Name: { isValid: any, message: string },
		Performance: { isValid: any, message: string },
		Products: { isValid: any, message: string },
		Vat: { isValid: any, message: string },
		Capacity: { isValid: any, message: string },
		PriceBlocks: { isValid: any, message: string },
	} = {
		Name: {
			message: "Ürün adı zorunludur.",
			isValid(): boolean {
				return this.product && this.product.Localization && this.product.Localization.Tr && this.product.Localization.Tr.Name && this.product.Localization.Tr.Name.length > 0;
			}
		},
		Performance: {
			message: "Performans eklenmesi zorunludur",
			isValid():boolean {
				return (this.product && this.product.IsBundle) ? true : this.performance != null;
			}
		},
		Products: {
			message: "Birleştirilmiş ürünlerde en az bir adet ürün eklenmelidir!",
			isValid():boolean {
				return (this.product && !this.product.IsBundle) ? true : (this.promises && this.promises.products && this.promises.products.new && this.promises.products.new.length > 0);
			}
		},
		Vat: {
			message: "KDV oranı zorunlu alandır!",
			isValid():boolean {
				return this.product && this.product.Vat > 0;
			}
		},
		Capacity: {
			message: "Kapasite bilgisi zorunludur!",
			isValid():boolean {
				return this.product && this.product.IsBundle ? true : this.performanceProduct && this.performanceProduct.Capacity > 0;
			}
		},
		PriceBlocks: {
			message: "Fiyat bilgilerinde zorunlu alanlar eksik!",
			isValid():boolean {
				return this.priceBlocks.find( item => item.isValid == false ) == null;
			}
		}
	};

	private get isValid():boolean {
		if( this.product && this.validation
			&& this.validation.Name.isValid.call(this)
			&& this.validation.Performance.isValid.call(this)
			&& this.validation.Products.isValid.call(this)
			//&& this.validation.Capacity.isValid.call(this)
			&& this.validation.PriceBlocks.isValid.call(this)
			){
			return true;
		}else{
			// if( this.product && this.validation) console.log(
			// 	this.validation.Name.isValid.call(this),
			// 	this.validation.Performance.isValid.call(this),
			// 	this.validation.Products.isValid.call(this)
			// )
			return false
		}
	};

	private get allPriceCollapsed():boolean {
		let expandedPriceBlock:ProductPriceBlockComponent = this.priceBlocks.find( priceBlock => priceBlock.expandableBlock.isExpanded );
		return expandedPriceBlock ? false : true;
	}

  constructor(
	@Inject('productEntityService') private productEntityService: EntityService,
	@Inject('productProductEntityService') private productProductEntityService: EntityService,
	@Inject('performanceProductEntityService') private performanceProductEntityService: EntityService,
	@Inject('venueSeatEntityService') private venueSeatEntityService: EntityService,
	@Inject('currencyEntityService') private currencyEntityService: EntityService,
    private productService: ProductService,
	private entityService: EntityService,
	private performanceProductService: PerformanceProductService,
	private productProductService: ProductProductService,
	private venueSeatService: VenueSeatService,
	private priceListService: PriceListService,
	private variantService: VariantService,
	private variantPriceService: VariantPriceService,

	private headerTitleService: HeaderTitleService,
	private notificationService: NotificationService,
    
    private router: Router,
    private route: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
	private tetherService: TetherDialog,
  ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Ürünler');
	this.role = this.route.snapshot.data["role"];;
	this.isEditMode = this.role == "edit";

	this.currencyEntityService.setCustomEndpoint('GetAll');
	this.currencyEntityService.data.subscribe( result => {
		this.currencyList = [];
		// this.currencyList.push({text: 'Seçiniz', value: -1});
		result.forEach( currency => {
			//this.currencyList.push({text: `${currency['Code']} / ${currency['Name']}`, value: currency['Id']});
			this.currencyList.push({text: `${currency['Code']}`, value: currency['Id']});
		});
	});
	this.currencyEntityService.fromEntity('CCurrency').take(100).page(0).executeQuery();

	this.vatList = [];
	this.vatList.push({text: "Seçiniz", value: 0});
	this.vatList.push({text: "%1", value: 0.01});
	this.vatList.push({text: "%8", value: 0.08});
	this.vatList.push({text: "%18", value: 0.18});

	this.priceListActions = [];
	
	this.setProduct();
  }

  private setProduct() {
		if(this.isEditMode && this.route.snapshot.params && this.route.snapshot.params && this.route.snapshot.params["id"]){
			let id = this.route.snapshot.params["id"];
			this.isLoading = true;
			this.productEntityService.data.subscribe( products => {
				if(products && products[0]) {
					this.product = new Product(products[0]);
					this.promises.product.old = new Product(this.product);
					console.log(this.product);
					if(!this.product) return;

					this.isBundle = this.product.IsBundle;
					if(this.product.Localization) this.inputChangeHandler(this.product.Localization["Name"], "Name");
					if(this.product.Localization) this.inputChangeHandler(this.product.Localization["Info"], "Info");
					
					
					if(this.isBundle) {
						this.productProductsServiceDataHandler();
						this.resetProductProducts(true);
					}else{
						this.performanceProductEntityServiceDataHandler();
						this.resetPerformaceProduct(true);

						this.venueSeatsServiceDataHandler();
						this.resetVenueSeats(true);

						if(this.product.PriceLists) this.product.PriceLists.forEach( priceList => {
							this.promises.priceLists.old.push(priceList);
							if(priceList.Variants) priceList.Variants.forEach( variant => {
								this.promises.variants.old.push(variant);
								if(variant.Prices) variant.Prices.forEach( variantPrice => {
									this.promises.variantPrices.old.push(variantPrice);
								})
							});
						});
						this.resetPriceLists();
					}
					
					this.changeDetector.detectChanges();
				}
				this.isLoading = false;
			});
			
			this.productEntityService.setCustomEndpoint('GetAll');
			this.productEntityService.fromEntity('PProduct')
				.where('Id', '=', id)
				.expand(['Localization'])
				.expand(['PriceLists', 'Localization'])
				.expand(['PriceLists', 'Variants', 'Localization'])
				.expand(['PriceLists', 'Variants', 'Prices', 'SalesChannel'])
				.expand(['Currency'])
				.take(1).page(0).executeQuery();

			//this.resetPerformaceProduct(true, id);
		}else {
			this.product = new Product({
				GroupId: 1,
				CurrencyId: 19,
				Name: this.title,
				// Info: null,
				// OrganizerFirmId: null,
				// Vat: 0,
				IsRefundable: true,
				IsBundle: this.isBundle,
				// MaxProductsPerTrx: 0,
				// IsSeatSelectionAvailable: false
			});
			this.product.IsRefundable = this.product.GroupId != 1;
			if(this.title) this.inputChangeHandler(this.title, "Name");
			
			this.promises.product.old = new Product(this.product);
		}
		this.changeDetector.detectChanges();
	}
	
	private productProductsServiceDataHandler() {
		if(!this.product || !this.product.Id) return;
		this.productProductEntityService.data.subscribe( result => {
			if(result) {
				this.promises.products.old = [];
				this.promises.products.new = [];
				this.products = [];

				let productProduct: ProductProduct;
				result.forEach( productProductData => {
					productProduct = new ProductProduct(productProductData);
					this.promises.products.old.push(productProduct);
					this.promises.products.new.push(productProduct);

					this.products.push(productProduct["RelatedProduct"])
				});
			}
		})
	}

	private resetProductProducts(flushQuery:boolean = false) {
		if(this.promises.products.saved.create && this.promises.products.saved.update && this.promises.products.saved.delete) {
			this.promises.products.saved = {create: false, update: false, delete: false};
			this.promises.product.saved.products = true;
			this.checkSaved();
			flushQuery = true;
		}
		if(flushQuery) {
			if(!this.product || !this.product.Id) return;
			this.productProductEntityService.setCustomEndpoint("GetAll");
			this.productProductEntityService
				.fromEntity('PProductProduct')
				.where('ProductId', '=',  this.product.Id)
				.expand(['Product'])
				.expand(['Product', 'PriceLists','Variants'])
				.expand(['Product', 'Currency'])
				.expand(['RelatedProduct'])
				.expand(['RelatedProduct', 'PriceLists','Variants'])
				.expand(['RelatedProduct', 'Currency'])
				.take(100)
				.page(0)
				.executeQuery();
		}
	}	

	private performanceProductEntityServiceDataHandler() {
		this.performanceProductEntityService.data.subscribe(entities => {
			if(entities) {
				if(entities.length == 1) {
					if(entities[0]["ProductId"] && entities[0]["PerformanceId"]) {
						this.performanceProduct = new PerformanceProduct(entities[0]);
						this.promises.performanceProduct.old = this.performanceProduct;
						this.promises.performanceProduct.new = this.performanceProduct;

						this.performance = new Performance(entities[0].Performance);
						if(this.performance.Status != PerformanceStatus.OnSale) this.priceListActions.push({action: "remove", label: "Fiyatı Sil"});
					}
				}else{
					
				}
			}
		})
	}
	
	private resetPerformaceProduct(flushQuery:boolean = false) {
		if(this.promises.performanceProduct.saved) {
			this.promises.performanceProduct.saved=false;
			this.promises.product.saved.performanceProduct = true;
			this.checkSaved();
			flushQuery = true;
		}
		if(flushQuery) {
			if(!this.product || !this.product.Id) return;
			this.performanceProductEntityService.setCustomEndpoint('GetAll');
			this.performanceProductEntityService
				.fromEntity('EPerformanceProduct')
				.where('ProductId', '=',  this.product.Id)
				.expand(['Product'])
				.expand(['Product', 'PriceLists','Variants'])
				.expand(['Product', 'Currency'])
				.expand(['Performance', 'Localization'])
				.expand(['Performance', 'VenueTemplate', 'Localization'])
				.expand(['Performance', 'VenueTemplate', 'Venue', 'Localization'])
				.expand(['Performance', 'VenueTemplate', 'Venue', 'Town', 'City', 'Country', 'Localization'])
				.take(1)
				.page(0)
				.executeQuery();
		}
	}

	private venueSeatsServiceDataHandler() {
		if(!this.product || !this.product.Id) return;
		this.venueSeatEntityService.data.subscribe( result => {
			if(result) {
				this.promises.venueSeats.old = [];
				this.promises.venueSeats.new = [];

				let venueSeat: VenueSeat;
				result.forEach( venueSeatData => {
					venueSeat = new VenueSeat(venueSeatData);
					this.promises.venueSeats.old.push(venueSeat);
					this.promises.venueSeats.new.push(venueSeat);
				});
				//if(this.performanceProduct) this.performanceProduct.Capacity = this.promises.venueSeats.new.length;
			}
		})
	}

	private resetVenueSeats(flushQuery:boolean = false) {
		if(this.promises.venueSeats.saved.create && this.promises.venueSeats.saved.update && this.promises.venueSeats.saved.delete) {
			this.promises.venueSeats.saved = {create: false, update: false, delete: false};
			this.promises.product.saved.venueSeats = true;
			this.checkSaved();
			flushQuery = true;
		}
		if(flushQuery) {
			if(!this.product || !this.product.Id) return;
			this.venueSeatEntityService.setCustomEndpoint("GetAll");
			this.venueSeatEntityService
				.fromEntity('EVenueSeat')
				.where('ProductId', '=',  this.product.Id)
				.take(1000000)
				.page(0)
				.executeQuery();
		}
	}

	private resetPriceLists(flushQuery:boolean = false) {
		if(this.promises.priceLists.saved.create && this.promises.priceLists.saved.update && this.promises.priceLists.saved.delete) {
			this.promises.priceLists.saved = {create: false, update: false, delete: false};
			this.promises.product.saved.priceLists = true;
			this.saveVariants();
			this.checkSaved();
		}
	}

	private resetVariants(flushQuery:boolean = false) {
		if(this.promises.variants.saved.create && this.promises.variants.saved.update && this.promises.variants.saved.delete) {
			this.promises.variants.saved = {create: false, update: false, delete: false};
			this.promises.product.saved.variants = true;
			this.saveVariantPrices();
			this.checkSaved();
		}
	}

	private resetVariantPrices(flushQuery:boolean = false) {
		if(this.promises.variantPrices.saved.create && this.promises.variantPrices.saved.update && this.promises.variantPrices.saved.delete) {
			this.promises.variantPrices.saved = {create: false, update: false, delete: false};
			this.promises.product.saved.variantPrices = true;
			this.checkSaved();
		}
	}

	private tabChangeHandler(event) {
		switch(event.value) {
			case "product":
				this.tetherService.confirm({
					title: "Ürün türünü değiştiriyorsunuz!",
					description: "Birleştirilmiş ürün oluşturma işleminden ayrılmak istediğinize emin misiniz?",
					confirmButton: {label: "EVET"},
					dismissButton: {label: "VAZGEÇ"}
				}).then( result => {
					this.isBundle = false;
					this.setProduct();
				}).catch(reason => {
					this.typeSelector.selectedValues = ["bundleProducts"];
				});
			break;
			case "bundleProducts":
				this.tetherService.confirm({
					title: "Ürün türünü değiştiriyorsunuz!",
					description: "Performaslı ürün oluşturma işleminden ayrılmak istediğinize emin misiniz?",
					confirmButton: {label: "EVET"},
					dismissButton: {label: "VAZGEÇ"}
				}).then( result => {
					this.isBundle = true;
					this.setProduct();
				}).catch(reason => {
					this.typeSelector.selectedValues = ["product"];
				});
			break;
		}
	}

	private openVenueEditor(){
		if(!this.performance || !this.performance['VenueTemplate'] || !this.product) return;
		this.isLoading = true;
		this.editorIsShown = true;
	}

	private closeVenueEditor() {
		this.editorIsShown = false;
		this.selectedSeats = [];
	}

	private saveSelectedSeats() {
		this.isLoading = false;
		this.editorIsShown = false;
		this.venueSeats = [];
		let venueSeat: VenueSeat;
		this.selectedSeats.forEach( seat => {
			venueSeat = new VenueSeat();
			venueSeat.Id = seat["Id"];
			venueSeat.VenueGateId = seat["GateId"];
			venueSeat.IsStanding = seat["IsStanding"];
			venueSeat.VenueRowId = seat["RowId"];
			venueSeat.SeatPriority = seat["SeatPriority"];
			venueSeat.Status = seat["Status"];
			venueSeat.TicketType = seat["TicketType"];
			this.venueSeats.push(venueSeat);
		});
		if(this.performanceProduct) this.performanceProduct.Capacity = this.venueSeats.length;
		this.promises.venueSeats.new = this.venueSeats;
		this.selectedSeats = null;
	}

	private venueEditorEventHandler(event) {
		switch(event.type) {
			case VenueTemplateEditorComponent.EVENT_READY:
				this.isLoading = false;
				let self = this;
				setTimeout(function() {
					self.selectSeats();
				}, 8000);
				this.selectSeats();				
			break;
			case VenueTemplateEditorComponent.EVENT_SELECT:
				this.selectedSeats = event.payload.filter( item => item["Status"] != null);
				console.log(this.selectedSeats);
				this.changeDetector.detectChanges();
			break;
		}
	}

	private selectSeats() {
		if(this.selectedSeats) {
			let seatIds: number[] = [];
			console.log(this.selectedSeats);
			this.selectedSeats.forEach( seat => {
				seatIds.push(seat["Id"]);
			});
			this.venueEditor.selectSeats(seatIds);
			console.log(seatIds);
		}
	}

	private inputChangeHandler(event, name: string) {
		if(!this.product) return;
		switch(name) {
			case "Name":
				this.title = event;
				if(!this.product.Localization) this.product.Localization = {};
				if(!this.product.Localization.Tr) this.product.Localization.Tr = {};
				if(!this.product.Localization.En) this.product.Localization.En = {};
				this.product.Localization.Tr.Name = this.title;
				this.product.Localization.En.Name = this.title;
			break;
			case "Info":
				if(!this.product.Localization) this.product.Localization = {};
				if(!this.product.Localization.Tr) this.product.Localization.Tr = {};
				if(!this.product.Localization.En) this.product.Localization.En = {};
				this.product.Localization.Tr.Info = event;
				this.product.Localization.En.Info = event;
			break;
			default: 
				this.product[name] = event;
				break;
		}
		this.changeDetector.detectChanges();
	}

	private checkHandler(value, name:string, target: string = "product") {
		switch(name) {
			default:
			this[target][name] = value;
			break;
		}
	}

	private gotoProductCreate(){
		this.router.navigate(['event', 'create']);
	}

	private productsActionHandler(event) {
		switch(event.action) {
			case "openSearchBox":
				
			break;
			case "createEvent":
				if(!this.isValid){
					this.tetherService.dismiss();
					this.tetherService.confirm({
						title: "Etkinlik Henüz Kaydedilmedi!",
						description: "Gerekli alanlar doldurulmadığı için kayıt işlemi yapılamadı. Yine de etkinliği kaydetmeden bir başka etkinlik oluşturmak istyor musunuz?",
						confirmButton: {label: "EVET"},
						dismissButton: {label: "VAZGEÇ"}
					}).then( result => {
						this.gotoProductCreate();
					}).catch(reason => {
						this.productSearchSelect.openSearchBox();
					});
				}else{
					this.onSaveComplete = this.gotoProductCreate;
					this.saveProduct();
				}
			break;
		}
	}

	private productsChangeHandler(event) {
		if(!event && event.length == 0) return;
		this.promises.products.new = [];
		let productProduct: ProductProduct;

		event.forEach(item => {
			productProduct = this.promises.products.old.find( productProductItem => item.RelatedProductId == productProductItem.RelatedProductId );
			if(!productProduct) {
				productProduct = new ProductProduct(
					{
						RelatedProductId: item.Id,
						PriceActionId: null
					}
				);
			}
			this.promises.products.new.push(productProduct);
		});
	}

	private performanceCapacityActionHandler(event: {action: string}) {
		switch(event.action) {
			case "openVenueEditor":
				this.openVenueEditor();
			break;
			case "addCapacity":
				if(this.performanceProduct && event["data"]) this.performanceProduct.Capacity = event["data"]["capacity"];
			break;
		}
	}

	private performanceCapacityChangeHandler(event:Performance) {
		if(this.performance && event && this.performance.Id == event.Id) return;
		this.performance = event;
		if(!this.performance) {
			this.performanceProduct = this.promises.performanceProduct.new = null;
			return;
		};		
		this.performanceProduct = new PerformanceProduct({
			PerformanceId: this.performance.Id,
			Capacity: 0,
			Duration: 0,
			CategoryColorSeat: 'ffffff',
			IsSoldOut: false,
			IsBundle: false,
			NonBundleLimit: 0
		});
		this.promises.performanceProduct.new = this.performanceProduct;
	}

	private addNewPrice() {
		if(!this.product) return;
		if(this.product && !this.product.PriceLists) this.product.PriceLists = [];
		let priceList = new PriceList();
		priceList.ProductId = this.product.Id;
		priceList.IsEnabled = true;
		this.product.PriceLists.push(priceList);
		this.changeDetector.detectChanges();
		this.priceBlocks.last.firstTextInput.focus();
	}

	private removePrice(priceList: PriceList) {
		if(!this.product || !this.product.PriceLists) return;
		let existPriceList: PriceList = this.product.PriceLists.find( item => item.Id == priceList.Id);
		if(existPriceList) this.product.PriceLists.splice(this.product.PriceLists.indexOf(existPriceList), 1);
		this.changeDetector.detectChanges();
	}

	private toggleAllPriceBlocks() {
		this.allPriceCollapsed ? this.priceBlocks.forEach( priceBlock => priceBlock.expand() ) : this.priceBlocks.forEach( priceBlock => priceBlock.collapse() );
	}

	private productPriceBlockActionHandler(event) {
		switch(event.action) {
			case "remove":
				this.removePrice(event.params.priceList)
			break;
		}
	}

	private productPriceBlockChangeHandler(event, index) {
		//console.log(this.product.PriceLists, event);
	}

	private colorPickerSelectEvent(event) {
		if(this.performanceProduct) this.performanceProduct.CategoryColorSeat = event.color;
	}

	private submitProduct(event) {
		this.saveProduct();
	}

	private exit(event) {
		this.router.navigate(["products"]);
	}

	private saveProduct() {
		this.isPromising = true;
		this.promises.product.new = this.product;
		if(this.product.Id) {
			this.productService.update(new Product(this.product)).subscribe( 
				result => {
					this.saveRelations();
				},
				error => {
					this.notificationService.add({text: `<b>Ürünler kaydedilemedi</b> Lütfen bütün gerekli alanları doldurun.<br/><small>${error.Message}</small>`, type:'warning', timeOut: 8000});
				},
				complete => {
					
				}
			)
		}else{
			this.productService.create(new Product(this.product)).subscribe( 
				result => {
					this.product.Id = result;
					this.saveRelations(true);
				},
				error => {
					this.notificationService.add({text: `<b>Ürünler kaydedilemedi</b> Lütfen bütün gerekli alanları doldurun.<br/><small>${error.Message}</small>`, type:'warning', timeOut: 8000});
				},
				complete => {

				}
			)
		}
	}

	private saveRelations(isNew: boolean = false) {
		if(!this.product.IsBundle) this.savePerformaceProduct(isNew);
		if(this.product.IsBundle) this.saveProductProducts(isNew);
		if(!this.product.IsBundle) this.saveVenueSeats(isNew);
		if(!this.product.IsBundle) this.savePriceLists(isNew);
	}

	private saveProductProducts(isNew: boolean = false){
		if(this.product) { this.promises.products.new.map( item => item.ProductId = this.product.Id) };
		let willUpdate: ProductProduct[] = [];
		let willDelete: ProductProduct[] = [].concat(this.promises.products.old);
		let willCreate: ProductProduct[] = [].concat(this.promises.products.new);
		let sourceList: ProductProduct[] = [].concat(this.promises.products.new);

		let item: ProductProduct;
		let matchedItem: ProductProduct;
		let action: string;
		while(sourceList.length > 0) {
			item = sourceList.shift();
			matchedItem = willDelete.find( productPrdouct => item.ProductId == productPrdouct.ProductId);
			if(matchedItem) {
				willDelete.splice(willDelete.indexOf(matchedItem), 1);
				willCreate.splice(willCreate.indexOf(matchedItem), 1);
				willUpdate.push(item);
			}
		};

		// console.log("will delete : ", willDelete);
		// console.log("will update  : ", willUpdate);
		// console.log("will create : ", willCreate);

		if(willCreate.length > 0) {
			this.productProductService.setCustomEndpoint('PostAll');
			this.productProductService.create(willCreate).subscribe(
				response => {
					this.promises.products.saved.create = true;
					this.resetProductProducts();
				},
				error => {
					console.log("error : ", error);
				}
			);
		}else{
			this.promises.products.saved.create = true;
			this.resetProductProducts();
		}

		if(willUpdate.length > 0) {
			this.productProductService.setCustomEndpoint('PutAll');
			this.productProductService.update(willUpdate, 'put').subscribe(
				response => {
					this.promises.products.saved.update = true;
					this.resetProductProducts();
				}, error => {
					console.log(JSON.stringify(error));
				}
			);
		}else {
			this.promises.products.saved.update = true;
			this.resetProductProducts();
		}

		if(willDelete.length > 0) {
			let total: number = willDelete.length;
			let index: number = 0;
			willDelete.forEach(productPrdouct => {
				this.productProductService.setCustomEndpoint(productPrdouct.ProductId + '/' + productPrdouct.RelatedProductId);
				this.productProductService.delete(null).subscribe(result => {
					index++;
					if(total == index) {
						this.promises.products.saved.delete = true;
						this.resetProductProducts();
					}
				});
			});
		}else{
			this.promises.products.saved.delete = true;
			this.resetProductProducts();
		}
	}

	private savePerformaceProduct(isNew: boolean = false){
		if(this.performanceProduct) {
			if(!this.performanceProduct.ProductId) {
				this.performanceProduct.ProductId = this.product.Id;
				this.promises.performanceProduct.new = this.performanceProduct;

				this.performanceProductService.setCustomEndpoint(this.promises.performanceProduct.new.ProductId + "/" + this.promises.performanceProduct.new.PerformanceId);
				this.performanceProductService.create(this.promises.performanceProduct.new).subscribe(
					result => {
						this.promises.performanceProduct.saved = true;
						this.resetPerformaceProduct();
					},
					error => {
						this.notificationService.add({text: `<b>Ürün performans ilişkilisi kaydedilemedi.</b> Lütfen bütün gerekli alanları doldurun.<br/><small>${error.Message}</small>`, type:'warning', timeOut: 8000});
					}
				);
			}else{
				this.promises.performanceProduct.new = this.performanceProduct;

				this.performanceProductService.setCustomEndpoint(this.promises.performanceProduct.new.ProductId + "/" + this.promises.performanceProduct.new.PerformanceId);
				this.performanceProductService.update(this.promises.performanceProduct.new).subscribe(
					result => {
						this.promises.performanceProduct.saved = true;
						this.resetPerformaceProduct();
					},
					error => {
						this.isPromising = false;
						this.notificationService.add({text: `<b>Ürün performans ilişkilisi kaydedilemedi.</b> Lütfen bütün gerekli alanları doldurun.<br/><small>${error.Message}</small>`, type:'warning', timeOut: 8000});
					}
				);
			}
		}
	}

	private saveVenueSeats(isNew: boolean = false){
		if(!this.product) return;
		let willUpdate: VenueSeat[] = [];
		let willDelete: VenueSeat[] = [].concat(this.promises.venueSeats.old);
		let willCreate: VenueSeat[] = [].concat(this.promises.venueSeats.new);
		let sourceList: VenueSeat[] = [].concat(this.promises.venueSeats.new);

		let item: VenueSeat;
		let matchedItem: VenueSeat;
		let action: string;
		while(sourceList.length > 0) {
			item = sourceList.shift();
			matchedItem = willDelete.find( venueSeat => item.Id == venueSeat.Id);
			if(matchedItem) {
				willDelete.splice(willDelete.indexOf(matchedItem), 1);
				willCreate.splice(willCreate.indexOf(matchedItem), 1);
				willUpdate.push(item);
			}
		};

		// console.log("will delete : ", willDelete);
		// console.log("will update  : ", willUpdate);
		// console.log("will create : ", willCreate);

		if(willCreate.length > 0) {
			willCreate.map( item => item.ProductId = this.product.Id);
			this.venueSeatService.setCustomEndpoint('PatchAll');
			this.venueSeatService.update(willCreate).subscribe(
				response => {
					this.promises.products.saved.create = true;
					this.resetVenueSeats();
				},
				error => {
					console.log("error : ", error);
				}
			);
		}else{
			this.promises.venueSeats.saved.create = true;
			this.resetVenueSeats();
		}

		if(willUpdate.length > 0) {
			willUpdate.map(item => item.ProductId = this.product.Id);
			this.venueSeatService.setCustomEndpoint('PatchAll');
			this.venueSeatService.update(willUpdate).subscribe(
				response => {
					this.promises.venueSeats.saved.update = true;
					this.resetVenueSeats();
				}, error => {
					console.log(JSON.stringify(error));
				}
			);
		}else {
			this.promises.venueSeats.saved.update = true;
			this.resetVenueSeats();
		}

		if(willDelete.length > 0) {
			willDelete.map( item => item.ProductId = null);
			this.venueSeatService.setCustomEndpoint('PatchAll');
			this.venueSeatService.update(willDelete).subscribe(
				response => {
					this.promises.venueSeats.saved.delete = true;
					this.resetVenueSeats();
				}, error => {
					console.log(JSON.stringify(error));
				}
			);
		}else{
			this.promises.venueSeats.saved.delete = true;
			this.resetVenueSeats();
		}
	}

	private savePriceLists(isNew: boolean = false){
		if(this.product) { 
			this.promises.priceLists.new = [].concat(this.product.PriceLists);
			this.promises.priceLists.new.map( item => item.ProductId = this.product.Id) 
		};
		let willUpdate: PriceList[] = [];
		let willDelete: PriceList[] = [].concat(this.promises.priceLists.old);
		let willCreate: PriceList[] = [].concat(this.promises.priceLists.new);
		let sourceList: PriceList[] = [].concat(this.promises.priceLists.new);

		let item: PriceList;
		let matchedItem: PriceList;
		let action: string;
		while(sourceList.length > 0) {
			item = sourceList.shift();
			matchedItem = willDelete.find( priceList => item.Id == priceList.Id);
			if(matchedItem) {
				willDelete.splice(willDelete.indexOf(matchedItem), 1);
				willCreate.splice(willCreate.indexOf(willCreate.find( item => item.Id == matchedItem.Id)), 1);
				willUpdate.push(item);
			}
		};

		// console.log("will delete : ", willDelete);
		// console.log("will update  : ", willUpdate);
		// console.log("will create : ", willCreate);

		if(willCreate.length > 0) {
			this.priceListService.setCustomEndpoint('PostAll');
			this.priceListService.create(willCreate).subscribe(
				response => {
					response.forEach( (item, index) => {
						willCreate[index].Id = item;
					});
					this.promises.priceLists.saved.create = true;
					this.resetPriceLists();
				},
				error => {
					console.log("error : ", error);
				}
			);
		}else{
			this.promises.priceLists.saved.create = true;
			this.resetPriceLists();
		}

		if(willUpdate.length > 0) {
			this.priceListService.setCustomEndpoint('PutAll');
			this.priceListService.update(willUpdate, 'put').subscribe(
				response => {
					this.promises.priceLists.saved.update = true;
					this.resetPriceLists();
				}, error => {
					console.log(JSON.stringify(error));
				}
			);
		}else {
			this.promises.priceLists.saved.update = true;
			this.resetPriceLists();
		}

		if(willDelete.length > 0) {
			let total: number = willDelete.length;
			let index: number = 0;
			willDelete.forEach(priceList => {
				this.priceListService.flushCustomEndpoint();
				this.priceListService.delete(priceList.Id).subscribe(result => {
					index++;
					if(total == index) {
						this.promises.priceLists.saved.delete = true;
						this.resetPriceLists();
					}
				});
			});
		}else{
			this.promises.priceLists.saved.delete = true;
			this.resetPriceLists();
		}
	}

	private saveVariants(isNew: boolean = false){
		if(this.product) { 
			this.promises.priceLists.new.forEach( priceList => {
				if(priceList.Variants) priceList.Variants.forEach( variant => {
					variant.PriceListId = priceList.Id;
					variant.ProductId = this.product.Id;
					this.promises.variants.new.push(new Variant(variant));
				});
			});
		};
		let willUpdate: Variant[] = [];
		let willDelete: Variant[] = [].concat(this.promises.variants.old);
		let willCreate: Variant[] = [].concat(this.promises.variants.new);
		let sourceList: Variant[] = [].concat(this.promises.variants.new);

		let item: Variant;
		let matchedItem: Variant;
		let action: string;
		while(sourceList.length > 0) {
			item = sourceList.shift();
			matchedItem = willDelete.find( variant => item.Id == variant.Id);
			if(matchedItem) {
				willDelete.splice(willDelete.indexOf(matchedItem), 1);
				willCreate.splice(willCreate.indexOf(willCreate.find( item => item.Id == matchedItem.Id)), 1);
				willUpdate.push(item);
			}
		};

		// console.log("will delete : ", willDelete);
		// console.log("will update  : ", willUpdate);
		// console.log("will create : ", willCreate);

		if(willCreate.length > 0) {
			this.variantService.setCustomEndpoint('PostAll');
			this.variantService.create(willCreate).subscribe(
				response => {
					response.forEach( (item, index) => {
						willCreate[index].Id = item;
					});
					this.promises.variants.saved.create = true;
					this.resetVariants();
				},
				error => {
					console.log("error : ", error);
				}
			);
		}else{
			this.promises.variants.saved.create = true;
			this.resetVariants();
		}

		if(willUpdate.length > 0) {
			this.variantService.setCustomEndpoint('PutAll');
			this.variantService.update(willUpdate, 'put').subscribe(
				response => {
					this.promises.variants.saved.update = true;
					this.resetVariants();
				}, error => {
					console.log(JSON.stringify(error));
				}
			);
		}else {
			this.promises.variants.saved.update = true;
			this.resetVariants();
		}

		if(willDelete.length > 0) {
			let total: number = willDelete.length;
			let index: number = 0;
			willDelete.forEach(variant => {
				this.variantService.flushCustomEndpoint();
				this.variantService.delete(variant.Id).subscribe(result => {
					index++;
					if(total == index) {
						this.promises.variants.saved.delete = true;
						this.resetVariants();
					}
				});
			});
		}else{
			this.promises.variants.saved.delete = true;
			this.resetVariants();
		}
	}

	private saveVariantPrices(isNew: boolean = false){
		if(this.product) { 
			this.promises.variants.new.forEach( variant => {
				if(variant.Prices) variant.Prices.forEach( variantPrice => {
					variantPrice.VariantId = variant.Id;
					this.promises.variantPrices.new.push(new VariantPrice(variantPrice));
				});
			});
		};
		let willUpdate: VariantPrice[] = [];
		let willDelete: VariantPrice[] = [].concat(this.promises.variantPrices.old);
		let willCreate: VariantPrice[] = [].concat(this.promises.variantPrices.new);
		let sourceList: VariantPrice[] = [].concat(this.promises.variantPrices.new);

		let item: VariantPrice;
		let matchedItem: VariantPrice;
		let action: string;
		while(sourceList.length > 0) {
			item = sourceList.shift();
			matchedItem = willDelete.find( variantPrice => item.Id == variantPrice.Id);
			if(matchedItem) {
				willDelete.splice(willDelete.indexOf(matchedItem), 1);
				willCreate.splice(willCreate.indexOf(willCreate.find( item => item.Id == matchedItem.Id)), 1);
				willUpdate.push(item);
			}
		};

		// console.log("will delete : ", willDelete);
		// console.log("will update  : ", willUpdate);
		// console.log("will create : ", willCreate);

		if(willCreate.length > 0) {
			this.variantPriceService.setCustomEndpoint('PostAll');
			this.variantPriceService.create(willCreate).subscribe(
				response => {
					response.forEach( (item, index) => {
						willCreate[index].Id = item;
					});
					this.promises.variantPrices.saved.create = true;
					this.resetVariantPrices();
				},
				error => {
					console.log("error : ", error);
				}
			);
		}else{
			this.promises.variantPrices.saved.create = true;
			this.resetVariants();
		}

		if(willUpdate.length > 0) {
			this.variantPriceService.setCustomEndpoint('PutAll');
			this.variantPriceService.update(willUpdate, 'put').subscribe(
				response => {
					this.promises.variantPrices.saved.update = true;
					this.resetVariantPrices();
				}, error => {
					console.log(JSON.stringify(error));
				}
			);
		}else {
			this.promises.variantPrices.saved.update = true;
			this.resetVariantPrices();
		}

		if(willDelete.length > 0) {
			let total: number = willDelete.length;
			let index: number = 0;
			willDelete.forEach(variant => {
				this.variantPriceService.flushCustomEndpoint();
				this.variantPriceService.delete(variant.Id).subscribe(result => {
					index++;
					if(total == index) {
						this.promises.variantPrices.saved.delete = true;
						this.resetVariantPrices();
					}
				});
			});
		}else{
			this.promises.variantPrices.saved.delete = true;
			this.resetVariantPrices();
		}
	}

	private checkSaved(){
		if(this.product.IsBundle) {
			if(this.promises.product.saved.products) {
				this.promises.product.saved = {performanceProduct: false, products: false, venueSeats: false, priceLists: false, variants: false, variantPrices: false};
				this.notificationService.add({text: `<b>${this.title}</b> ürünü başarıyla kaydedildi.`, type:'success'});
				this.isPromising = false;
				if(this.onSaveComplete != null) {
					this.onSaveComplete();
				}else{
					this.router.navigate(['products', 'bundles']);
				}
			}
		}else{
			if(this.promises.product.saved.performanceProduct) {
				this.promises.product.saved = {performanceProduct: false, products: false, venueSeats: false, priceLists: false, variants: false, variantPrices: false};
				this.notificationService.add({text: `<b>${this.title}</b> ürünü başarıyla kaydedildi.`, type:'success'});
				this.isPromising = false;
				if(this.onSaveComplete != null) {
					this.onSaveComplete();
				}else{
					this.router.navigate(['products', 'performances']);
				}
			}
		}
		
	}

}
