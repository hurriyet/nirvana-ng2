import { Component, OnInit, HostBinding } from '@angular/core';
import * as moment from 'moment';

import { HeaderTitleService } from './../../services/header-title.service';
import { EntityService } from './../../services/entity.service';
import { TetherDialog } from './../../components/tether-dialog/tether-dialog';


@Component({
	selector: 'app-transactions',
	templateUrl: './transactions.component.html',
	styleUrls: ['./transactions.component.scss'],
	providers: [EntityService],
})
export class TransactionsComponent implements OnInit {
	// @HostBinding('class.or-transactions') true;

	subscription;
	errorMessage: any;

	transactions = [];
	count: number;
	private noDataInContent: boolean = false;
	private isLoading: boolean = false;

	selectedItems: Array<Object> = [];
	isAllSelected: boolean = false;

	private showPagination:boolean = true;
	pageSizes: Array<Object> = [{text: '10', value: 10}, {text: '20', value: 20}];
	pageSize: number = 10;
	private currentPage: number = 1;

	pills: Array<any> = [
		{text: 'BUGÜN', filter: this.getDateRangeFilter('today', 'PaymentDate'), isActive: false},
		{text: 'DÜN', filter: this.getDateRangeFilter('yesterday', 'PaymentDate'), isActive: false},
		{text: 'SON HAFTA', filter: this.getDateRangeFilter('lastweek', 'PaymentDate'), isActive: false},
		{text: 'SON AY', filter: this.getDateRangeFilter('lastmonth', 'PaymentDate'), isActive: false},
	];
	// sortParams: Array<any> = [
	// 	{text: 'ADA GÖRE', value: JSON.stringify({sortBy: "Id", type: "desc"})},
	// 	{text: 'BEDELE GÖRE', value: JSON.stringify({sortBy: "Amount", type: "desc"})}
	// ];

	constructor(
		private headerTitleService: HeaderTitleService,
		private entityService: EntityService,
		private tetherDialog: TetherDialog,
	) {
	}

	ngOnInit() {
		this.headerTitleService.setTitle('İşlemler');

		this.entityService.setCustomEndpoint('GetAll');
		this.subscription = this.entityService.queryParamSubject.subscribe(
			params => {
				this.isLoading = true;
				this.updateLocalParams(params);

				let query = this.entityService.fromEntity('TBasket')
				.expand(['Currency'])
				.expand(['SalesChannel'])
				.expand(['SalesSubChannel'])
				.take(params['pageSize'])
				.page(params['page']);

				let sort = params["sort"] ? (typeof params["sort"] == 'string'  ? JSON.parse(params["sort"]) : params["sort"]) : null;
				if(sort && sort[0]){
					query.orderBy(sort[0]["sortBy"], sort[0]["type"])
				}
				if(params["search"]){
					query.search(params["search"]["key"], params["search"]["value"]);
				}
				if (params['filter'] && params['filter'].length > 0) {
					query.whereRaw(params['filter'][0].filter);
				}

				query.executeQuery();
			},
			error => this.errorMessage = <any>error
		);

		this.entityService.data.subscribe(
			entities => {
				this.selectedItems = [];
				this.transactions = entities;

				this.noDataInContent = this.transactions.length == 0;
				this.isLoading = false;
			},
			error => this.errorMessage = <any>error
		);

		this.entityService.getCount().subscribe(
			count => this.count = count,
			error => this.errorMessage = <any>error
		);
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	private updateLocalParams(params: Object = {}) {
		this.currentPage = params['page'] ? params['page'] : 1
		this.pageSize = params['pageSize'] ? params['pageSize'] : 10
	}

	private getDateRangeFilter(filter: string, key: string) {
		let startDate = null;
		let endDate = moment().endOf('day');

		switch (filter) {
			case "today":
				startDate = moment().startOf('day');
				break;
			case "yesterday":
				startDate = moment().startOf('day').subtract(1, 'days');
				endDate = endDate.subtract(1, 'days');
				break;
			case "lastweek":
				startDate = moment().startOf('week');
				break;
			case "lastmonth":
				startDate = moment().startOf('month');
				break;
		}

		if (startDate && endDate) {
			return  `${key} gt ${startDate.toISOString()} and ${key} lt ${endDate.toISOString()}`
		}
		return ""
	}

	onInputChange(event) {
		this.entityService.setSearch({ key: 'RefId', value: event });
	}

	toggleSortTitle(sort) {
		if(sort){
			this.entityService.setOrder(sort, true);
		} else {
			this.entityService.flushOrder();
		}
	}

	pillFilter(pill) {
		this.entityService.setFilter(pill);
	}

	transistPage(page) {
		this.entityService.setPage(page);
	}

	changePageSize(pageSize) {
		this.entityService.setPageSize(pageSize);
	}

	selectAllItems(selectAll: boolean): void {
		if (selectAll && this.selectedItems.length < this.transactions.length) {
			this.selectedItems = [];
			this.transactions.forEach(item => {
				this.selectedItems.push(item);
			});
			this.isAllSelected = true;
		}
		if (!selectAll) {
			this.isAllSelected = false;
			this.selectedItems = [];
		}
	}

	selectItem(isSelected: boolean, transaction: any): void {
		if (isSelected) {
			this.selectedItems.push(transaction);
		} else {
			let selectedVenue = this.selectedItems.filter(item => {
				return (transaction === item);
			})[0];
			this.selectedItems.splice(this.selectedItems.indexOf(selectedVenue), 1);
		}
	}

	openContextMenu(e, transaction) {
		let content = {
			title: "İŞLEMLER",
			// iconSet: "",
			data: [
				{ label: 'Test', icon: 'edit', action: 'edit'},
			]
		}

		this.tetherDialog.context(content, {
			target: e.target,
			attachment: "top right",
			targetAttachment: "top right",
			targetOffset: '-13px 0px'
		}).then(result => {
			if (result) {
				switch (result['action']) {
					case "edit":
						break;
				}
			}
		}).catch(reason => console.log("dismiss reason : ", reason));
	}
}
