import { Component, OnInit, Output, HostBinding, Renderer } from '@angular/core';
import { HeaderTitleService } from '../../services/header-title.service';
import { Venue } from '../../models/venue';
import { VenueService } from '../../services/venue.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TetherDialog } from '../../components/tether-dialog/tether-dialog';
import { NotificationService } from '../../services/notification.service';


@Component({
    selector: 'app-venues',
    templateUrl: './venues.component.html',
    styleUrls: ['./venues.component.scss'],
    providers: [VenueService]
})
export class VenuesComponent implements OnInit {
    @HostBinding('class.or-venues') true;

    subscription;
    errorMessage: any;
    isListViewActive: boolean = false;
    isCardViewActive: boolean = true;

    venues: Venue[];
    count: number;
    private noDataInContent: boolean = false;
    private isLoading: boolean = false;

    selectedItems: Array<Object> = [];
    isAllSelected: boolean = false;

    private showPagination:boolean = true;
    pageSizes: Array<Object> = [{text: '10', value: 10}, {text: '20', value: 20}];
    pageSize: number = 10;
    private currentPage: number = 1;

    filter: Array<Object> = [];
    pills: Array<any> = [
        {text: 'AKTİF', filter: 'IsActive eq true', isActive: false},
        {text: 'PASİF', filter: 'IsActive eq false', isActive: false},
    ];
    sortParams: Array<any> = [
        {text: 'ADA GÖRE', value: JSON.stringify({sortBy: "Name", type: "desc"})},
        {text: 'ŞEHİRE GÖRE', value: JSON.stringify({sortBy: "VenueLocationInfo.CityName", type: "desc"})}
    ];

    // actionButtons: Array<Object> = [
    //     {label: 'Aktif Et', icon: 'visibility', action: 'visibilityOn'},
    //     {label: 'Durdur', icon: 'visibility_off', action: 'visibilityOff' },
    // ];

    constructor(
        private renderer: Renderer,
        private headerTitleService: HeaderTitleService,
        private venueService: VenueService,
        private router: Router,
        private route: ActivatedRoute,
        private tetherService: TetherDialog,
        private notificationService : NotificationService
    ) {
        this.venueService.setCustomEndpoint('GetVenueList');
        this.venueService.setFilter(this.pills[0], false);
    }

    ngOnInit(): void {
        this.headerTitleService.setTitle('Mekanlar');

        this.subscription = this.venueService.queryParamSubject.subscribe(
            params => {
                this.isLoading = true;
                this.updateLocalParams(params);
                this.venueService.gotoPage(params);
            },
            error => this.errorMessage = <any>error
        );

        this.venueService.data.subscribe(
            venues => {
                this.selectedItems = [];
                this.venues = venues;
                this.isLoading = false;

               if(this.venues.length == 0) {
                   this.noDataInContent = true;
                } else {
                    this.noDataInContent = false;
                }
            },
            error => this.errorMessage = <any>error
        );

        this.venueService.getCount().subscribe(
            count => {
                this.count = count;
                if(count <= this.pageSize) {
                     this.showPagination = false;
                } else {
                    this.showPagination = true;
                }
            },
            error => this.errorMessage = <any>error
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    private updateLocalParams(params: Object = {}) {
        this.currentPage = params['page'] ? params['page'] : 1
        this.pageSize = params['pageSize'] ? params['pageSize'] : 10
    }

    onResize(event) { }

    ngAfterViewInit()  {}

    find() {}

    transistPage(page) {
        this.venueService.setPage(page);
    }

    toggleSortTitle(sort) {
        this.venueService.setOrder(sort);
    }

    changePageSize(pageSize) {
        this.venueService.setPageSize(pageSize);
    }

    onInputChange(event) {
        this.venueService.setSearch({ key: 'Name', value: event });
    }

    sortVenues(sort) {
        this.venueService.setOrder(sort);
    }

    filterVenues(pill) {
        this.venueService.setFilter(pill);
    }

    selectAllItems(selectAll: boolean): void {
        if (selectAll && this.selectedItems.length < this.venues.length) {
            this.selectedItems = [];
            this.venues.forEach(item => {
                this.selectedItems.push(item);
            });
            this.isAllSelected = true;
        }
        if (!selectAll) {
            this.isAllSelected = false;
            this.selectedItems = [];
        }
    }

    selectItem(isSelected: boolean, venue: Venue): void {
        if (isSelected) {
            this.selectedItems.push(venue);
        } else {
            let selectedVenue = this.selectedItems.filter(item => {
                return (venue === item);
            })[0];
            this.selectedItems.splice(this.selectedItems.indexOf(selectedVenue), 1);
        }
    }

    get isMultiSelectionActive(): boolean {
        return this.selectedItems.length > 0;
    }

    getRawData() {
        console.log('raw', this.venueService.getRawData());
    }

    callSelectedItemsAction(action: string) {
        console.log(action);
    }

    changeView(viewType) {
        this.venueService.setActiveViewType(viewType);
    }

    goToLink(link){
        this.router.navigate(['/'+link]);
    }

    cardActionHandler(event) {
        switch(event.target) {
            case "select":
                this.selectItem(event.action, event.data.model);
                break;
            case "context":
                let actionResult = event.action;
                if(actionResult) {
                    let venueId = event.action.parameters.venueId;
                    switch (actionResult['action']) {
                        case "editVenue":
                            if (actionResult['parameters'] && actionResult['parameters']['venueId']) {
                                this.router.navigate(['/venue', actionResult['parameters']['venueId'], 'edit']);
                            }
                            break;
                        case 'activate':
                            this.isLoading = true;
                            this.venueService.flushCustomEndpoint();
                            let activateTemplate = this.venueService.update({Id:venueId, IsActive:true}, 'patch');
                            activateTemplate.subscribe(save => {
                                // let venue = this.venues.find(temp => {
                                //     return (temp.Id == venueId)
                                // });
                                // venue.IsActive = true;
                                // this.isLoading = false;
                                this.venueService.setCustomEndpoint('GetVenueList')
                                this.venueService.reload();
                                this.notificationService.add({type:'success', text:'Mekan aktif hale getirildi.'});
                            });
                            break;
                        case 'deActivate':
                            this.isLoading = true;
                            this.venueService.flushCustomEndpoint();
                            let deActivateTemplate = this.venueService.update({Id:venueId, IsActive:false}, 'patch');
                            deActivateTemplate.subscribe(save => {
                                // let venue = this.venues.find(temp => {
                                //     return (temp.Id == venueId)
                                // });
                                // venue.IsActive = false;
                                // this.isLoading = false;
                                this.venueService.setCustomEndpoint('GetVenueList')
                                this.venueService.reload();
                                this.notificationService.add({type:'success', text:'Mekan pasif hale getirildi.'});
                            });
                            break;
                    }
                }
                break;
            case "goto":
                this.router.navigate([`/${event.data.entryType}`,event.data.model.Id,'performances']);
                break;
        }
    }

    openContext(e,venue){
        this.tetherService.context({
            title: "İŞLEMLER",
            data: [
                { label: 'Düzenle', icon: 'edit', action: 'edit' },
                { label: venue.IsActive == true ? 'Durdur' : 'Aktif Et', icon: venue.IsActive == true ? 'visibility_off' : 'visibility', action: venue.IsActive == true ? 'deactive' : 'active' }
                /*{ label: 'Göster', icon: 'visibility_off', action: 'visibilityOff' },
                { label: 'Arşivle', icon: 'archive', action: 'archive' },
                { label: 'Sil', icon: 'delete', action: 'delete' },*/
            ]
        }, {target: e.target, attachment: "top right", targetAttachment: "top right",}).then(result => {
            let venueId = venue.Id;
            switch(result['action']) {
                case "edit":
                    this.router.navigate(['/venue', venue.Id, 'edit']);
                    break;
                case "deactive":
                    this.isLoading = true;
                    this.venueService.flushCustomEndpoint();
                    let deActivateTemplate = this.venueService.update({Id:venueId, IsActive:false}, 'patch');
                    deActivateTemplate.subscribe(save => {
                        let venue = this.venues.find(temp => {
                            return (temp.Id == venueId)
                        });
                        venue.IsActive = false;

                        this.isLoading = false;
                        this.notificationService.add({type:'success', text:'Mekan dekatif hale getirildi.'});
                    });
                    break;
                case "active":
                    this.isLoading = true;
                    this.venueService.flushCustomEndpoint();
                    let activateTemplate = this.venueService.update({Id:venueId, IsActive:true}, 'patch');
                    activateTemplate.subscribe(save => {
                        let venue = this.venues.find(temp => {
                            return (temp.Id == venueId)
                        });
                        venue.IsActive = true;

                        this.isLoading = false;
                        this.notificationService.add({type:'success', text:'Mekan aktif hale getirildi.'});
                    });
                    break;
            }
        }).catch(reason=>{});
    }
}
