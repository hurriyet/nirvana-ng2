import { GridListComponent } from './../../components/grid-list/grid-list.component';
import { AfterContentInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild, EventEmitter, Output,ContentChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { AuthenticationService } from './../../services/authentication.service';
import { Router  } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit,AfterContentInit,OnDestroy {
	@ContentChild(GridListComponent) grids: GridListComponent[];
	isSideMenuCollapsed:boolean;
	@Output() sendEvent:EventEmitter<any> = new EventEmitter();
    parentSubject:Subject<any> = new Subject();
	toggleSideMenu(isSideMenuCollapsed){
		this.isSideMenuCollapsed = isSideMenuCollapsed;
	}
  	constructor(private authenticationService : AuthenticationService, private router: Router) {
  		if(!authenticationService.getToken()){
  			this.router.navigate(['login']);
  		}
  	}

  	ngOnInit() {

  	}

    ngOnDestroy() {

	}
	ngAfterContentInit() {

	}


	eventBubbling(){
		return "Bubling";
	}


}
