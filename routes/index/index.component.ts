import { RelativeDatePipe } from './../../pipes/relative-date.pipe';
import { Component, OnInit, ComponentFactory, ComponentFactoryResolver, Injector, Input, ChangeDetectorRef,ComponentRef } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { TetherDialog } from '../../components/tether-dialog/tether-dialog';
import { RelatedContentComponent } from '../../common/related-content/related-content.component';
import { FeedbackFormComponent } from '../../common/feedback-form/feedback-form.component';
import { TranslateService } from '../../services/translate.service';
import { NotificationService } from '../../services/notification.service';
import { EntityService } from '../../services/entity.service';

@Component({
  	selector: 'app-index',
  	templateUrl: './index.component.html',
  	styleUrls: ['./index.component.scss'],
  	entryComponents: [ RelatedContentComponent ],
  	providers: [EntityService]
})
export class IndexComponent implements OnInit {


  	constructor(
        private route: ActivatedRoute,
        private router: Router,
        private resolver: ComponentFactoryResolver,
        private injector: Injector,
        private tetherService: TetherDialog,
        private translateService : TranslateService,
		private changeDetector: ChangeDetectorRef,
		private notificationService : NotificationService,
		private entityService:EntityService
    ) {
  		// notificationService.add({type:'success'});
  		// setTimeout(function(){
  		// 	notificationService.add({type:'warning'});
  		// }, 1000)
    }

	ngOnInit() {
		this.showContentOverlay();
  	}

	log(value){
		console.log(value);
	}

	showContentOverlay() {
		//this.tetherService.content()
	}

	feedback(){
		this.tetherService.confirm({
			title: 'Etkinliği iptal etmek istiyor musunuz?',
			description:'<b>DİKKAT!</b> Bu işlem geri alınamaz.',
			feedback: {label: 'İPTAL NEDENİ'},
			confirmButton: {label: 'İPTAL ET'},
			dismissButton: {label: 'VAZGEÇ'}
			}).then( result => {
				console.log(result);
			}).catch( reason => {
				console.log(reason);
			})
	}

	confirm(){
		this.tetherService.confirm({
			title: 'Etkinliği Sil?',
			description:'<b>DİKKAT!</b> Silmek istediğinizden emin misiiniz?',
			confirmButton: {label: 'SİL'},
			dismissButton: {label: 'VAZGEÇ'}
			}).then( result => {
				console.log(result);
			}).catch( reason => {
				console.log(reason);
			})
	}

	eventHandler(event){
		console.log("breadcrumb Event",event);
	}

	alert(){
		this.tetherService.confirm({
			title: 'Etkinlik silindi'}).then( result => {
				console.log(result);
			}).catch( reason => {
				console.log(reason);
			})
	}

	openContext(e){
		this.tetherService.context({
			title: "İŞLEMLER",
			data: [
				{ label: 'Kopyala', icon: 'layers', action: 'copy' },
				{ label: 'Gizle', icon: 'visibility', action: 'visibilityOn' },
				{ label: 'Göster', icon: 'visibility_off', action: 'visibilityOff' },
				{ label: 'Arşivle', icon: 'archive', action: 'archive' },
				{ label: 'Sil', icon: 'delete', action: 'delete' },
			]
		}, {target: e.target, attachment: "top right", targetAttachment: "top right",}).then(result => {
			console.log(result);
			switch(result['action']) {
				case "copy":
					this.tetherService.confirm({title: result["label"]}).then(res=>{}).catch(err=>{});
				break;
				case "delete":
					this.tetherService.confirm({title: result["label"], description:"<b>DİKKAT!</b> Silmek istediğinize emin misiniz?", confirmButton:{label: "SİL"}, dismissButton:{label: "VAZGEÇ"}}).then(res=>{}).catch(err=>{});
				break;
			}
		}).catch(reason=>{});
	}
}