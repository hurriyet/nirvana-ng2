import { Component, OnInit, HostBinding } from '@angular/core';
import { PerformanceService } from '../../../services/performance.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EventService } from '../../../services/event.service';
import { NotificationService } from '../../../services/notification.service';

@Component({
	selector: 'app-event-performances',
	templateUrl: './event-performances.component.html',
	styleUrls: ['./event-performances.component.scss'],
	providers:[PerformanceService, EventService]
})
export class EventPerformancesComponent implements OnInit {
	@HostBinding('class.or-event-performances') true;

	subscription;
	errorMessage: any;

	performances;
	count: number;

	pageID: number;
	private showPagination: boolean = true;
	pageSize: number = 10;
	private currentPage: number = 1;

	private isLoading: boolean = false;
	private noDataInContent: boolean = false;

	private flags: {PublishDateFieldOn: boolean} = {
		PublishDateFieldOn: false
	};
	private newPublishDate: string;
	private event;

	constructor(
		private performanceService: PerformanceService,
		private route: ActivatedRoute,
		private router: Router,
		private eventService: EventService,
		private notificationService: NotificationService
	) {
	}

	ngOnInit() {
		this.subscription = this.route.parent.params.subscribe(params => {
			let param = +params['id'];
			this.pageID = param;
			this.noDataInContent = false;
			this.isLoading = true;

			this.performanceService.setCustomEndpoint('GetPerformanceList');
			this.performanceService.setQueryParams({page: this.currentPage, pageSize: this.pageSize, protectedFilter: `EventId eq ${param}`});
			// this.performanceService.query({page: this.currentPage, pageSize: this.pageSize, filter: [{filter: `EventId eq ${param}`}]});
			this.eventService.find(this.pageID, true);
		});

		this.subscription = this.performanceService.queryParamSubject.subscribe(
			params => {
				this.noDataInContent = false;
				this.isLoading = true;
				this.updateLocalParams(params);
				this.performanceService.gotoPage(params);
			},
			error => this.errorMessage = <any>error
		);

		this.performanceService.data.subscribe(res => {
			this.performances = res;
			this.isLoading = false;
			if(this.performances.length == 0) {
				this.noDataInContent = true;
			} else {
				this.noDataInContent = false;
			}
		});

		this.performanceService.count.subscribe(res => {
			this.count = res;
			if((Math.ceil(this.count / this.pageSize)) <= 1 ) {
				this.showPagination  = false;
			} else {
				this.showPagination = true;
			}
		});

		this.eventService.data.subscribe(event => {
			if(event &&  event[0]){
				this.event = event[0];
				this.flags.PublishDateFieldOn = this.event.PublishDate != null;
			}
		});
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	private updateLocalParams(params: Object = {}) {
	    this.currentPage = params['page'] ? params['page'] : 0
	    this.pageSize = params['pageSize'] ? params['pageSize'] : 10
	}

	transistPage(page){
		this.isLoading = true;
		this.performanceService.setPage(page);
	}

	cardActionHandler(event) {
		switch(event.target) {
			case "select":
				// this.selectItem(event.action, event.data.model);
				break;
			case "context":
				let actionResult = event['action'];
				let actionData = event['data'];
				if(!actionResult || !actionData) { break; }

				switch(actionResult['action']) {
					case "edit":
						this.router.navigate([`/${actionData.entryType}`, actionData.model.Id, 'edit']);
						break;
					case "visibilityOn":
					case "visibilityOff":
					case "archive":
					case "delete":
						this.performanceService.callItemAction(actionData.model, actionResult['action']);
						break;
				}
				break;
		}
	}

	private checkHandler(value, name:string, target: string = "event") {
		switch(name) {
			case 'PublishDateFieldOn':
				this.flags.PublishDateFieldOn = value;
				if(!value) {
					this.newPublishDate = null;
					this.savePublishDate();
				}
			break;
		}
	}

	private dateChangeHandler(value, name){
		switch(name) {
			case 'PublishDate':
				this.newPublishDate = value;
			break;
		}
	}

	private savePublishDate(){
		if(this.newPublishDate != this.event.PublishDate){
			this.event.set('PublishDate', this.newPublishDate);
			let save = this.eventService.update({Id: this.event.Id, 'PublishDate' : this.newPublishDate});
			save.subscribe(result=>{
				this.notificationService.add({text: 'Yayınlanma tarihi güncellendi', type:'success'});
			})
		}
	}
}
