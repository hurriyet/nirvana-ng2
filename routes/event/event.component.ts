import { HeaderTitleService } from './../../services/header-title.service';
import { Component, OnInit, ViewChild, HostBinding, Renderer } from '@angular/core';
import { TitleSwitcherComponent } from '../../components/title-switcher/title-switcher.component';
import { Observable } from 'rxjs/Observable';
import { EventService } from '../../services/event.service';
import { VenueService } from '../../services/venue.service';
import { PerformanceService } from '../../services/performance.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Event } from '../../models/event';

@Component({
    selector: 'app-event',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.scss'],
    providers: [EventService, VenueService, PerformanceService]
})
export class EventComponent implements OnInit {
    @ViewChild(TitleSwitcherComponent) titleSwitcher: TitleSwitcherComponent;
    @HostBinding('class.or-event') true;

    tabs: Array<any> = [
        { label: 'ÜRÜNLER', routerLink: 'products' },
    ];

    subscription;
    event: Event;
    performance;
    venues: Array<any> = [];
    eventVenues: Array<any> = [];
    eventVenuesDic: {} = {};

    constructor(
        private router: Router,
        private eventService: EventService,
        private route: ActivatedRoute,
        private venueService: VenueService,
        private performanceService: PerformanceService,
        private headerTitleService: HeaderTitleService,
        private renderer: Renderer
    ) { }

    ngOnInit() {
        this.headerTitleService.setTitle('Etkinlikler');

        this.subscription = this.route.params.subscribe(params => {
            let param = +params['id'];
            this.eventService.setCustomEndpoint('GetEventList');
            this.eventService.query({ pageSize: 1, filter: [{ filter: `Id eq ${param}` }] })
        });

        this.eventService.data.subscribe(res => {
            if (this.titleSwitcher && this.titleSwitcher.finder) {
                res.forEach( eventData => {
                    let result: {}[] = [];
                        result.push({
                            id: eventData.Id,
                            title: eventData.Name,
                            icon: "audiotrack",
                            params: {event: eventData}
                        })

                        this.titleSwitcher.finderSearchResults = Observable.of([{
                            title: "ARAMA SONUÇLARI",
                            list: result
                        }]);
                });
            } else if (res && res.length > 0) {
                this.event = res[0];

                if(this.event.ChildEventCount > 0) {
                    this.tabs = [
                        {label: 'ETKİNLİKLER', routerLink: 'events' },
                        { label: 'ÜRÜNLER', routerLink: 'products' },
                    ];
                    this.router.navigate(["event", this.event.Id, 'events']);

                } else if (this.event.Performances && this.event.Performances.length > 0) {
                    this.tabs = [
                        {label: 'PERFORMANSLAR', routerLink: 'performances' },
                        { label: 'ÜRÜNLER', routerLink: 'products' },
                    ];
                    this.router.navigate(["event", this.event.Id, 'performances']);

                    this.event.Performances.forEach(performance => {
                        if(!this.performance || this.performance.Date > performance.Date) {
                            this.performance = performance;
                        }
                        if(performance.Venue){
                            if(!this.eventVenuesDic[performance.Venue.Id]) {
                                this.eventVenuesDic[performance.Venue.Id] = performance.Venue;
                                this.eventVenues.push(performance.Venue);
                            }
                        }
                    });

                    if (this.eventVenues.length === 1) {
                        this.venueService.find(this.eventVenues[0].Id, true);
                        this.venueService.data.subscribe(res => {
                            if (res[0]) {
                                this.venues.push(res[0]);
                            }
                        });
                    } else if (this.eventVenues.length > 1) {
                        this.venues = this.eventVenues;
                    }
                    let self = this;
                    
                }
            }
        });
    }

    checkedHandler($event) {
        console.log($event);
    }

    titleSearchHandler(value) {
        this.eventService.setCustomEndpoint('GetEventList');
        if(value && value.length > 0) this.eventService.query({ page: 0, pageSize: 10, search: {key:'Name', value:value}});
    }

    titleChangeHandler(event) {
        this.router.navigate(["event", event.id]);
    }
}
