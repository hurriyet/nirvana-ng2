import { HeaderTitleService } from './../../services/header-title.service';
import { Component, OnInit, ComponentFactoryResolver, ViewChild, HostBinding } from '@angular/core';
import * as moment from 'moment';

import { TitleSwitcherComponent } from '../../components/title-switcher/title-switcher.component';
import { Observable } from 'rxjs/Observable';
import { VenueService } from '../../services/venue.service';
import { EventService } from '../../services/event.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Venue } from '../../models/venue';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Component({
    selector: 'app-venue',
    templateUrl: './venue.component.html',
    styleUrls: ['./venue.component.scss'],
    providers: [VenueService, EventService]
})
export class VenueComponent implements OnInit {
    @HostBinding('class.or-venue') true;
    @ViewChild(TitleSwitcherComponent) contentTitle: TitleSwitcherComponent;

    venue: Venue;
    venueStatistics: Object;
    param: any;
    subscription;
    public contentTitleData: Object = {};

    mon = 1;
    nextMon = 8;
    thisMonday = moment().day(this.mon).toISOString();
    nextMonday = moment().day(this.nextMon).toISOString();
    now = moment().toISOString();
    isoWeek = moment().isoWeek();
    thisWeek = moment().isoWeek();
    narrowIndexTitle: string = "Bu Hafta";
    sidebarEvents: any;
    sidebarIsLoading: boolean = false;
    sidebarIsNoDataContent: boolean = false;

    tabs: Array<any> = [{ label: 'ETKİNLİK GÖRÜNÜMÜ', routerLink: 'events' }, { label: 'OTURMA DÜZENLERİ', routerLink: 'layouts' }];
    statisticLabels: Object = { EventCount: 'Etkinlik', PerformanceCount: 'Performans', TemplateCount: 'Yerleşim' };

    constructor(
        private venueService: VenueService,
        private venueSearchService: VenueService,
        private route: ActivatedRoute,
        private router: Router,
        private eventService: EventService,
        private headerTitleService: HeaderTitleService
    ) { }

    ngOnInit() {
        this.headerTitleService.setTitle('Mekanlar');
        this.venueService.setCustomEndpoint('GetVenue');

        this.subscription = this.route.params.subscribe(params => {
            this.param = +params['id'];
            this.venueService.query({}, [{ key: 'venueId', value: this.param }], true);
        });

        this.venueService.data.subscribe(res => {
            if (res[0]) {
                this.venue = res[0];
                let statistics = [],
                    keys = Object.getOwnPropertyNames(this.statisticLabels);
                for (let key of keys) {
                    statistics.push({ label: this.statisticLabels[key], value: this.venue[key] });
                }
                this.venueStatistics = statistics;
            }
        });

        let self = this;
        this.venueSearchService.data.subscribe(response => {
            if (response.length > 0 && self.contentTitle) {
                let result = [];
                for (let venue of response) {
                    result.push({
                        id: venue.Id,
                        icon: "venue",
                        title: venue.Localization.Tr.Name,
                        description: venue.Localization.Tr.Description
                    });
                }
                if (self.contentTitle) {
                    let searchData = [
                        {
                            title: "ARAMA SONUÇLARI",
                            list: result
                        }];
                    self.contentTitle.finderSearchResults = Observable.of(searchData);
                }

            } else {
                if (self.contentTitle) {
                    self.contentTitle.finderSearchResults = Observable.of([]);
                }
            }
        });

        // --------------- sidebar events ----------------------------------------------------------
        this.eventService.setCustomEndpoint('GetEventList');
        this.eventService.query({ pageSize: 10, filter: [{ filter: `BeginDate gt ${this.thisMonday} and BeginDate lt ${this.nextMonday}` }] }, [{ key: 'VenueId', value: this.param }]);
        this.eventService.data.subscribe(res => {
            this.sidebarEvents = res;
            this.sidebarIsLoading = false;
            this.sidebarIsNoDataContent = this.sidebarEvents.length == 0;
        });
        // -----------------------------------------------------------------------------------------
    }

    onContentTitleSearchHandler(value) {
        this.venueSearchService.query({ page: 0, pageSize: 10, search: [{ key: 'Localization.Tr.Name', value: value }] });
    }

    onContentTitleChangedHandler(result) {
        this.router.navigate(['/venue', result.id]);
    }

    fetchWeekly(arg: boolean) {
        if (this.sidebarIsLoading) {return;}
        this.sidebarIsLoading = true;

        if (arg) {
            // go to next week
            this.isoWeek = this.isoWeek + 1;
            this.mon = this.mon + 7;
            this.nextMon = this.nextMon + 7;
            this.thisMonday = moment().day(this.mon).toISOString();
            this.nextMonday = moment().day(this.nextMon).toISOString();
        } else {
            // go to previous week
            this.isoWeek = this.isoWeek - 1;
            this.mon = this.mon - 7;
            this.nextMon = this.nextMon - 7;
            this.thisMonday = moment().day(this.mon).toISOString();
            this.nextMonday = moment().day(this.nextMon).toISOString();
        }
        if (this.isoWeek < 1) this.isoWeek = this.isoWeek + 52;
        if (this.isoWeek > 52) this.isoWeek = this.isoWeek - 52;
        this.narrowIndexTitle = this.isoWeek.toString() + ". Hafta";
        if (this.thisWeek == this.isoWeek) this.narrowIndexTitle = "Bu Hafta"
        if (this.thisWeek - this.isoWeek == -1) this.narrowIndexTitle = "Gelecek Hafta";
        if (this.thisWeek - this.isoWeek == 1) this.narrowIndexTitle = "Geçen Hafta";

        this.eventService.query({
            pageSize: 10,
            filter: [{ filter: `BeginDate gt ${this.thisMonday} and BeginDate lt ${this.nextMonday}` }]
        }, [{ key: 'VenueId', value: this.param }]);
    }
}
