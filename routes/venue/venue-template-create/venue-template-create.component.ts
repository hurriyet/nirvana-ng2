import { NotificationService } from './../../../services/notification.service';
import { VenueTemplateEditorComponent } from './../../../components/venue-template-editor/venue-template-editor.component';
import { Component, OnInit,HostBinding, AfterViewInit , ElementRef, ViewChild, Renderer, Inject} from '@angular/core';
import { AuthenticationService } from './../../../services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TemplateService } from './../../../services/template.service';
import { PerformanceService } from './../../../services/performance.service';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-venue-template-create',
  templateUrl: './venue-template-create.component.html',
  styleUrls: ['./venue-template-create.component.scss'],
  providers: [
	  	{ provide: 'deleteTemplateByPerformance', useClass: TemplateService }, 
		{ provide: 'copyVenueTemplateService', useClass: PerformanceService }, 
	  	TemplateService, PerformanceService]
})
export class VenueTemplateCreateComponent implements OnInit, AfterViewInit {
	@ViewChild('venued') venued: ElementRef;
	@ViewChild(VenueTemplateEditorComponent) venueEditor: VenueTemplateEditorComponent;
  	
	@HostBinding('class.or-venue-template-create') true;

  	private editorIsShown: boolean = false;
	private editorRole: string;
	private venueId;
	private performanceId : any;
	private venueTemplateId: any;
	private templateName : string;
	private isLoading: boolean = false;
	private isPromising: boolean = false;
	private isEdit : boolean  = false;
	private template;

	private validation: {
		TemplateName: { isValid: any, message: string },
	} = {
		TemplateName: {
			message: "Oturma düzeni adı zorunludur.",
			isValid(): boolean {
				return this.templateName && this.templateName.length > 0;
				//return this.template && this.template.Localization && this.template.Localization.Tr && this.template.Localization.Tr.Name && this.template.Localization.Tr.Name.length > 0
			}
		},
	};

	private get isValid():boolean {
		if( this.validation
			&& this.validation.TemplateName.isValid.call(this)
			){
			//console.log(this.template, this.validation.TemplateName.isValid.call(this));
			return true;
		}else{
			//if(this.validation && this.template) console.log(this.template, this.validation.WebUrl.isValid.call(this));
			return false
		}
	};
	
	constructor(
		private elementRef:ElementRef,
		private authenticationService : AuthenticationService,
		private renderer: Renderer,
		private router: Router,
		private route: ActivatedRoute,
		private templateService : TemplateService,
		private performanceService : PerformanceService,
		@Inject('deleteTemplateByPerformance') private deleteTemplateByPerformance: TemplateService,
		@Inject('copyVenueTemplateService') private copyVenueTemplateService: PerformanceService,
		private notificationService: NotificationService
		) {
			this.route.params.subscribe(params => {
			if(params){
				this.venueId = +params["id"];
				this.route.queryParams.subscribe(queryParams => {
					this.performanceId = queryParams["performanceId"];
					this.venueTemplateId = queryParams["venueTemplateId"];
				})
			}
		})
 	 }

	ngOnInit() {
		let sub = null;
		this.isLoading = true;
		if(this.venueTemplateId && this.performanceId){
			this.editorRole = VenueTemplateEditorComponent.ROLE_PERFORMANCE;
			this.deleteTemplateByPerformance.setCustomEndpoint("DeleteTemplateByPerformance");
			console.log("========> DeleteTemplateByPerformance() <========")
			this.deleteTemplateByPerformance.delete({"performanceId": this.performanceId}).subscribe(result => {
				console.log("========> CopyVenueTemplate() <========")
				this.copyVenueTemplateService.setCustomEndpoint("CopyVenueTemplate");
				this.copyVenueTemplateService.create({"PerformanceId": this.performanceId,"VenueTemplateId": this.venueTemplateId}).subscribe(result => {
					this.editorIsShown = true;
					console.log("Template Kopyalandı : ", result);
				}, error => {
					this.notificationService.add({type: "danger", text: "Template kopyalanamadı : " + error});
				});
			}, error => {
				this.notificationService.add({type: "danger", text: "Eski template silinemedi : " + error});
			});
			// this.performanceService.setCustomEndpoint('CopyVenueTemplate');
			// sub = this.performanceService.create({"PerformanceId": this.performanceId,"VenueTemplateId": this.venueTemplateId})
		}else if(this.venueTemplateId && !this.performanceId){
			this.editorRole = VenueTemplateEditorComponent.ROLE_VENUE;
			this.isEdit = true;
			this.templateService.flushCustomEndpoint();
			this.templateService.find(this.venueTemplateId, true);
			this.templateService.data.subscribe(template => {
				if(template != undefined && template.length == 1 && template[0]){
					this.template = template[0];
					this.venueTemplateId = this.template["Id"];
					this.titleChangeHandler(this.template.Localization.Tr.Name);
					this.editorIsShown = true;
					//this.renderer.setElementAttribute(this.venued.nativeElement , 'data-layout-id', this.template["Id"]);
				}
			});
		}else {
			this.editorRole = VenueTemplateEditorComponent.ROLE_VENUE;
			this.editorIsShown = true;
			// this.templateService.flushCustomEndpoint();
			// sub = this.templateService.create({VenueId:this.venueId, IsStanding: false});
		}

		if(sub){
			sub.subscribe(templateId => {
				if(templateId){
					this.venueTemplateId = templateId;
				}
				this.editorIsShown = true;
			});
		};
	}

	ngAfterViewInit(){

	}

	venueEditorEventHandler(event) {
		switch (event.type) {
			case VenueTemplateEditorComponent.EVENT_READY:
				this.isLoading = false;
			break;
			case VenueTemplateEditorComponent.EVENT_SAVE_SUCCESS:
				this.isPromising = false;
				if(this.performanceId) {
					this.router.navigate(['performance', this.performanceId, 'edit']);
				}else{
					this.router.navigate(['venue', this.venueId, 'layouts']);
				}
				this.notificationService.add({type: "success", text: "Kayıt işlemi başarıyla gerçekleştirildi."});
			break;
			case VenueTemplateEditorComponent.EVENT_SAVE_FAIL:
				this.isPromising = false;
				this.notificationService.add({type: "danger", text: "Kayıt işlemi gerçekleştirilemedi!"});
			break;
		}
	}

	private titleChangeHandler(value) {
		this.templateName = value;
		if(!this.template) return;
		if(!this.template.Localization) this.template.Localization = {};
		if(!this.template.Localization.Tr) this.template.Localization.Tr = {};
		if(!this.template.Localization.En) this.template.Localization.En = {};
		this.template.Localization.Tr.Name = value;
		this.template.Localization.Tr.ShortName = value;
		this.template.Localization.En.Name = value;
		this.template.Localization.En.ShortName = value;
	}

	saveTemplate(){
		this.isPromising = true;
		this.venueEditor.save({name: this.templateName});
	}

	exit() {
		if(this.venueId) this.router.navigate(['venue', this.venueId, "layouts"]);
	}

}




