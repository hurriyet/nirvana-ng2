import { Injectable } from '@angular/core';
import { Response, Http } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BaseDataService } from "../classes/base-data-service";
import { StoreService } from './../services/store.service';
import { AuthenticationService } from './authentication.service';
import { NotificationService } from './notification.service';
import { environment } from '../../environments/environment';
@Injectable()
export class ShoppingCartService extends  BaseDataService {
	count : BehaviorSubject<number> =  new BehaviorSubject(0);
	data : BehaviorSubject<Object[]> =  new BehaviorSubject([]);
	queryParams : Object = {filter: [], sort : [], pageSize: 20, page : 1};
	queryParamSubject : BehaviorSubject<Object> =  new BehaviorSubject(this.queryParams);
	cartItems : {id: number, type:string, title:string, date:any, location:string, products: Object[]}[] = [];
	cartItemsSubject :BehaviorSubject<Object[]> =  new BehaviorSubject([]);
	cartSubTotalSubject :BehaviorSubject<{cartTotal:number,cartCurrency:string}> =  new BehaviorSubject({cartTotal:0,cartCurrency:'TL'});
	cartUser : Object;
	constructor(http : Http, storeService : StoreService, authenticationService : AuthenticationService, private notificationService : NotificationService){
		super(http, 'ShoppingCart', storeService, authenticationService);
		this.baseUrl = environment.api.boxoffice + '/' + environment.api.path;
		this.createNewCart();
		this.data.subscribe(cart => {
			if(cart && !cart['State']){
				cart.forEach(event => {
					if(event['Performances'] && event['Performances'].length > 0){
						let cartItem  = {
							id: event['Id'],
							type: 'Etkinlik',
							title: event['Name'],
							date: event['SalesBeginDate'],
							location:event['Venue'],
							products: []
						};
						event['Performances'].forEach(performance => {
							cartItem.date = performance['DateTime'];
							performance['Products'].forEach(product => {
								product['Variants'].forEach(variant => {
									cartItem.products.push({
										id: variant['Id'],
										title: variant['Name'],
										price: { amount: variant['Price']['Amount'], currency: variant['Price']['Currency'] },
										count: variant['CountOfProductsSelected']
									})
								})
							})
						})
						this.cartItems.push(cartItem);
					}
				})
			}
			this.cartItemsSubject.next(this.cartItems);
		});
	}
	createNewCart(){
		this.setCustomEndpoint('CreateShoppingCart', true);
  		let newShoppingCart = this.create();
  		newShoppingCart.subscribe(item => {
  			this.removeCartUser();
			console.log('New Shopping Cart has been crated.', item);
		},error=>{
			this.notificationService.add({text:'Alışveriş sepeti oluşturulamadı.', type:'warning'});
		});
	}
	getData() : BehaviorSubject<Object[]>{
		return this.data;
	}
	map(response: Response): any[] {
        let responseObjects = response.json();
        if (responseObjects) {
            this.data.next(responseObjects);
            return responseObjects;
        }
    }
	gotoPage(params : Object){
		let page = params["page"] || 0,
			sort = params["sort"] ? (typeof params["sort"] == 'string'  ? JSON.parse(params["sort"]) : params["sort"]) : null,
			filter = params["filter"] || null,
			pageSize = params["pageSize"] || 20;
		this.query({pageSize:pageSize, page:page,sort:sort, filter:filter});
	}
	addToCart(item){
		this.setCustomEndpoint('SetProductAndCount', true);
		let model = {
		  "Selections": [
		    {
		      "VariantId": item.variant.Id,
		      "Count": item.count
		    }
		  ]
		};
		let save = this.save(model);
		save.subscribe(result => {
			if(result){
				this.getCartSummary();
			}
		},
		error => {
			if(error['Message']){
	        	this.notificationService.add({text:error['Message'], type:'warning'})
	    	}
	    });
	}
	isItemInCart(item){
	}
	updateItem(item){

	}
	removeFromCart(variantId){
		this.setCustomEndpoint('SetProductAndCount', true);
		let model = {
		  "Selections": [
		    {
		      "VariantId": variantId,
		      "Count": 0
		    }
		  ]
		};
		let save = this.save(model);
		save.subscribe(result => {
			this.getCartSummary();
		},error => {
			if(error['Message']){
	        	this.notificationService.add({text:error['Message'], type:'warning'})
	    	}
	    });
	}
	getCartData(){
		return this.cartItems;
	}
	create(model : Object = {}){
		return this.save(model);
	}
	flushCart(){
		this.cartItems = [];
		this.cartItemsSubject.next(this.cartItems);
		this.createNewCart();
	}
	setCartUser(cartUser){
		this.cartUser = cartUser;
		this.cartUser['fullName'] = cartUser['Name'] + ' ' + cartUser['Surname'];
	}
	getCartUser(){
		return this.cartUser;
	}
	removeCartUser(){
		this.cartUser = null;
	}
	getCartSummary(){
		this.setCustomEndpoint('GetCartSummary', true);
		this.query({});
		this.cartItems = [];
	}
}
