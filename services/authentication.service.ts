import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod, URLSearchParams } from "@angular/http";
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import { User } from '../models/user';
import { NotificationService } from './notification.service';

@Injectable()
export class AuthenticationService {

    private baseUrl: string;
    private token: string;
    private user: BehaviorSubject<User> = new BehaviorSubject(null);
    public user$ = this.user.asObservable();

    private authenticatedUser: User;
    redirectUrl: any;

    isLoggedIn: boolean = false;
    isLoggedIn$: Subject<boolean> = new Subject<boolean>();
    private isBoxofficeUser : boolean = false;
    constructor(
    	private http: Http, private router: Router,
    	private notificationService : NotificationService
    	) {
        let token = localStorage.getItem('nirvanaToken');
        this.token = token;
        this.getUser(this.token).subscribe(response => {
        	if(this.token){
        		this.loginComplete();
        	}
        }, error => {
            //this.loginComplete();
        });
    }
    login(username: string, password: string): Observable<any> {
        localStorage.removeItem('nirvanaToken');
        localStorage.removeItem('nirvanaUser');
        localStorage.removeItem('nirvanaUserName');
        localStorage.removeItem('nirvanaFirstName');
        localStorage.removeItem('nirvanaLastName');
        localStorage.removeItem('nirvanaImages');
        localStorage.removeItem('nirvanaSettings');
        localStorage.removeItem('nirvanaBoxOfficeUser');
        this.token = null;

        if (username == "noservice" && password == "nirvana") {
            let user = new User();
            user.UserName = username;
            this.user.next(new User(user));
            this.authenticatedUser = this.user.value;
            this.loginComplete();
            return this.user;
        }
        let json = {
			'UserName': username,
			'Password': password,
			'grant_type': "password",
		}
		let firmCodes = {'nirvana':'POZ','mobilet':'MBT','burak':'POZ','BURAK':'POZ'};
		let channelCodes = {'nirvana':'Web','mobilet':'Web','burak':'BoxOffice','BURAK':'BoxOffice'};
        if(firmCodes[username]){
        	this.isBoxofficeUser = true;
        	json['FirmCode'] = firmCodes[username];
        	localStorage.setItem('nirvanaBoxOfficeUser', 'true');
        	json['ApiKey'] = 'xxxxxxxxxxxxxxxxxxxxxxxxxXXXxxxXXXX';
        	json['ChannelCode'] = channelCodes[username];
        }else{
        	localStorage.setItem('nirvanaBoxOfficeUser', 'false');
        	this.isBoxofficeUser = false;
        }
        this.setApi();
        let headers = new Headers({
            'accept': 'application/json',
            'content-type': 'application/json'
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.baseUrl + '/' + 'Token',
        	json,
        	options)
            .flatMap(
            	response => {
	                this.token = response.json().access_token;
	                localStorage.setItem('nirvanaToken', this.token);
	                if(this.token){
	                	if(this.isBoxofficeUser){
		                	this.redirectUrl = '/boxoffice';
	                	}else{
	                		this.redirectUrl = '/events';
	                	}
	                	return this.getUser(this.token);
	                }
            }
            );
    }

    recoverPassword(username: string, email: string) {
        let headers = new Headers({
            'accept': 'application/json',
            'accept-language': 'en-US,en;q=0.8',
            'content-type': 'application/json'
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.baseUrl + '/Account/ResetPassword', { "UserName": username, "Email": email }, options)
            .map(response => response.json());
    }

    logout( navigate : boolean = true) {
    	this.notificationService.notifications.complete();
        localStorage.removeItem('nirvanaToken');
        localStorage.removeItem('nirvanaUser');
        localStorage.removeItem('nirvanaUserName');
        localStorage.removeItem('nirvanaFirstName');
        localStorage.removeItem('nirvanaLastName');
        localStorage.removeItem('nirvanaImages');
        localStorage.removeItem('nirvanaSettings');
        localStorage.removeItem('nirvanaFirmCode');
        localStorage.removeItem('nirvanaBoxOfficeUser');
        this.token = null;
        this.isLoggedIn = false;
        this.isLoggedIn$.next(this.isLoggedIn);
        location.reload();
        if(navigate){
        	this.router.navigateByUrl("/login");
        }
    }
    loginComplete() {
        if (this.authenticatedUser) this.authenticatedUser.isReady = true;
        this.isLoggedIn = true;
        this.isLoggedIn$.next(this.isLoggedIn);
        this.redirect();
    }

    redirect() {
        if (this.redirectUrl){
        	let redirectUrl = this.redirectUrl;
        	this.redirectUrl = null;
        	this.router.navigate([redirectUrl]);
        }
    }

    private getUser(token): Observable<any> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'bearer ' + token);
        let options = new RequestOptions({ headers: headers });
        if (token) {
        	this.setApi();
            return this.http.get(this.baseUrl + '/' + 'Account/GetUserInfo', options).map(
                response => {
                	let user = new User(response.json());
                    this.user.next(user);
                    this.user.complete();
                    this.authenticatedUser = user;
                    localStorage.setItem('nirvanaUserName', this.user.value.UserName);
                    localStorage.setItem('nirvanaFirstName', this.user.value.FirstName);
                    localStorage.setItem('nirvanaLastName', this.user.value.LastName);
                    localStorage.setItem('nirvanaImages', this.user.value.Images);
                    localStorage.setItem('nirvanaUser', this.user.value.Id.toString());
                    localStorage.setItem('nirvanaFirmCode', this.user.value.FirmCode);
                    localStorage.setItem('nirvanaChannelCode', this.user.value.ChannelCode);
                    return this.user$;
                },
                error => {
                    console.log("error : ", error.json());
                }
            )
        } else {
            return Observable.of([]);
        }
    }
    getToken(): string {
        return this.token;
    }
    getAuthenticatedUser() {
        return this.authenticatedUser;
    }
    getUserFirmCode() : string{
    	return (localStorage.getItem('nirvanaFirmCode') ? localStorage.getItem('nirvanaFirmCode') : this.getAuthenticatedUser().FirmCode);
    }
    getUserChannelCode() : string{
    	return (localStorage.getItem('nirvanaChannelCode') ? localStorage.getItem('nirvanaChannelCode') : this.getAuthenticatedUser().ChannelCode);
    }
    isBoxOfficeUser(){
    	return localStorage.getItem('nirvanaBoxOfficeUser') == 'true' ? true : false;
    }
    setApi(){
    	if(this.isBoxOfficeUser()){
    		this.baseUrl = environment.api.boxoffice + '/' + environment.api.path;
    	}else{
    		this.baseUrl = environment.api.host + '/' + environment.api.path;
    	}
    }
}
