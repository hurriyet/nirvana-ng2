import { Injectable } from '@angular/core';
import { Response, Http } from "@angular/http";
import { Venue } from "../models/venue";
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BaseDataService } from "../classes/base-data-service";
import { StoreService } from './../services/store.service';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class VenueService extends BaseDataService {
    count: BehaviorSubject<number> = new BehaviorSubject(0);
    data: BehaviorSubject<Venue[]> = new BehaviorSubject([]);
    queryParams: Object = { filter: [], sort: [], pageSize: 10, page: 1 };
    queryParamSubject: BehaviorSubject<Object> = new BehaviorSubject(this.queryParams);
    viewType: Object = { isCardViewActive: true, isListViewActive: false };
    constructor(http: Http, storeService: StoreService, authenticationService: AuthenticationService) {
        super(http, 'VVenue', storeService, authenticationService);
    }
    getRawData(): Venue[] {
        return this.storeService.getData('VVenue');
    }
    getData(): BehaviorSubject<Venue[]> {
        return this.data;
    }
    create(event) {
        return this.save(event);
    }
    gotoPage(params: Object) {
        let page = params["page"] || 0,
            sort = params["sort"] ? (typeof params["sort"] == 'string' ? JSON.parse(params["sort"]) : params["sort"]) : null,
            filter = params["filter"] || null,
            pageSize = params["pageSize"] || 4,
            search = params["search"] || null;
        this.query({ pageSize: pageSize, page: page, sort: sort, filter: filter, search: search });
    }
}

