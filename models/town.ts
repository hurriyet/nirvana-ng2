import { BaseModel } from '../classes/base-model';

export class Town extends BaseModel{
	Name: string;
  	CityId: number;
  	Order: number;
  	Id: number;
}
