import {BaseModel} from '../classes/base-model';

export class PerformancePerformer extends BaseModel {
    PerformanceId: number;
    PerformerId: number;
    BeginDate: Date;
    EndDate: Date;
    Info: string;
    Id: number;
}
