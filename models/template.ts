import { Venue } from './venue';
import { BaseModel } from '../classes/base-model';

export class Template extends BaseModel{
	Id: number;
	VenueId: number;
  	Info: string;
  	LayoutImage: string;
  	IsStanding: boolean;
  	IsActive: boolean;
	Venue: Venue;
}