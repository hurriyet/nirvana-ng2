import { Variant } from './variant';
import {BaseModel} from '../classes/base-model';

export class PriceList extends BaseModel{
    Id: number;
    ProductId: number;
    NominalPrice: number;
    IsEnabled: boolean;
    BeginDate: Date;
    EndDate: Date;
    Localization?: {
        Tr?: {
            Name?: string,
            Info?: string
        },
        En?: {
            Name?: string
            Info?: string
        }
    };
    Variants: Variant[]
}