import { Template } from './template';
import { BaseModel } from '../classes/base-model';
import { PerformanceStatus } from './performance-status.enum';
import { ReservationExpirationType } from './reservation-expiration-type.enum';

export class Performance extends BaseModel {
 	IsArchive: boolean;
    CategoryId: number;
    VenueTemplateId: number;
    Status: PerformanceStatus;
    PublishDate: string;
    SalesBeginDate: string;
    SalesEndDate: string;
    Code: string;
    Date: string;
    IsEnabled: boolean;
    PurchaseTimeSeconds: number;
    IsSeatSelectionEnabled: boolean;
    IsSeason: boolean;
    SeasonalPerformanceId: number;
    Images: string;
    ReservationAvailable: boolean;
    ReservationExpirationType: ReservationExpirationType;
    ReservationExpirationTime: number;
    SuspensionReason: string;
    CancellationReason: string;
    NoExpire: boolean;
    ExpirationType: number;
    ExpirationDate: string;
    EventId: number;
    IsInviteFriendAvailable: boolean;
    InviteFriendExpirationType: ReservationExpirationType;
    InviteFriendExpirationTime: number;
    IsBundle: boolean;
    Id: number;
    PerformanceName: string;
    VenueTemplate: Template;
    Localization: {
        Tr?: {
          Name?: string;
          ShortName?: string;
          Description?: string;
          StatusText?: string
        },
        En?: {
          Name?: string;
          ShortName?: string;
          Description?: string;
          StatusText?: string
        }
	}
}