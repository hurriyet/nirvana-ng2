import { BaseModel } from '../classes/base-model';
import { FirmType} from './firm-type.enum';
import { SponsorType} from './sponsor-type.enum';

export class Firm extends BaseModel {
	Id: number;
	Name : string;
	ShortName: string;
	Type: SponsorType;
}